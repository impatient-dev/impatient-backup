import org.gradle.jvm.tasks.Jar

plugins {
	id("org.jetbrains.kotlin.jvm") version "1.9.0"
	id("org.jetbrains.compose") version "1.4.3"
	id("application")
}

group = "imp-dev"
version = "0.0.0"
val jvm_version: String by project

repositories {
	mavenCentral()
}

dependencies {
	implementation(project(":impatient-compose-desktop"))
	implementation(project(":impatient-coroutines"))
	implementation(project(":impatient-json"))
	implementation(project(":impatient-logback"))
	implementation(project(":impatient-okhttp"))
	implementation(project(":impatient-orm-kotlin"))
	implementation(project(":impatient-sqlite"))
	implementation(project(":impatient-sqlite-cache"))
	implementation(project(":impatient-sqlite-main"))
	implementation(project(":impatient-utils"))
	implementation(project(":impatient-web-undertow"))

	testImplementation(project(":impatient-junit4"))
}

java.toolchain.languageVersion.set(JavaLanguageVersion.of(jvm_version.toInt()))

application.mainClass = "imp.backup.MainKt" // applies to run, but not jar

tasks.withType<Jar> {
	manifest.attributes(mapOf(
		"Main-Class" to application.mainClass
	))

	// make a fat JAR - https://www.baeldung.com/kotlin/gradle-executable-jar
	duplicatesStrategy = DuplicatesStrategy.EXCLUDE
	val contents = configurations.runtimeClasspath.get()
		.map { if (it.isDirectory) it else zipTree(it) } +
		sourceSets.main.get().output
	from(contents)
}