package imp.backup.test

import imp.backup.data.LOCATION_META_FILENAME
import imp.backup.data.PATCH_DB_FILENAME
import imp.backup.job.PlanTogglePatchJob
import imp.backup.util.PlanTogglePatchAssertions
import imp.backup.util.TestPatchLocation
import imp.logback.initLogbackTrace
import imp.util.defaultScope
import imp.util.logger
import kotlinx.coroutines.runBlocking
import org.junit.BeforeClass
import org.junit.Test

class PatchToggleTest {
	companion object {
		@BeforeClass @JvmStatic fun setupLogging() {
			initLogbackTrace()
		}
	}


	@Test fun testApplyBasic() = runBlocking {
		TestPatchLocation.of("patch-test").use { test ->
			test.main.file("foo.txt")
			test.staging.file("bar/bar.txt")
			test.staging.file("bar/bar2.txt")

			val plan = PlanTogglePatchJob(test.location, "bar", true)
			plan.run(defaultScope())
			PlanTogglePatchAssertions(test, plan)
				.fileMain(LOCATION_META_FILENAME)
				.fileStaging(PATCH_DB_FILENAME)
				.fileMain("foo.txt")
				.fileStaging("bar/bar.txt")
				.fileStaging("bar/bar2.txt")
				.assert(filesToApply = 2)

			//TODO actually apply
		}
	}


	@Test fun testApplyReplacePreexisting() = runBlocking {
		logger("test").debug("debug test")
		logger("test").trace("trace test")
		TestPatchLocation.of("patch-test").use { test ->
			test.main.file("bar.txt")
			test.staging.file("bar/bar.txt")
			test.staging.file("bar/bar2.txt")

			val plan = PlanTogglePatchJob(test.location, "bar", true)
			plan.run(defaultScope())
			PlanTogglePatchAssertions(test, plan)
				.fileMain(LOCATION_META_FILENAME)
				.fileStaging(PATCH_DB_FILENAME)
				.fileMain("bar.txt")
				.fileStaging("bar/bar.txt")
				.fileStaging("bar/bar2.txt")
				.assert(filesToApply = 2)

			//TODO actually apply
		}
	}
}