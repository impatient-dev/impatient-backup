package imp.backup.util

import imp.backup.data.PatchConflict
import imp.backup.job.PlanTogglePatchJob
import imp.util.FirstList
import imp.util.putNew
import org.junit.Assert
import java.nio.file.Path
import java.nio.file.Paths

/**Builder for asserting whether the results are correct for a job planning to toggle a patch.*/
class PlanTogglePatchAssertions (
	val location: TestPatchLocation,
	val job: PlanTogglePatchJob,
) {
	private val dirAssertions = TestDirAssertions()
	/**The key is the patch name.*/
	private val expectedConflicts = HashMap<String, PatchConflict>()

	private val mainRelative: Path = location.dir.path.relativize(location.main.path)
	private val stagingRelative: Path = location.dir.path.relativize(location.staging.path)

	/**Adds an assertion that a file exists in the main (not staging) directory, and (if the content is not null) has the specified content.*/
	fun fileMain(relative: String, content: String? = null): PlanTogglePatchAssertions {
		dirAssertions.expectFile(mainRelative.resolve(relative), content)
		return this
	}
	/**Adds an assertion that a file exists in the staging directory, and (if the content is not null) has the specified content.*/
	fun fileStaging(relative: String, content: String? = null): PlanTogglePatchAssertions {
		dirAssertions.expectFile(stagingRelative.resolve(relative), content)
		return this
	}

	/**Adds an assertion that a conflict is present in the adapter.*/
	fun conflict(patchName: String, vararg files: String): PlanTogglePatchAssertions {
		val fileList = FirstList(files.map {Paths.get(it)})
		expectedConflicts.putNew(patchName, PatchConflict(patchName, files.size, fileList))
		return this
	}

	private var failed = 0
	private fun fail(msg: String) {
		System.err.println("\t$msg")
		failed++
	}

	/**Asserts that all expected files are present on disk, and no others are, and that the filesToApply count is correct.*/
	fun assert(filesToApply: Long) {
		println("Beginning assertions for test location: ${location.location.name} in ${location.dir.path}")
		failed = 0

		dirAssertions.assert(location.dir)

		if(filesToApply != job.files)
			fail("Files to apply: expect $filesToApply, found ${job.files}.")

		if(job.conflicts == null) {
			if(expectedConflicts.size > 0)
				fail("Job conflicts collection is null.")
		} else {
			val actualConflicts = job.conflicts!!.associateBy { it.otherPatchName }
			for(entry in expectedConflicts.entries) {
				val patchName = entry.key
				val e = entry.value
				val a = actualConflicts[patchName]
				if(a == null)
					fail("Missing conflict for patch $patchName")
				else {
					val ef = HashSet(e.someFiles)
					val af = HashSet(a.someFiles)
					if(ef != af) {
						System.err.println("Expected file conflicts: ${e.someFiles.joinToString()}")
						System.err.println("Actual file conflicts: ${a.someFiles.joinToString()}")
						fail("Wrong conflicted files for patch $patchName")
					}
				}
			}
			for(patchName in actualConflicts.keys)
				if(!expectedConflicts.containsKey(patchName))
					fail("Extra conflict for patch $patchName")
		}

		if(failed > 0)
			Assert.fail("$failed assertion(s) failed for plan toggle patch job - location ${location.location.name}, patch ${job.patchName}")
	}
}