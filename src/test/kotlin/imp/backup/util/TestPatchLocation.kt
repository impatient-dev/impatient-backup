package imp.backup.util

import imp.backup.data.*
import imp.backup.jsonMapper
import imp.util.logger

private val log = TestPatchLocation::class.logger

/**A temporary patch location for testing. All files are deleted when this is closed.
 * The constructor creates the location meta file.*/
class TestPatchLocation (
	name: String,
	val dir: TestDir,
) : AutoCloseable {
	val location: PatchLocation
	val main: TestDir
	val staging: TestDir

	init {
		log.debug("Initializing test location {} at {}.", name, dir.path)
		main = TestDir.of(dir.resolve("main"))
		staging = TestDir.of(dir.resolve("staging"))
		val meta = LocationMeta(name, LocationType.PATCH, stagingDirectory = staging.path.toAbsolutePath().toString())
		main.file(LOCATION_META_FILENAME, jsonMapper.writeValueAsString(meta))
		location = loadLocation(main.path) as PatchLocation
	}

	override fun close() {
		dir.close()
	}

	companion object {
		fun of(name: String) : TestPatchLocation {
			return TestPatchLocation(name, TestDir.of(TEST_FILES_DIR.resolve(name), assertNew = true))
		}
	}
}