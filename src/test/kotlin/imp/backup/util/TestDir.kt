package imp.backup.util

import imp.util.listFilesRecursively
import imp.util.logger
import org.apache.commons.io.FileUtils
import java.nio.charset.StandardCharsets.UTF_8
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.*

private val log = TestDir::class.logger

val TEST_FILES_DIR: Path = Paths.get("temp-test")

/**A temporary directory for testing, to be deleted when the tests finish.
 * You can then create and modify files in this directory. Most methods accept only relative paths.*/
class TestDir (
	val path: Path,
	assertNew: Boolean = false,
) : AutoCloseable {

	init {
		if(assertNew && Files.exists(path))
			throw IllegalStateException("Directory already exists: $path")
	}

	override fun close() {
		log.debug("Deleting {}", path)
		FileUtils.deleteQuietly(path.toFile())
	}

	fun resolve(relative: Path): Path = path.resolve(relative.assertRelative())
	fun resolve(relative: String): Path = resolve(Paths.get(relative))
	fun resolve(relativeFirst: String, vararg rest: String): Path = resolve(Paths.get(relativeFirst, *rest))

	/**Creates a file containing text.*/
	fun file(relative: Path, text: String = "") {
		log.debug("Writing to {}: '{}'.", relative, text)
		FileUtils.write(path.resolve(relative.assertRelative()).toFile(), text, Charsets.UTF_8)
	}
	/**Creates a file containing text.*/
	fun file(relative: String, text: String = "") = file(Paths.get(relative), text)

	private fun Path.assertRelative(): Path {
		if(this.isAbsolute)
			throw IllegalArgumentException("Must be a relative path: $this")
		if(!path.resolve(this).startsWith(path))
			throw IllegalArgumentException("Sneaking out of the test dir: $this out of $path")
		return this
	}


	companion object {
		/**Generates a test directory in the specified location.*/
		fun of(path: Path, assertNew: Boolean = false): TestDir = TestDir(path, assertNew)
		/**Generates a test directory in the specified location.*/
		fun of(path: String, assertNew: Boolean = false): TestDir = of(Paths.get(path), assertNew)
		/**Generates a randomly named directory.*/
		fun random(): TestDir = of(Paths.get(UUID.randomUUID().toString()), true)
	}
}



fun TestDir.assertions() = TestDirAssertions()

/**Builder to assert a TestDir contains all the files you expect. Not reusable.*/
class TestDirAssertions {
	private val expectedRelative = HashMap<Path, String?>()

	fun expectFile(relative: Path, content: String? = null): TestDirAssertions {
		if(relative.isAbsolute)
			throw IllegalArgumentException("Absolute: $relative")
		expectedRelative.put(relative, content)
		return this
	}

	fun expectFile(relative: String, content: String? = null): TestDirAssertions = expectFile(Paths.get(relative), content)

	private var failed = 0
	private fun fail(msg: String) {
		System.err.println("\t$msg")
		failed++
	}

	fun assert(dir: TestDir) {
		println("Beginning assertions for test dir: ${dir.path}")
		failed = 0
		val actual = dir.path.listFilesRecursively().asSequence()
			.map {dir.path.relativize(it)}
			.toCollection(HashSet())
		for(a in actual) {
			if(!expectedRelative.contains(a))
				fail("Extra file: $a")
			else {
				val expectContent = expectedRelative[a]
				val actualContent = if(Files.exists(a)) FileUtils.readFileToString(a.toFile(), UTF_8) else null
				if (expectContent != null && expectContent != actualContent)
					fail("Wrong content in $a: $actualContent")
			}
		}

		for(path in expectedRelative.keys)
			if(!actual.contains(path))
				fail("Missing file: $path")

		if(failed > 0)
			throw RuntimeException("$failed assertion(s) failed for test dir: ${dir.path}")
		println("All assertions passed for test dir: ${dir.path}")
	}
}