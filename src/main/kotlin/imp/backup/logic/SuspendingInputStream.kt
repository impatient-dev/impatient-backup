package imp.backup.logic

import java.io.InputStream


interface SuspendingInputStream : AutoCloseable {
	/**The same as InputStream.read(), except it can suspend. Returns -1 if we've reached the end of the stream.*/
	suspend fun read(): Int


	/**Reads up to 2 bytes (big endian). Returns -1 if the stream is empty/finished. If there is only 1 byte left in the stream, the 2nd is treated as 0.*/
	suspend fun read2be(): Int {
		val a = this.read()
		if(a == -1)
			return -1
		var b = this.read()
		if(b == -1)
			b = 0
		return (a shl 8) or b
	}

	/**Reads up to 2 bytes (little endian). Returns -1 if the stream is empty/finished. If there is only 1 byte left in the stream, the 2nd is treated as 0.*/
	suspend fun read2le(): Int {
		val a = this.read()
		if(a == -1)
			return -1
		var b = this.read()
		if(b == -1)
			b = 0
		return (b shl 8) or a
	}

	/**Reads up to 4 bytes (big endian). Returns -1 if the stream is empty/finished.
	 * If there is at least 1 byte left in the stream, but fewer than 4, the missing bytes will be treated as 0.*/
	suspend fun read4be(): Int {
		var out = this.read()
		if(out == -1)
			return -1
		out = out shl 24
		for(i in 0 until 3) {
			val r = this.read()
			if(r == -1)
				return out
			out = out or (r shl (8 * (2 - i)))
		}
		return out
	}

	/**Reads up to 4 bytes (little endian). Returns -1 if the stream is empty/finished.
	 * If there is at least 1 byte left in the stream, but fewer than 4, the missing bytes will be treated as 0.
	 * Deprecated because this method doesn't allow you to distinguish between end-of-stream and 4 bytes that together make a -1*/
	@Deprecated("-1 is ambiguous")
	suspend fun read4le(): Int {
		var out = this.read()
		if(out == -1)
			return -1
		for(i in 0 until 3) {
			val r = this.read()
			if(r == -1)
				return out
			out = out or (r shl (8 * (i + 1)))
		}
		return out
	}
}



class SuspendingInputStreamWrapper (
	private val wrapped: InputStream,
) : SuspendingInputStream {
	override suspend fun read() = wrapped.read()
	override fun close() = wrapped.close()
}

fun String.byteSuspendingInputStream() = SuspendingInputStreamWrapper(this.byteInputStream())