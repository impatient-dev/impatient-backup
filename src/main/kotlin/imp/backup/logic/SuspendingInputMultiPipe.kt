package imp.backup.logic

import imp.util.asInt
import imp.util.logger
import imp.util.receiveOpt
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlin.math.min

private val log = SuspendingInputMultiPipe::class.logger

private const val CHANNEL_CAPACITY = 1

/**Generates a SuspendingInputStream for each reader you provide.
 * Sends bytes to the readers as you write them to this object.
 * All readers must read their streams fully and simultaneously.*/
class SuspendingInputMultiPipe (
	private val readers: List<suspend (SuspendingInputStream) -> Unit>,
	dispatcher: CoroutineDispatcher = Dispatchers.IO,
	private val bufferSize: Int = DEFAULT_BUFFER_SIZE,
) : AutoCloseable {
	/**A rotating array of buffers that are provided to the child streams.
	 * In addition to the max number of buffers that can be waiting in channels,
	 * we need 1 extra buffer that can be written to and 1 more that can be read by children (is no longer in a channel).*/
	private val buffers = Array(CHANNEL_CAPACITY + 2) { MyBuffer(bufferSize) }
	/**The index of the current buffer we're writing to.*/
	private var wb = 0
	private var channels = Array(readers.size) { Channel<MyBuffer>(CHANNEL_CAPACITY) }
	private val streams = Array(readers.size) { i -> MyStream(channels[i]) }

	private val scope = CoroutineScope(dispatcher)
	private val jobs = Array(readers.size) { i -> scope.launch { runReader(i) }}

	private suspend fun runReader(index: Int) {
		log.trace("Starting reader {}/{}.", index + 1, readers.size)
		try {
			readers[index](streams[index])
			log.trace("Finished reader {}/{}.", index + 1, readers.size)
		} catch(e: Throwable) {
			if(e !is CancellationException)
				log.error("Error in reader ${index + 1}/${readers.size}.", e)
			throw e
		}
	}

	/**Copies some bytes to all child streams.*/
	suspend fun write(bytes: ByteArray, count: Int) {
		var written = 0
		var b = buffers[wb]
		while(true) {
			val write = min(count - written, b.remainingCapacity)
			b.write(bytes, written, write)
			written += write
			if(b.remainingCapacity > 0)
				return
			channels.forEach { it.send(b) }
			wb = (wb + 1) % buffers.size
			b = buffers[wb]
			b.clear()
		}
	}

	/**Signals to all child streams that there are no more bytes to read, then waits for the children to finish.
	 * You might want to use a timeout around this function.*/
	suspend fun finish() {
		channels.forEach { it.close() }
		joinAll(*jobs)
	}

	override fun close() {
		channels.forEach { it.close() }
		jobs.forEach { it.cancel() }
		scope.cancel()
	}


	/**A SuspendingInputStream that reads bytes from the buffers it gets from the channel.
	 * Starts returning -1 for the next byte once it reaches the end of the channel*/
	private class MyStream (
		val channel: Channel<MyBuffer>,
	) : SuspendingInputStream {
		private var buffer: MyBuffer? = null
		/**The next byte to read in the buffer.*/
		private var pos = -1

		override fun close() {
			channel.close()
		}

		override suspend fun read(): Int {
			var b = buffer
			if(b == null || pos >= b.size) {
				b = channel.receiveOpt()
				b?.let { check(it.size > 0) }
				buffer = b
				pos = 0
			}
			if(b == null)
				return -1
			val out = b.array[pos++].asInt
			assert(out != -1)
			return out
		}
	}


	private class MyBuffer (size: Int) {
		val array = ByteArray(size + 4)
		/**The number of valid bytes in the array.*/
		var size = 0
			private set
		inline val capacity get() = array.size
		val remainingCapacity get() = capacity - size

		/**The bytes must not overflow the array.*/
		fun write(bytes: ByteArray, srcOffset: Int, len: Int) {
			System.arraycopy(bytes, srcOffset, array, size, len)
			size += len
		}

		fun clear() {
			size = 0
		}
	}



	class Builder {
		private val _readers = ArrayList<suspend (SuspendingInputStream) -> Unit>()
		val readerCount: Int get() = _readers.size

		fun add(reader: suspend (SuspendingInputStream) -> Unit): Builder {
			_readers.add(reader)
			return this
		}

		/**Creates a multi-pipe with the specified readers.
		 * All readers will read from their streams on the same dispatcher, but an individual reader can override this with withContext().*/
		fun build(dispatcher: CoroutineDispatcher = Dispatchers.IO) = SuspendingInputMultiPipe(_readers, dispatcher)
	}
}
