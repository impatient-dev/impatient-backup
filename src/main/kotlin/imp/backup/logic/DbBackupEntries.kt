package imp.backup.logic

import imp.backup.data.DbBackupEntry
import imp.backup.data.DbBackupImpl
import imp.backup.data.dbTableEntry
import imp.sqlite.Database
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import java.nio.file.Path

/**A class that knows how to look up entries in the backup database.
 * Caches the last directory found.*/
class DbBackupEntries (
	private val backup: DbBackupImpl,
) {
	private val mutex = Mutex()
	private var cached: MyEntry? = null

	/**Returns the entry with the provided relative path, if any.*/
	suspend fun get(relative: Path, db: Database): DbBackupEntry? = mutex.withLock {
		check(!relative.isAbsolute) { "Path is absolute: $relative" }
		val c = cached
		if(c != null && c.relative == relative)
				return c.entry
		val out = dbTableEntry.query().andWhere("path = ?").param(relative.toString()).selectOnly(db)
		if(out != null && out.isDir())
			cached = MyEntry(relative, out)
		return out
	}

	/**Returns the entry with the provided path, or throws if there's no such entry.*/
	suspend fun require(relative: Path, db: Database): DbBackupEntry = get(relative, db) ?: throw Exception("Entry $relative not found in $backup")
}


private class MyEntry (
	val relative: Path,
	val entry: DbBackupEntry,
) {
	init {
		check(!relative.isAbsolute)
	}
}