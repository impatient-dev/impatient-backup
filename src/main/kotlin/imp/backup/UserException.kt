package imp.backup

/**Thrown when we discover the user did something wrong, e.g. put something invalid in a config file. The message must be user-friendly.*/
class UserException (
	msg: String,
	cause: Throwable? = null,
	) : RuntimeException(msg, cause)