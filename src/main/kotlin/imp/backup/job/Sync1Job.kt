package imp.backup.job

import imp.backup.data.BackupImpl
import imp.backup.data.UserLocation
import imp.backup.files.*
import imp.util.*
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.ensureActive
import kotlinx.coroutines.joinAll
import java.io.IOException
import java.lang.Integer.max
import java.nio.file.Path
import java.time.Instant


private val log = Sync1Job::class.logger

class FailedPath (
	val path: Path,
	val th: Throwable,
)



/**A 1-way sync, where files are created/deleted/etc. in the destination until it exactly matches the source.
 * This can be used for a backup from a user location to a backup location, or a restore (the reverse).
 *
 * This is implemented as 2 coroutines connected by channels.
 * The goal is to have 1 coroutine continuously copying bytes from the source to the destination,
 * and the other coroutine ensuring there is always enough work for the byte-copy coroutine.*/
class Sync1Job (
	val src: SyncTarget,
	val dst: SyncTarget,
) : AppJob {
	private val sh = NonWriteableFullEntryHandler(src.reqFull())
	private val dh = dst.reqFull()

	@Volatile var state = State.NOT_STARTED
		private set

	/**The file or directory we're currently inspecting.*/
	@Volatile var inspectingPathRelative: Path? = null
		private set
	/**The file we're currently copying, if any.*/
	@Volatile var copyingFileRelative: Path? = null
		private set
	/**How many bytes we've copied into the current file. Invalid if we're not copying into a file currently.
	 * When the file we're copying into changes, the change to this variable will come slightly later; be careful.*/
	@Volatile var copyingNumerator = 0L
		private set
	/**The size in bytes of the current file we're copying. Has the same complexities as copyingNumerator.*/
	@Volatile var copyingDenominator = 0L
		private set
	/**Numbers of files/directories created, including overwrites of existing files.*/
	@Volatile var pathsCopied = 0L
		private set
	/**Number of bytes written for created/overwritten files.*/
	@Volatile var bytesCopied = 0L
		private set
	/**The number of files/directories deleted, including ones that were later replaced.*/
	@Volatile var pathsDeleted = 0L
		private set
	/**Total size of all files deleted, including files that were lated replaced.*/
	@Volatile var bytesDeleted = 0L
		private set

	@Volatile var errorCount = 0L
		private set
	/**Populated when the job finishes.*/
	@Volatile var errors: FirstList<FailedPath>? = null
		private set
	private val _errors = FirstList<FailedPath>(100)
	@Volatile var start: Instant? = null
		private set
	@Volatile var end: Instant? = null
		private set

	/**Number of files in the source location - will be written in the metadata at the end of this sync.
	 * This number will be inaccurate if we run into errors.*/
	private var filesForMeta = 0L
	/**Number of bytes in the source location - will be written in the metadata at the end of this sync.
	 * This number will be inaccurate if we run into errors.*/
	private var bytesForMeta = 0L


	override suspend fun run(scope: CoroutineScope) {
		try {
			log.info("Starting backup from {} to {}.", src, dst)
			state = State.RUNNING
			val chCopy = Channel<FileToWrite>(16)
			start = Instant.now()

			val srcRoot = sh.getRootDir()!!
			val dstRoot = dh.getRootDir(createIfMissing = true)!!
			if(dst is BackupImpl)
				dst.startBackup(start!!, (src as UserLocation).name)

			joinAll(
				scope.launchIo {
					diffDirs(srcRoot, dstRoot, chCopy, scope)
					log.debug("Done diffing files.")
					chCopy.close()
				},
				scope.launchIo {
					doCopyFiles(chCopy, scope)
					log.debug("Done writing files.")
				},
			)

			inspectingPathRelative = null
			end = Instant.now()
			if(dst is BackupImpl)
				dst.endBackup(end!!, filesForMeta, bytesForMeta)
			state = State.SUCCESS
			log.debug("1-way sync job finished.")
		} catch(e: Throwable) {
			if(e is CancellationException)
				state = State.ABORTED
			else
				state = State.FAILED
			throw e
		} finally {
			errors = _errors
		}
	}



	/**Finds differences between a source and destination directory, recursively checking any subdirectories. Both directories must exist.
	 * If a file needs to be copied to the destination, that is sent to the channel;
	 * all other changes to the destination are handled in this method or the methods it calls.*/
	private suspend fun diffDirs(srcDir: FullLocEntry, dstDir: FullLocEntry, chCopy: Channel<FileToWrite>, scope: CoroutineScope) {
		log.debug("Diffing dir {}.", srcDir.path)
		assert(srcDir.isDir) { "Source is not a directory: ${srcDir.path}" }
		assert(dstDir.isDir) { "Destination is not a directory: ${dstDir.path}" }
		assert(srcDir.path == dstDir.path) { "Path mismatch for directories to diff: src=${srcDir.path}, dst=${dstDir.path}" }

		try {
			val its = sortedDirIterator(srcDir, sh, scope) ?: return
			val itd = sortedDirIterator(dstDir, dh, scope) ?: return
			var s = its.nextOpt()
			var d = itd.nextOpt()

			while (s != null || d != null) {
				if(s?.bytes != null) {
					filesForMeta++
					bytesForMeta += s.bytes!!
				}
				if (s == null || (d != null && s.path > d.path)) { // entry missing from source
					inspectingPathRelative = d!!.path
					diffCaseDstOnly(d)
					d = itd.nextOpt()
				} else if (d == null || (s != null && s.path < d.path)) { // entry missing from destination
					inspectingPathRelative = s.path
					diffCaseSrcOnly(s, srcDir, chCopy, scope)
					s = its.nextOpt()
				} else { // same entry in both source and destination
					inspectingPathRelative = s.path
					diffCaseMatch(s, d, srcDir, dstDir, chCopy, scope)
					s = its.nextOpt()
					d = itd.nextOpt()
				}
			}
		} catch(e: Throwable) {
			if(e is CancellationException)
				throw e
			recordError(Exception("Failed to process directory ${srcDir.path}", e), srcDir.path)
			return // the iterators may not line up now
		}
	}

	/**During a diff, handles the case where both the source and destination have entries with the same path.*/
	private suspend fun diffCaseMatch(
		s: FullLocEntry,
		d: FullLocEntry,
		sParent: FullLocEntry,
		dParent: FullLocEntry,
		chCopy: Channel<FileToWrite>,
		scope: CoroutineScope,
	) {
		assert(s.path == d.path)
		if(s.type != d.type) {
			log.debug("Replace destination {} with {}: {}.", d.type, s.type, d.path)
			deleteAndRecord(d, dh)
			createInDestination(s, sParent, chCopy, scope)
		} else if(s.isFile) {
			if(filesAreDifferent(s, d)) {
				log.debug("Overwrite destination file: {}.", s.path)
				dh.delete(d)
				chCopy.send(FileToWrite(s, sParent))
			}
		} else if(s.isDir) {
			diffDirs(s, d, chCopy, scope)
		} else {
			assert(s.isSymlink)
			if(s.symlinkTo!! != d.symlinkTo!!) {
				log.debug("Alter destination symlink {} from {} to {}.", s.path, d.symlinkTo, s.symlinkTo)
				dh.delete(d)
				createInDestination(s, sParent, chCopy, scope)
			}
		}
	}

	/**During a diff, handles the case where the destination is missing an entry that the source has.*/
	private suspend fun diffCaseSrcOnly(s: FullLocEntry, parent: FullLocEntry, chCopy: Channel<FileToWrite>, scope: CoroutineScope) {
		log.debug("Copy {} to destination: {}.", s.type, s.path)
		createInDestination(s, parent, chCopy, scope)
	}

	/**During a diff, handles the case where the source is missing an entry that the destination has.*/
	private suspend fun diffCaseDstOnly(d: FullLocEntry) {
		log.debug("Delete from destination: {}.", d.path)
		deleteAndRecord(d, dh)
	}

	private suspend fun createInDestination(s: FullLocEntry, parent: FullLocEntry, chCopy: Channel<FileToWrite>, scope: CoroutineScope) {
		if(s.type == EntryType.FILE) {
			chCopy.send(FileToWrite(s, parent))
		} else if(s.type == EntryType.DIR) {
			val created = try {
				dh.createDir(parent, s.name, s.path)
			} catch(e: Exception) {
				recordError(Exception("Failed to create dir ${s.path}", e), s.path)
				null
			}
			if(created != null) {
				pathsCopied++
				diffDirs(s, created, chCopy, scope)
			}
		} else { //symlink
			dh.createSymlink(parent, s.name, s.symlinkTo!!)
		}
	}


	/**Returns null if we fail to list the files.*/
	private suspend fun sortedDirIterator(dir: FullLocEntry, handler: FullLocEntryHandler, scope: CoroutineScope): Iterator<FullLocEntry>? {
		try {
			return handler.listDir(dir).sortedBy { it.name }.iterator()
		} catch(e: IOException) {
			val ee = IOException("Failed to list ${if(handler == sh) "source" else "destination"} files in ${dir.path}", e)
			recordError(ee, dir.path)
			return null
		}
	}



	/**Reads from the channel. For each entry in the channel, copies that file to the destination.
	 * Assumes the corresponding file/directory in the destination has already been deleted.*/
	private suspend fun doCopyFiles(ch: Channel<FileToWrite>, scope: CoroutineScope) {
		while(true) {
			val entry = ch.receiveOpt() ?: return
			log.debug("Writing file {}.", entry.file.path)
			copyingFileRelative = entry.file.path
			copyingNumerator = 0
			copyingDenominator = entry.file.bytes!!
			val buffer = ByteArray(decideChunkSize(sh, dh))

			try {
				dh.writeFile(entry.parent, entry.file.name, entry.file.bytes, entry.file.lastModified!!).use { outs ->
					sh.readFile(entry.file).use { ins ->
						while (true) {
							val i = ins.read(buffer, scope)
							if (i == -1) {
								outs.finish(scope)
								break
							}
							assert(i != 0)
							outs.write(buffer, scope, i)
							bytesCopied += i
							copyingNumerator += i
							scope.ensureActive()
						}
					}
				}

				pathsCopied++
				if(copyingNumerator != copyingDenominator)
					log.warn("Wrong number of bytes copied for {}: {} instead of {}.", entry.file.path, copyingNumerator, copyingDenominator)
			} catch(e: Exception) {
				val ee = Exception("Failed to copy file ${entry.file.path}", e)
				log.error("", ee)
				errorCount++
				_errors.add(FailedPath(entry.file.path, ee))
			}

			copyingFileRelative = null
		}
	}


	private fun recordError(e: Exception, path: Path) {
		log.error("Error processing $path.", e)
		errorCount++
		_errors.add(FailedPath(path, e))
	}

	private suspend fun deleteAndRecord(entry: FullLocEntry, handler: FullLocEntryHandler) {
		handler.delete(entry)
		pathsDeleted++
		// TODO recursively count all files/directories deleted
		if(entry.bytes != null)
			bytesDeleted += entry.bytes
	}


	enum class State {
		NOT_STARTED,
		RUNNING,
		SUCCESS,
		ABORTED,
		FAILED,
	}



	private data class FileToWrite(
		val file: FullLocEntry,
		val parent: FullLocEntry,
	) {
		init {
			check(file.isFile && parent.isDir)
		}
	}

	private fun filesAreDifferent(a: FullLocEntry, b: FullLocEntry): Boolean {
		assert(a.isFile && b.isFile)
		return a.bytes!! != b.bytes!! || a.lastModified!! != b.lastModified!!
	}
}


private fun decideChunkSize(a: FullLocEntryHandler, b: FullLocEntryHandler): Int {
	return max(decideChunkSize(a.raw), decideChunkSize(b.raw))
}

private fun decideChunkSize(h: RawLocEntryHandler): Int {
	if(h is RemoteRawEntryHandler)
		return 500_000
	return DEFAULT_BUFFER_SIZE
}