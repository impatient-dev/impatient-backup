package imp.backup.job

import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineScope

/**A job the user wants to execute. E.g. a backup, or a restore, or a sync.
 * In addition to the run method, each job is expected to have a bunch of public variables
 * describing the current state of the job, so the UI etc. can tell how things are going.
 * It must be safe to read these variables in any thread - they should be volatile or something.
 * You may want to make the setters for these variables protected, so a subclass "fake" job can be created for UI testing.*/
interface AppJob {
	/**Does the thing this type of job does, such as backing up files.
	 * This method should periodically check whether it was cancelled, and throw CancellationException if it stops early
	 * Run can only be called once for a job instance - create a new job if you want to retry.*/
	@Throws(CancellationException::class) suspend fun run(scope: CoroutineScope)
}


/**The possible states for a job that just runs, with no intermediate steps.*/
enum class SimpleJobState {
	NOT_STARTED,
	RUNNING,
	DONE,
}