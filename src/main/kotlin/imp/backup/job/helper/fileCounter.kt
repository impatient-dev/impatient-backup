package imp.backup.job.helper

import imp.util.fmtBytes
import imp.util.logger
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ensureActive
import java.io.FileNotFoundException
import java.nio.file.Files
import java.nio.file.Path
import java.util.concurrent.ThreadLocalRandom


private val log = FileCounter::class.logger


/**Counts the number and total size of all files in a directory, recursively.*/
open class FileCounter (
	val path: Path
) {
	@Volatile var files: Long = 0
		protected set
	@Volatile var bytes: Long = 0
		protected set

	override fun toString() = "Estimate($files files, ${fmtBytes(bytes)})"

	open fun estimate(scope: CoroutineScope) {
		if(files > 0)
			throw RuntimeException("Already estimated.")
		log.debug("Estimating files in {}", path)
		doEstimate(path, scope)
		log.debug("Result = {}", this)
	}

	private fun doEstimate(path: Path, scope: CoroutineScope) {
		scope.ensureActive()
		if(Files.isRegularFile(path)) {
			try {
				bytes += Files.size(path)
				files++
			} catch(e: FileNotFoundException) {
				log.warn("File disappeared: {}", path)
			}
		} else {
			log.trace("Estimating {}", path)
			Files.list(path).use { str -> str.forEach { doEstimate(it, scope) } }
		}
	}
}


/**Doesn't look at any files, but pretends it does for UI testing.*/
class FakeFileCounter (path: Path, private val timeMs: Long) : FileCounter(path) {
	override fun estimate(scope: CoroutineScope) {
		for(i in 0 until timeMs / 500) {
			for(j in 0 until 5) {
				scope.ensureActive()
				Thread.sleep(100)
				bytes += ThreadLocalRandom.current().nextLong(500)
			}
			files++
		}
	}
}