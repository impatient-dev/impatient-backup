package imp.backup.job

import imp.backup.data.*
import imp.sqlite.Database
import imp.util.FirstList
import imp.util.logger
import kotlinx.coroutines.CoroutineScope
import java.nio.file.Files
import java.nio.file.Path

private val log = PlanTogglePatchJob::class.logger

private val CONFLICT_FILES_TO_SHOW = 10

/**Figures out how many files would be involved with applying or removing a patch.
 * Also finds conflicts (if applying) or all applied patches that could replace removed files (if removing).*/
open class PlanTogglePatchJob (
	val location: PatchLocation,
	val patchName: String,
	val apply: Boolean,
) : AppJob {
	@Volatile var state = SimpleJobState.NOT_STARTED
		protected set
	/**Files to apply/remove, excluding conflicts.*/
	@Volatile var files = 0L
		protected set
	@Volatile var bytes = 0L
		protected set

	/**Set when the job finishes, but only if we're applying (not removing) a patch.*/
	@Volatile var conflicts: List<PatchConflict>? = null
		protected set
	private val conflictsByName = HashMap<String, PatchConflict>()

	/**Set when the job finishes, but only if we're removing (not applying) a patch.*/
	@Volatile var replacements: List<Patch>? = null
		protected set

	override suspend fun run(scope: CoroutineScope) {
		log.info("Planning patch {} in location {}.", patchName, location.name)
		state = SimpleJobState.RUNNING
		try {
			location.db().use { db ->
				if (apply)
					planApply(db)
				else
					planRemove(db)
			}
		} catch(e: Throwable) {
			throw Exception("Failed to plan to toggle patch $patchName in location ${location.name}", e)
		} finally {
			state = SimpleJobState.DONE
		}
	}


	private fun planApply(db: Database) {
		val patch = PatchRepo.getOrCreatePatch(patchName, db)
		PatchRepo.forEachToApply(patch, location, db, { doApply(it) }, { path, conflict -> doResolveApplyConflict(path, conflict) })
		conflicts = conflictsByName.values.toList()
	}

	private fun doApply(relative: Path) {
		log.trace("Found file to apply: {}", relative)
		files++
		bytes += Files.size(location.stagingDir.resolve(patchName).resolve(relative))
	}

	private fun doResolveApplyConflict(path: Path, otherPatch: Patch?): Boolean {
		if(otherPatch == null)
			return true
		val conflict = conflictsByName.computeIfAbsent(otherPatch.name) { PatchConflict(otherPatch.name, 0, FirstList(CONFLICT_FILES_TO_SHOW)) }
		conflict.files++
		conflict.someFiles.add(path)
		return true
	}


	private fun planRemove(db: Database) {
		val patch = patchOrm.query(patchTable).andWhere("name = ?").param(patchName).selectOnly(db) ?: throw Exception("Patch not found: $patchName")
		PatchRepo.forEachToRemove(patch, location, HashMap(), db) { relative, record ->
			log.trace("Found file to remove: {}", relative)
			files++
			bytes += Files.size(location.path.resolve(relative))
			false
		}
		replacements = patchOrm.query(patchTable).andWhere("applied = 1").selectAll(db)
	}
}