package imp.backup.job

import imp.backup.data.*
import imp.sqlite.Database
import imp.sqlite.withTransaction
import imp.util.logger
import kotlinx.coroutines.CoroutineScope
import java.nio.file.Files
import java.nio.file.Path

private val log = TogglePatchJob::class.logger

open class TogglePatchJob (
	val location: PatchLocation,
	val patchName: String,
	/**As opposed to remove.*/
	val apply: Boolean,
	/**Tells this job whether it's ok to replace applied files belonging to other patches.
	 * If this returns false, the job will fail.*/
	private val canReplaceConflict: (otherPatch: Patch) -> Boolean,
) : AppJob {
	@Volatile var state: SimpleJobState = SimpleJobState.NOT_STARTED
		protected set
	/**Files belonging to this patch that were added/removed.*/
	@Volatile var files: Long = 0
		protected set
	/**Files not belonging to this patch that were added/removed.*/
	@Volatile var otherFiles: Long = 0
		protected set

	override suspend fun run(scope: CoroutineScope) {
		log.info("{} patch {} in location {}.", if(apply) "Applying" else "Removing", patchName, location.name)
		state = SimpleJobState.RUNNING
		try {
			location.db().use { db ->
				//the planning job should create the database record
				val patch = patchOrm.query(patchTable).andWhere("name = ?").param(patchName).selectOnly(db) ?: throw Exception("Patch not found in database: $patchName")
				if (apply)
					doApply(patch, db)
				else
					doRemove(patch, db)

				patch.applied = apply
				if(apply)
					patchTable.update(patch, db)
				else
					patchTable.delete(patch, db)
			}
		} catch(e: Throwable) {
			throw Exception("Failed to toggle patch $patchName in location ${location.name}", e)
		} finally {
			state = SimpleJobState.DONE
		}
	}


	private fun doApply(patch: Patch, db: Database) {
		val patchDir = location.stagingDir.resolve(patch.name)
		PatchRepo.forEachToApply(patch, location, db, {doApplyFile(it, patch, patchDir, db)}, {path, conflictPatch -> doResolveConflict(path, conflictPatch, db)})
	}

	private fun doResolveConflict(relative: Path, patch: Patch?, db: Database): Boolean {
		if(patch != null && !canReplaceConflict(patch)) {
			log.debug("Skipping {} due to conflict with {}.", relative, patch)
			return false
		}

		db.withTransaction {
			val record = PatchRepo.getOrCreateFile(relative, patch?.id, true, db)
			val fromFile = location.path.resolve(relative)
			val toRoot = if(patch == null) location.baseFiles else location.stagingDir.resolve(patch.name)
			val toFile = toRoot.resolve(relative)

			log.debug("Moving {} --> {}.", fromFile, toFile)
			record.applied = false
			if(patch == null)
				patchFileTable.update(record, db)
			else
				patchFileTable.delete(record, db)
			Files.createDirectories(toFile.parent)
			Files.move(fromFile, toFile)
			otherFiles.inc()
		}
		otherFiles++
		return true
	}

	private fun doApplyFile(relative: Path, patch: Patch, patchDir: Path, db: Database) {
		db.withTransaction {
			val record = PatchRepo.getOrCreateFile(relative, patch.id, false, db)
			val fromFile = patchDir.resolve(relative)
			val toFile = location.path.resolve(relative)
			log.debug("Applying {} --> {}.", fromFile, toFile)
			Files.createDirectories(toFile.parent)
			Files.move(fromFile, toFile)
			record.applied = true
			patchFileTable.update(record, db)
			files.inc()
		}
		files++
	}


	private fun doRemove(patch: Patch, db: Database) {
		val patchDir = location.stagingDir.resolve(patch.name)
		PatchRepo.forEachToRemove(patch, location, HashMap(), db) { relative, record ->
			db.withTransaction {
				patchFileTable.delete(record, db)
				val fromFile = location.path.resolve(relative)
				val toFile = patchDir.resolve(relative)
				log.debug("Removing {} --> {}.", fromFile, toFile)
				Files.createDirectories(toFile.parent)
				Files.move(fromFile, toFile)
				files.inc()
			}
			true
		}
	}
}