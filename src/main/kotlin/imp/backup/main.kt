package imp.backup

import androidx.compose.ui.window.application
import imp.backup.cui.MainWindow
import imp.json.impJackson
import imp.logback.initLogbackCreateConfigIfMissing
import imp.util.assertWorkingDirContains
import imp.util.defaultScope
import imp.util.fmt.TOD12
import imp.util.logger
import java.nio.file.Paths
import kotlin.system.exitProcess

val jsonMapper = impJackson()
val todFmt = TOD12

fun main() {
	assertWorkingDirContains("src/main/kotlin", "impatient-backup.jar")
	initLogbackCreateConfigIfMissing(Paths.get("logback.xml"), "/logback-default.xml", MainWindow::class)
	val log = logger("imp.backup.main")

	log.debug("Starting UI.")
	val window = MainWindow()
	application { window.compose() }
}


/**General background scope for doing things that shouldn't be cancelled.*/
val backScope = defaultScope()

/**Exits the program. This should eventually be replaced with something that shows the error in the UI.*/
fun onUnhandledBackgrounderror(e: Throwable) {
	e.printStackTrace()
	exitProcess(-1)
}