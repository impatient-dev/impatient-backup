package imp.backup.repo

import androidx.compose.runtime.Immutable
import imp.backup.crypto.SymKey
import imp.backup.data.*
import imp.util.impDirList
import imp.util.logger
import kotlinx.coroutines.CoroutineScope
import java.time.Instant

private val log = BackupRepo::class.logger

@Immutable data class BackupI (
	/**Where the backup is from.*/
	val userLocationName: String?,
	/**Where the backup is to.*/
	val backupLocationName: String,
	/**Uniquely identifies this backup among other backups of the same type in this backup location.*/
	val name: String,
	val type: BackupType,
	val lastStart: Instant?,
	val lastEnd: Instant?,
	val files: Long?,
	val bytes: Long?,
	val key: SymKey?,
	val backup: BackupDecl,
)



/**Saves and loads information about individual backups.
 * All backups returned by this class have been initialized.*/
object BackupRepo {
	suspend fun getAllTo(location: BackupLocation): List<BackupI> {
		log.debug("Loading backups for {} in {}.", location.name, location.path)
		val out = ArrayList<BackupDecl>()
		location.rawBackupDir.impDirList { path ->
			out.add(RawBackupDecl(path, location.name))
		}
		location.dbBackupDir.impDirList { path ->
			out.add(DbBackupDecl(path, location.name))
		}
		log.debug("Found {} backups to {}.", out.size, location.name)
		return out.map { backup ->
			val info = backup.info()
			BackupI(info?.fromLocationName, backup.locationName, backup.name, backup.type, info?.lastStart, info?.lastEnd, info?.files, info?.bytes, info?.key, backup)
		}
	}


	suspend fun getAllFrom(location: UserLocation, scope: CoroutineScope): List<BackupI> {
		val locations = LocationRepo.getAll()
		log.debug("Looking for all backups to {} in {} locations.", location.name, locations.size)
		val out = ArrayList<BackupI>()
		for(b in locations) {
			if(b !is BackupLocation)
				continue
			for(backup in getAllTo(b))
				if(backup.userLocationName == location.name)
					out.add(backup)
		}
		log.debug("Found {} backups from {}.", out.size, location.name)
		return out
	}
}