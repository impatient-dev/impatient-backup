package imp.backup.repo

import imp.util.logger
import imp.util.minusAt
import imp.util.plus1
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Path
import kotlin.io.path.writeLines

/**Keeps track of all paths where locations are supposed to be. LocationRepo is notified about any changes.*/
object LocationPathsRepo {
	private val log = LocationPathsRepo::class.logger
	private val mutex = Mutex()
	private val file = Path.of("locations.txt")
	private var _paths: List<Path>? = null

	private fun loadPaths(): List<Path> {
		log.debug("Loading location paths to try from {}.", file)
		val strings = if(Files.exists(file)) Files.readAllLines(file, StandardCharsets.UTF_8) else emptyList()
		val pList = ArrayList<Path>()
		for(str in strings) {
			try {
				pList.add(Path.of(str))
			} catch(e: Exception) {
				log.error("Invalid path: $str.", e)
			}
		}
		return pList
	}

	/**Acquires the lock, loads the paths if they aren't already loaded, then calls the lambda.*/
	private suspend fun <T> lockedAndLoaded(block: (List<Path>) -> T): T = mutex.withLock {
		var p = _paths
		if(p == null) {
			p = loadPaths()
			_paths = p
		}
		return block(p)
	}

	suspend fun list(): List<Path> = lockedAndLoaded { it }


	suspend fun add(path: Path) = lockedAndLoaded { paths ->
		if(!paths.contains(path)) {
			log.debug("Adding new path: {}.", path)
			val p = paths.plus1(path)
			file.writeLines(p.map { it.toString() })
			_paths = p
			LocationRepo.onAddPath(path)
			log.trace("Path added.")
		}
	}

	suspend fun remove(path: Path) = lockedAndLoaded { paths ->
		val i = paths.indexOf(path)
		if(i == -1) {
			log.warn("Path not found to remove: {}.", path)
		} else {
			log.debug("Removing path {}.", path)
			val p = paths.minusAt(i)
			file.writeLines(p.map { it.toString() })
			_paths = p
			LocationRepo.onRemovePath(path)
			log.trace("Path removed.")
		}
	}
}