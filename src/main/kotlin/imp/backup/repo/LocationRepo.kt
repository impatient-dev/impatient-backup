package imp.backup.repo

import imp.backup.data.BackupLocation
import imp.backup.data.Location
import imp.backup.data.UserLocation
import imp.backup.data.loadLocation
import imp.util.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import java.nio.file.Path

private val log = LocationRepo::class.logger

/**A breakdown of all locations by type. Immutable.*/
class LocationBreakdown (all: List<Location>) {
	val user: List<UserLocation>
	val backup: List<BackupLocation>

	init {
		val u = ArrayList<UserLocation>()
		val b = ArrayList<BackupLocation>()
		for(location in all) {
			if(location is UserLocation)
				u.add(location)
			else if(location is BackupLocation)
				b.add(location)
		}
		user = u
		backup = b
	}
}



/**Keeps track of all user/backup/etc. locations. LocationPathsRepo updates this one when the list of location paths changes.*/
object LocationRepo {
	@Volatile private var loaded = false
	private val mutex = Mutex()
	private val scope = ioScope()

	/**Immutable - copied on change.*/
	private var byPath: Map<Path, Location> = HashMap()
	/**Immutable - copie on change.*/
	private var all = emptyList<Location>()
	private var breakdown = LocationBreakdown(all)


	suspend fun refresh(): List<Location> = mutex.withLock {
		val tryPaths = LocationPathsRepo.list()
		val newMap = tryPaths.associateWith { path -> loadLocation(path) }
		diff(byPath, newMap,
			onAdd = { _, location -> _listeners.fireAsync { it.postAddLocation(location) }},
			onRemove = { _, location -> _listeners.fireAsync { it.postRemoveLocation(location) }},
			onModify = { _, a, b ->
				if(a.javaClass == b.javaClass) _listeners.fireAsync { it.postChangeLocation(a, b) }
				else _listeners.fireAsync { it.postRemoveLocation(a); it.postAddLocation(b) }
			}
		)
		byPath = newMap
		all = ArrayList(newMap.values)
		breakdown = LocationBreakdown(all)
		loaded = true
		log.debug("Done refreshing locations - produced {}.", all.size)
		all
	}


	suspend fun getAll(): List<Location> = mutex.withLock { if(loaded) all else null } ?: refresh()

	/**Returns all locations, broken down by type.*/
	suspend fun getBreakdown(): LocationBreakdown {
		mutex.withLock {
			if(!loaded)
				refresh()
			return breakdown
		}
	}

	/**Returns the only backup location with this name. Returns null if there are 0 or multiple matching locations.*/
	suspend fun getBackupIf1(name: String): BackupLocation? = getBreakdown().backup.find1 { it.name == name }
	/**Returns the only user location with this name. Returns null if there are 0 or multiple matching locations.*/
	suspend fun getUserIf1(name: String): UserLocation? = getBreakdown().user.find1 { it.name == name }

	/**Called by LocationPathsRepo when there's a new path we should try to find a location at. Returns quickly.*/
	fun onAddPath(path: Path) {
		scope.launch { refresh() }
	}
	/**Called by LocationPathsRepo when there's a new path we should try to find a location at. Returns quickly.*/
	fun onRemovePath(path: Path) {
		scope.launch { refresh() }
	}


	private val _listeners = BasicCoListeners<Listener>()
	val listeners get() = _listeners.toInterface
	interface Listener {
		suspend fun postAddLocation(location: Location) {}
		suspend fun postRemoveLocation(location: Location) {}
		/**Called when some details of a location change, but is still the same type. If the location changes type (e.g. user -> missing), this is handled as a remove + add.*/
		suspend fun postChangeLocation(old: Location, new: Location) {}
	}
}