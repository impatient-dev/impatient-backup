package imp.backup.files

import imp.backup.net.*
import imp.util.EMPTY_BYTE_ARRAY
import imp.web.ImpWs
import java.nio.file.Path
import java.time.Instant
import kotlin.io.path.Path

/**Reads and writes files for some location over a websocket.*/
class RemoteRawEntryHandler (
	val ws: ImpWs,
	val tgt: SyncTgtJ,
) : RawLocEntryHandler {
	override suspend fun getRootDir(createIfMissing: Boolean): RawLocEntry? {
		val out = RemoteRootDirExchange.request(tgt, ws, createIfMissing)
		return RawLocEntry(EMPTY_BYTE_ARRAY, null, null, Path(""), out.id, null, null, null, null)
	}

	override suspend fun listDir(dir: RawLocEntry): List<RawLocEntry> {
		val out = RemoteDirListExchange.request(tgt, LocEntryIdJ(dir.path, dir.id), ws)
		return out.map { RawLocEntry(it.name, it.nameIv, it.contentIv, it.path, it.id, dir.id, it.bytes, it.symlinkTo, it.lastModified) }
	}

	override suspend fun createDir(parent: RawLocEntry, name: ByteArray, nameIv: ByteArray?, path: Path?): RawLocEntry {
		val out = RemoteDirCreateExchange.request(tgt, LocEntryIdJ(parent.path, parent.id), name, nameIv, ws)
		return RawLocEntry(out.name, out.nameIv, out.contentIv, out.path, out.id, parent.id, out.bytes, null, out.lastModified)
	}

	override suspend fun createSymlink(parent: RawLocEntry, name: ByteArray, nameIv: ByteArray?, target: ByteArray, targetIv: ByteArray?): RawLocEntry {
		val out = RemoteSymlinkCreateExchange.request(tgt, LocEntryIdJ(parent.path, parent.id), name, nameIv, target, targetIv, ws)
		return RawLocEntry(out.name, out.nameIv, out.contentIv, out.path, out.id, parent.id, null, out.symlinkTo, null)
	}

	override suspend fun delete(path: Path?, id: Long?) {
		RemoteDeleteExchange.request(tgt, LocEntryIdJ(path, id), ws)
	}

	override suspend fun readFile(file: RawLocEntry): LocFileInputStream {
		TODO("Not yet implemented")
	}

	override suspend fun writeFile(parent: RawLocEntry, name: ByteArray, nameIv: ByteArray?, path: Path?, bytes: Long, lastModified: Instant): RawLocFileOutputStream {
		TODO("Not yet implemented")
	}
}