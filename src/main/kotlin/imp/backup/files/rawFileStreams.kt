package imp.backup.files

import imp.util.impFileOut
import imp.util.logger
import kotlinx.coroutines.CoroutineScope
import java.io.FileInputStream
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.attribute.FileTime
import java.time.Instant

private val log = RawFileLocOutputStream::class.logger


/**Reads bytes from a file.*/
class RawFileLocInputStream (
	val path: Path,
) : LocFileInputStream {
	private val stream = FileInputStream(path.toFile())

	override suspend fun read(buffer: ByteArray, scope: CoroutineScope): Int {
		return stream.read(buffer)
	}

	override fun close() = stream.close()
}


/**Writes bytes into a file, and sets the last-modified time once done.*/
class RawFileLocOutputStream (
	val path: Path,
	val lastModified: Instant,
) : RawLocFileOutputStream {
	private val stream = path.impFileOut()
	private var success = false

	override suspend fun write(buffer: ByteArray, scope: CoroutineScope, count: Int) = stream.write(buffer, 0, count)

	override suspend fun finish(iv: ByteArray?, scope: CoroutineScope) {
		check(iv == null) { "Encryption not supported" }
		success = true
	}

	override fun close() {
		stream.close()
		if(success) {
			Files.setLastModifiedTime(path, FileTime.from(lastModified)) // must be done after the close - close modifies the time
		} else {
			log.warn("Incomplete file closed - deleting {}.", path)
			Files.deleteIfExists(path)
		}
	}
}