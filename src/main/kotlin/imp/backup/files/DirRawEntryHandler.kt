package imp.backup.files

import imp.backup.util.decodeSymlink
import imp.backup.util.encodeSymlink
import imp.backup.util.symlinkTgt
import imp.util.EMPTY_BYTE_ARRAY
import imp.util.checkedResolve
import imp.util.impDirList
import imp.util.logger
import org.apache.commons.io.FileUtils
import java.nio.file.Files
import java.nio.file.Path
import java.time.Instant
import kotlin.io.path.Path
import kotlin.io.path.isRegularFile
import kotlin.io.path.isSymbolicLink
import kotlin.io.path.name

private val log = DirRawEntryHandler::class.logger

/**A basic handler that reads and writes files in some directory (and subdirectories).
 * The root directory must be an actual location on disk, not a location-relative path like "".*/
class DirRawEntryHandler (
	val root: Path,
) : RawLocEntryHandler {
	override suspend fun getRootDir(createIfMissing: Boolean) = RawLocEntry(EMPTY_BYTE_ARRAY, null, null, Path(""), null, null, null, null, null)

	override suspend fun listDir(dir: RawLocEntry): List<RawLocEntry> {
		check(dir.path != null)
		val out = ArrayList<RawLocEntry>()
		root.checkedResolve(dir.path).impDirList { full ->
			val relative = root.relativize(full)
			val symlinkTo = full.symlinkTgt()?.encodeSymlink()
			val bytes = if(symlinkTo != null || Files.isDirectory(full)) null else Files.size(full)
			val lastModified = if(bytes == null) null else Files.getLastModifiedTime(full).toInstant()
			out.add(RawLocEntry(relative.name.toByteArray(), null, null, relative, null, null, bytes, symlinkTo, lastModified))
		}
		return out
	}

	override suspend fun createDir(parent: RawLocEntry, name: ByteArray, nameIv: ByteArray?, path: Path?): RawLocEntry {
		check(parent.isDir) { "not a directory: $parent" }
		check(parent.path != null) { "parent missing path: $parent" }
		check(nameIv == null) { "Encryption is not supported." }
		val relative = parent.path.resolve(String(name))
		val fullPath = root.checkedResolve(relative)
		log.debug("Creating dir {}.", fullPath)
		Files.createDirectories(fullPath)
		return RawLocEntry(name, null, null, relative, null, null, null, null, null)
	}

	override suspend fun createSymlink(parent: RawLocEntry, name: ByteArray, nameIv: ByteArray?, target: ByteArray, targetIv: ByteArray?): RawLocEntry {
		check(parent.isDir) { "not a directory: $parent" }
		check(parent.path != null) { "parent missing path: $parent" }
		check(nameIv == null && targetIv == null) { "Encryption is not supported." }
		val relative = parent.path.resolve(String(name))
		val fullPath = root.checkedResolve(relative)
		val tgt = target.decodeSymlink()
		log.debug("Creating symlink at {} to {}.", fullPath, tgt)
		Files.createSymbolicLink(fullPath, tgt)
		return RawLocEntry(name, null, null, relative, null, null, null, target, null)
	}

	override suspend fun delete(path: Path?, id: Long?) {
		check(path != null)
		val resolved = root.checkedResolve(path)
		if(resolved.isSymbolicLink() || resolved.isRegularFile()) {
			log.debug("Deleting file {}.", resolved)
			Files.deleteIfExists(resolved)
		} else {
			log.debug("Deleting dir {}.", resolved)
			FileUtils.deleteDirectory(resolved.toFile())
		}
	}

	override suspend fun readFile(file: RawLocEntry): LocFileInputStream {
		check(file.isFile && file.path != null)
		return RawFileLocInputStream(root.checkedResolve(file.path))
	}

	override suspend fun writeFile(parent: RawLocEntry, name: ByteArray, nameIv: ByteArray?, path: Path?, bytes: Long, lastModified: Instant): RawLocFileOutputStream {
		check(parent.isDir && parent.path != null)
		val resolved = root.checkedResolve(parent.path.resolve(String(name)))
		return RawFileLocOutputStream(resolved, lastModified)
	}
}