package imp.backup.files

import androidx.compose.runtime.Immutable
import kotlinx.coroutines.CoroutineScope
import java.nio.file.Path
import java.time.Instant
import kotlin.io.path.name


/**A thing that can be the source or destination in a sync. A thing full of files that can be read and written.*/
interface SyncTarget {
	/**A user-readable path explaining where this is.*/
	val userPath: String
	/**The name of the location this represents or is in (not the location this is a backup from, if this is a backup).*/
	val locationName: String?
	val raw: RawLocEntryHandler
	/**Null if we're unable to read/write full entries (e.g. because we don't know the encryption key).*/
	val full: FullLocEntryHandler?
	/**Tries to throw an informative message if we can't return a handler.*/
	fun reqFull(): FullLocEntryHandler
}


enum class EntryType {
	DIR,
	FILE,
	/**Symbolic link.*/
	SYMLINK,
}


/**An abstraction over a file or directory or symlink in a location or backup.
 * This is the "raw" variant, where the name may be encrypted and we may not know the full path.
 * This is only a data class so that copy() is generated - equals() will not work due to the ByteArray members.*/
@Immutable data class RawLocEntry (
	val name: ByteArray,
	val nameIv: ByteArray?,
	/**If this is an encrypted backup, this is the initialization vector used to encrypt the content (for a file) or target (for a symlink).*/
	val contentIv: ByteArray?,
	/**Relative to the location root.*/
	val path: Path?,
	/**Required if the path is null.*/
	val id: Long?,
	/**Null if the location or backup doesn't use IDs, or if it does but this entry represents the top-level directory in the location or backup.*/
	val parentId: Long?,
	/**File content size. Null for a directory or symlink.
	 * If the file is encrypted, this is the size of the plaintext (because that's more useful for comparisons).*/
	val bytes: Long?,
	/**If this is a symlink, this is the (possibly encrypted) path it targets.*/
	val symlinkTo: ByteArray?,
	/**Null for a directory or symlink.*/
	val lastModified: Instant?,
) {
	val type: EntryType
	inline val isDir get() = type == EntryType.DIR
	inline val isFile get() = type == EntryType.FILE
	inline val isSymlink get() = type == EntryType.SYMLINK


	init {
		check(path != null || id != null) { "No identifying info" }
		check(path == null || (nameIv == null && contentIv == null)) { "We shouldn't have the path if initialization vectors are present" }
		check(path == null || !path.isAbsolute) { "Path is absolute: $path" }
		type = if(symlinkTo != null) EntryType.SYMLINK
			else if(bytes == null) EntryType.DIR else EntryType.FILE
	}
}


/**An abstraction over a file or directory or symlink in a location or backup.
 * This is the "full" variant, where the name has been decrypted (if necessary) and the full path is known.
 * Equals and hashCode may not work correctly due to the array members.*/
@Immutable data class FullLocEntry (
	/**Relative to the location root.*/
	val path: Path,
	val id: Long?,
	val parentId: Long?,
	/**File content size; null for a directory or symlilnk.*/
	val bytes: Long?,
	/**If this entry is a symlink, this is where it points.*/
	val symlinkTo: Path?,
	/**Null for a directory or symlink.*/
	val lastModified: Instant?,
	/**The initialization vector that the name is encrypted with when sending it to the raw handler, if any.*/
	val nameIv: ByteArray?,
	/**The initialization vector that is used to encrypt the content (for a file) or target (for a symlink).*/
	val contentIv: ByteArray?,
) {
	val name = path.name

	val type: EntryType
	inline val isDir get() = type == EntryType.DIR
	inline val isFile get() = type == EntryType.FILE
	inline val isSymlink get() = type == EntryType.SYMLINK

	init {
		check(!path.isAbsolute) { "Path is absolute: $path" }
		type = if(symlinkTo != null) EntryType.SYMLINK
			else if(bytes == null) EntryType.DIR else EntryType.FILE
	}
}




/**Reads and writes raw location entries for a location or backup.*/
interface RawLocEntryHandler {
	suspend fun getRootDir(createIfMissing: Boolean = false): RawLocEntry?
	/**Lists all entries in a directory. The order is undefined.*/
	suspend fun listDir(dir: RawLocEntry): List<RawLocEntry>
	/**There must not be an entry at this path already.
	 * The full path argument is optional, but may make things more efficient if the name is not encrypted (depending on location/backup type).*/
	suspend fun createDir(parent: RawLocEntry, name: ByteArray, nameIv: ByteArray?, path: Path?): RawLocEntry
	/**There must not be an entry at this path already.*/
	suspend fun createSymlink(parent: RawLocEntry, name: ByteArray, nameIv: ByteArray?, target: ByteArray, targetIv: ByteArray?): RawLocEntry
	/**Deletes a file or directory or symlink, if it exists. The directory doesn't have to be empty. The symlink target isn't affected.*/
	suspend fun delete(path: Path?, id: Long?)
	/**Reads bytes, possibly encrypted.*/
	suspend fun readFile(file: RawLocEntry): LocFileInputStream
	/**Creates a file and returns a stream that writes the (possibly encrypted) bytes.
	 * For an encrypted file, the content initialization vector is decided later, and the file will need to be updated with the IV when the stream finishes.*/
	suspend fun writeFile(parent: RawLocEntry, name: ByteArray, nameIv: ByteArray?, path: Path?, bytes: Long, lastModified: Instant): RawLocFileOutputStream
}

/**Reads and writes full location entries for a location or backup.*/
interface FullLocEntryHandler {
	/**The raw handler that is wrapped by this handler.*/
	val raw: RawLocEntryHandler
	suspend fun getRootDir(createIfMissing: Boolean = false): FullLocEntry?
	/**Lists all entries in a directory. The order is undefined.*/
	suspend fun listDir(dir: FullLocEntry): List<FullLocEntry>
	/**There must not be a file or directory at this path already.
	 * The full path argument is optional, but may make things more efficient (depending on location/backup type).*/
	suspend fun createDir(parent: FullLocEntry, name: String, path: Path?): FullLocEntry
	/**There must not be an entry at this path already.*/
	suspend fun createSymlink(parent: FullLocEntry, name: String, target: Path): FullLocEntry
	/**Deletes a file or directory or symlink, if it exists. The directory doesn't have to be empty. The symlink target isn't affected.*/
	suspend fun delete(entry: FullLocEntry)
	/**Reads plaintext bytes.*/
	suspend fun readFile(file: FullLocEntry): LocFileInputStream
	/**Creates the file and returns a stream that receives the plaintext bytes.*/
	suspend fun writeFile(parent: FullLocEntry, name: String, bytes: Long, lastModified: Instant): FullLocFileOutputStream
}

/**Writes a new file that doesn't currently exist.
 * It is acceptable for the file argument to be something from a different location or backup - it is only accessed for the necessary metadata, such as last-modified time, which may need to be saved.*/
suspend fun FullLocEntryHandler.writeFile(parent: FullLocEntry, file: FullLocEntry) : FullLocFileOutputStream {
	check(parent.isDir && file.isFile && file.bytes != null && file.lastModified != null)
	return writeFile(parent, file.name, file.bytes, file.lastModified)
}



/**A stream that reads bytes from a file.*/
interface LocFileInputStream : AutoCloseable {
	/**Reads bytes into the array, and returns the number of bytes written.
	 * Returns -1 if we've reached the end of the stream and there are no more bytes.*/
	suspend fun read(buffer: ByteArray, scope: CoroutineScope): Int
}

/**A stream that appends bytes to a file, for a raw (possibly encrypted) location entry.*/
interface RawLocFileOutputStream : AutoCloseable {
	/**Appends some bytes. After this function returns, you may reuse the buffer.*/
	suspend fun write(buffer: ByteArray, scope: CoroutineScope, count: Int = buffer.size)
	/**Indicates that all bytes have been written to the file, and provides the initialization vector that was used to encrypt them, if any.
	 * If this stream is closed before the file is finished, the file will be assumed incomplete and deleted.*/
	suspend fun finish(iv: ByteArray?, scope: CoroutineScope)
}

/**A stream that appends bytes to a file, for a full (plaintext) location entry.*/
interface FullLocFileOutputStream : AutoCloseable {
	/**Appends some bytes. After this function returns, you may reuse the buffer.
	 * The size of the array should be the size recommended by the writer.*/
	suspend fun write(buffer: ByteArray, scope: CoroutineScope, count: Int = buffer.size)
	/**Indicates that all bytes have been written to the file.
	 * If this stream is closed before the file is finished, the file will be assumed incomplete and deleted.*/
	suspend fun finish(scope: CoroutineScope)
}