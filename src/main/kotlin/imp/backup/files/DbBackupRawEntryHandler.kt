package imp.backup.files

import imp.backup.data.DbBackupEntry
import imp.backup.data.DbBackupImpl
import imp.backup.data.dbTableEntry
import imp.sqlite.Database
import imp.sqlite.withTransaction
import imp.util.EMPTY_BYTE_ARRAY
import imp.util.logger
import java.nio.file.Files
import java.nio.file.Path
import java.time.Instant

private val log = DbBackupRawEntryHandler::class.logger

/**Handles files in a database-based backup.*/
class DbBackupRawEntryHandler(
	val backup: DbBackupImpl,
) : RawLocEntryHandler {
	override suspend fun getRootDir(createIfMissing: Boolean): RawLocEntry? {
		var record = backup.db { db -> dbTableEntry.query().andWhere("parentId IS NULL").selectOnly(db) }
		if(record == null && createIfMissing) {
			record = DbBackupEntry(-1, EMPTY_BYTE_ARRAY, null, null, null, null, null, null)
			backup.db { db -> dbTableEntry.insert(record, db) }
		}
		return record?.toRaw()
	}

	override suspend fun listDir(dir: RawLocEntry): List<RawLocEntry> {
		check(dir.isDir && dir.id != null)
		return backup.db { db ->
			dbTableEntry.query().andWhere("parentId = ?").param(dir.id).selectAll(db)
		}.map { it.toRaw() }
	}

	override suspend fun createDir(parent: RawLocEntry, name: ByteArray, nameIv: ByteArray?, path: Path?): RawLocEntry {
		val record = DbBackupEntry(
			id = -1,
			name = name,
			parentId = parent.id,
			bytes = null,
			nameIv = nameIv,
			contentIv = null,
			symlinkTo = null,
			lastModifiedMs = null,
		)
		backup.db { db -> dbTableEntry.insert(record, db) }
		return record.toRaw()
	}

	override suspend fun createSymlink(parent: RawLocEntry, name: ByteArray, nameIv: ByteArray?, target: ByteArray, targetIv: ByteArray?): RawLocEntry {
		val record = DbBackupEntry(
			id = -1,
			name = name,
			parentId = parent.id,
			bytes = null,
			symlinkTo = target,
			nameIv = nameIv,
			contentIv = targetIv,
			lastModifiedMs = null,
		)
		backup.db { db -> dbTableEntry.insert(record, db) }
		return record.toRaw()
	}

	override suspend fun delete(path: Path?, id: Long?) {
		check(id != null)
		backup.db { db ->
			val record = dbTableEntry.query().andWhere("id = ?").param(id).selectOnly(db)
			if(record != null) {
				if(record.isFile())
					deleteFile(record, db)
				else
					recursivelyDeleteDir(record, db)
			}
		}
	}

	override suspend fun readFile(file: RawLocEntry): LocFileInputStream {
		check(file.isFile && file.id != null)
		backup.db { db ->
			val record = dbTableEntry.query().andWhere("id = ?").param(file.id).selectOnly(db) ?: throw Exception("File not found: ${file.id}")
			check(record.isFile()) { "Not a file according to the database: ${file.id}" }
			return RawFileLocInputStream(backup.decl.file(record))
		}
	}

	override suspend fun writeFile(parent: RawLocEntry, name: ByteArray, nameIv: ByteArray?, path: Path?, bytes: Long, lastModified: Instant): RawLocFileOutputStream {
		check(parent.isDir && parent.id != null && parent.id != -1L)
		val record = DbBackupEntry(
			id = -1,
			name = name,
			parentId = parent.id,
			bytes = bytes,
			nameIv = nameIv,
			contentIv = null,
			symlinkTo = null,
			lastModifiedMs = lastModified.toEpochMilli(),
		)
		backup.db { db ->
			dbTableEntry.insert(record, db)
			return RawFileLocOutputStream(backup.decl.file(record), lastModified)
		}
	}


	private fun recursivelyDeleteDir(dir: DbBackupEntry, db: Database) {
		log.trace("Deleting dir {}.", dir.id)
		assert(dir.isDir())
		val children = dbTableEntry.query().andWhere("parentId = ?").param(dir.id).selectAll(db)
		for(child in children) {
			if(child.isDir())
				recursivelyDeleteDir(child, db)
			else
				deleteFile(child, db)
		}
	}

	private fun deleteFile(file: DbBackupEntry, db: Database) = db.withTransaction {
		log.trace("Deleting file {}.", file.id)
		assert(file.isFile())
		dbTableEntry.deletePk(file.id, db)
		Files.deleteIfExists(backup.decl.file(file))
	}
}



private fun DbBackupEntry.toRaw() = RawLocEntry(
	name = name,
	nameIv = nameIv,
	contentIv = contentIv,
	path = null,
	id = id,
	parentId = parentId,
	bytes = bytes,
	symlinkTo = symlinkTo,
	lastModified = lastModifiedMs?.let(Instant::ofEpochMilli),
)

private fun RawLocEntry.toRecord() = DbBackupEntry(
	id = id ?: -1,
	name = name,
	parentId = parentId,
	bytes = bytes,
	nameIv = nameIv,
	contentIv = contentIv,
	symlinkTo = symlinkTo,
	lastModifiedMs = lastModified?.toEpochMilli(),
)