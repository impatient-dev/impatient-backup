package imp.backup.files

import java.nio.file.Path
import java.time.Instant

/**Throws an exception if any attempt is made to write.
 * Wrapping a handler in this class allows you to be extra-sure you don't accidentally change any files.*/
class NonWriteableFullEntryHandler (
	private val wrap: FullLocEntryHandler
) : FullLocEntryHandler {
	override val raw get() = wrap.raw
	override suspend fun getRootDir(createIfMissing: Boolean): FullLocEntry? {
		check(!createIfMissing) { "This is not writeable." }
		return wrap.getRootDir(createIfMissing)
	}

	override suspend fun listDir(dir: FullLocEntry) = wrap.listDir(dir)
	override suspend fun readFile(file: FullLocEntry) = wrap.readFile(file)

	override suspend fun createDir(parent: FullLocEntry, name: String, path: Path?): FullLocEntry {
		throw UnsupportedOperationException("This is not writeable.")
	}
	override suspend fun createSymlink(parent: FullLocEntry, name: String, target: Path): FullLocEntry {
		throw UnsupportedOperationException("This is not writeable.")
	}
	override suspend fun delete(entry: FullLocEntry) {
		throw UnsupportedOperationException("This is not writeable.")
	}
	override suspend fun writeFile(parent: FullLocEntry, name: String, bytes: Long, lastModified: Instant): FullLocFileOutputStream {
		throw UnsupportedOperationException("This is not writeable.")
	}
}