package imp.backup.files

import imp.backup.util.decodeSymlink
import imp.backup.util.encodeSymlink
import kotlinx.coroutines.CoroutineScope
import java.nio.file.Path
import java.time.Instant
import kotlin.io.path.Path

/**An object that simply wraps a raw handler, where no encryption or decryption is needed.*/
class PlaintextFullEntryHandler (
	override val raw: RawLocEntryHandler,
) : FullLocEntryHandler {
	override suspend fun getRootDir(createIfMissing: Boolean) = raw.getRootDir(createIfMissing)?.enrich(null)
	override suspend fun listDir(dir: FullLocEntry): List<FullLocEntry> = raw.listDir(dir.raw()).map { it.enrich(dir) }
	override suspend fun createDir(parent: FullLocEntry, name: String, path: Path?)
		= raw.createDir(parent.raw(), name.toByteArray(), null, path).enrich(parent)
	override suspend fun createSymlink(parent: FullLocEntry, name: String, target: Path)
		= raw.createSymlink(parent.raw(), name.toByteArray(), null, target.encodeSymlink(), null).enrich(parent)
	override suspend fun delete(entry: FullLocEntry) = raw.delete(entry.path, entry.id)
	override suspend fun readFile(file: FullLocEntry) = raw.readFile(file.raw())
	override suspend fun writeFile(parent: FullLocEntry, name: String, bytes: Long, lastModified: Instant): FullLocFileOutputStream {
		check(parent.isDir)
		val path = parent.path.resolve(name)
		val out = raw.writeFile(parent.raw(), name.toByteArray(), null, path, bytes, lastModified)
		return PlaintextLocFileOutputStream(out)
	}
}



private class PlaintextLocFileOutputStream(private val raw: RawLocFileOutputStream) : FullLocFileOutputStream {
	override suspend fun write(buffer: ByteArray, scope: CoroutineScope, count: Int) = raw.write(buffer, scope, count)
	override suspend fun finish(scope: CoroutineScope) = raw.finish(null, scope)
	override fun close() = raw.close()
}



private fun RawLocEntry.enrich(parent: FullLocEntry?): FullLocEntry {
	assert(if(parent == null) parentId == null else parent.id == parentId)
	val p = if(path != null) path else {
		val n = String(name)
		if(parent == null) Path(n) else parent.path.resolve(n)
	}
	return FullLocEntry(
		path = p,
		id = id,
		parentId = parentId,
		bytes = bytes,
		symlinkTo = symlinkTo?.decodeSymlink(),
		lastModified = lastModified,
		null, null,
	)
}

private fun FullLocEntry.raw() = RawLocEntry(
	name = name.toByteArray(),
	nameIv = null,
	contentIv = null,
	path = path,
	id = id,
	parentId = parentId,
	bytes = bytes,
	symlinkTo = symlinkTo?.encodeSymlink(),
	lastModified = lastModified,
)