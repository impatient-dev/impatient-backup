package imp.backup.files

import imp.backup.crypto.*
import imp.backup.util.decodeSymlink
import imp.util.EMPTY_BYTE_ARRAY
import imp.util.closeOnFailR
import java.nio.file.Path
import java.time.Instant
import kotlin.io.path.Path

/**An object that wraps a raw handler.
 * This object encrypts all names and content sent to the raw handler, and decrypts them when reading from the raw handler.
 * This object is responsible for ensuring that the raw handler (which may be on a less-trusted server) never sees any plaintext paths or content.
 *
 * This class assumes it is working with a (local or remote) database-based backup; it will not work with a raw backup or a user location.
 * With this assumption, this class sometimes sends an empty byte array instead of the actual encrypted name to the raw layer.
 * It does this in situations where the raw layer doesn't actually need and won't use the name, avoiding some pointless encryption.
 * (This works because database-based backups only reference things by IDs; the names are just metadata in such backups.)*/
class CryptoFullEntryHandler (
	private val key: SymKey,
	override val raw: RawLocEntryHandler,
) : FullLocEntryHandler {
	init {
		check(key.key != null) { "Key is missing: ${key.uuid}" }
	}

	override suspend fun getRootDir(createIfMissing: Boolean): FullLocEntry? {
		val out = raw.getRootDir(createIfMissing)
		if(out != null)
			return FullLocEntry(Path(""), out.id, null, null, null, null, null, null)
		return null
	}

	override suspend fun listDir(dir: FullLocEntry): List<FullLocEntry> {
		assert(dir.isDir) { "Not a directory: $dir" }
		val rawDir = RawLocEntry(EMPTY_BYTE_ARRAY, null, null, null, dir.id, dir.parentId, null, null, null)
		return raw.listDir(rawDir).map { re ->
			assert(dir.id == re.parentId)
			val name = decrypt(key, re.name, re.nameIv)
			FullLocEntry(dir.path.resolve(name), re.id, re.parentId, re.bytes, re.symlinkTo?.decodeSymlink(), re.lastModified, re.nameIv, re.contentIv)
		}
	}

	override suspend fun createDir(parent: FullLocEntry, name: String, path: Path?): FullLocEntry {
		val rawParent = RawLocEntry(EMPTY_BYTE_ARRAY, null, null, null, parent.id, parent.parentId, null, null, null)
		val cname = encrypt(key, name)
		val rawDir = raw.createDir(rawParent, cname.ciphertext, cname.iv, null)
		assert(rawDir.nameIv === cname.iv)
		return FullLocEntry(parent.path.resolve(name), rawDir.id, parent.id, null, null, null, rawDir.nameIv, rawDir.contentIv)
	}

	override suspend fun createSymlink(parent: FullLocEntry, name: String, target: Path): FullLocEntry {
		val rawParent = RawLocEntry(EMPTY_BYTE_ARRAY, null, null, null, parent.id, parent.parentId, null, null, null)
		val cname = encrypt(key, name)
		val ctgt = encrypt(key, target.toString())
		val rawLink = raw.createSymlink(rawParent, cname.ciphertext, cname.iv, ctgt.ciphertext, ctgt.iv)
		assert(rawLink.nameIv === cname.iv)
		assert(rawLink.contentIv === ctgt.iv)
		return FullLocEntry(parent.path.resolve(name), rawLink.id, parent.id, null, target, null, rawLink.nameIv, rawLink.contentIv)
	}

	override suspend fun delete(entry: FullLocEntry) {
		raw.delete(null, entry.id)
	}

	override suspend fun readFile(file: FullLocEntry): LocFileInputStream {
		val rawFile = RawLocEntry(EMPTY_BYTE_ARRAY, null, file.contentIv, null, file.id, file.parentId, file.bytes, null, file.lastModified)
		return raw.readFile(rawFile).closeOnFailR {
			CryptoWrapLocFileInputStream(it, key, file.contentIv)
		}
	}

	override suspend fun writeFile(parent: FullLocEntry, name: String, bytes: Long, lastModified: Instant): FullLocFileOutputStream {
		val rawParent = RawLocEntry(EMPTY_BYTE_ARRAY, null, null, null, parent.id, parent.parentId, null, null, null)
		val cname = encrypt(key, name)
		return raw.writeFile(rawParent, cname.ciphertext, cname.iv, null, bytes, lastModified).closeOnFailR {
			CryptoWrapLocFileOutputStream(it, key)
		}
	}
}