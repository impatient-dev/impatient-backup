package imp.backup.cui.adapter

import androidx.compose.runtime.Immutable
import imp.backup.job.FailedPath
import imp.backup.job.Sync1Job
import imp.backup.job.Sync1Job.State.*
import imp.compose.cms
import imp.compose.cmsn
import java.lang.System.currentTimeMillis
import java.nio.file.Path

private val FINAL_STATES = setOf(SUCCESS, FAILED, ABORTED)

class Sync1JobAdapter (
	job: Sync1Job,
) : JobAdapter<Sync1Job>(job) {
	val state = cms(job.state)
	val ms = cms(0L)
	val inspecting = cmsn<Path>()
	val copying = cmsn<CopyingInfo>()
	val pathsCopied = cms(0L)
	val bytesCopied = cms(0L)
	val pathsDeleted = cms(0L)
	val bytesDeleted = cms(0L)
	val errorCount = cms(0L)
	val errors = cmsn<List<FailedPath>>()

	private var prevCopyingPath: Path? = null

	override fun updateRunning(): Boolean {
		val s = job.state
		state.value = s

		val start = job.start
		if(start == null)
			ms.value = 0
		else
			ms.value = (job.end?.toEpochMilli() ?: currentTimeMillis()) - start.toEpochMilli()

		inspecting.value = job.inspectingPathRelative
		pathsCopied.value = job.pathsCopied
		bytesCopied.value = job.bytesCopied
		pathsDeleted.value = job.pathsDeleted
		bytesDeleted.value = job.bytesDeleted
		errorCount.value = job.errorCount

		val cp = job.copyingFileRelative
		val cn = job.copyingNumerator
		val cd = job.copyingDenominator
		if(cp != null && cp == prevCopyingPath)
			copying.value = CopyingInfo(cp, cn, cd)
		else
			copying.value = null
		prevCopyingPath = cp

		if(FINAL_STATES.contains(s)) {
			errors.value = job.errors
			copying.value = null
			return true
		}

		return false
	}


	@Immutable data class CopyingInfo (
		val path: Path,
		val bytesCopied: Long,
		val sizeBytes: Long,
	)
}