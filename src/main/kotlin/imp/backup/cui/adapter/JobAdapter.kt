package imp.backup.cui.adapter

import imp.backup.job.AppJob
import imp.compose.cms

/**An adapter for a particular type of AppJob.
 * Maintains composable mutable state describing the job, and updates this state when requested.*/
abstract class JobAdapter <Job: AppJob> (
	val job: Job,
) {
	/**True if the job has finished and no more updates are necessary. This value should be updated every update().*/
	var isDone: Boolean = false
		private set
	val done = cms(isDone)

	/**Updates the variables describing the job. Can only be called on the main thread.
	 * Returns true if the job is still running, or false if it's been cancelled/aborted.
	 * False means no more updates are necessary.*/
	protected abstract fun updateRunning(): Boolean

	fun update() {
		val d = updateRunning()
		if(d != isDone) {
			isDone = d
			done.value = d
		}
	}
}