package imp.backup.cui.dialog

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import imp.backup.compareBackupLocations
import imp.backup.compareUserLocations
import imp.backup.cui.AppComposition
import imp.backup.cui.AppTheme
import imp.backup.cui.job.startJobWithDialog
import imp.backup.data.BackupDecl
import imp.backup.data.BackupLocation
import imp.backup.data.UserLocation
import imp.backup.job.Sync1Job
import imp.backup.repo.BackupI
import imp.backup.repo.LocationRepo
import imp.compose.*
import imp.util.desktop.CDialog
import imp.util.launchDefault
import imp.util.launchIo
import imp.util.withScope
import kotlinx.coroutines.CoroutineScope

/**Asks the user to confirm he wants to update a backup, then launches the job and a UI for it.
 * If either location name is ambiguous, this dialog will prompt the user to choose the correct location.*/
class UpdateBackupDialog (
	private val backup: BackupI,
	private val close: () -> Unit,
	from: UserLocation? = null,
	to: BackupLocation? = null,
) : AppComposition {
	private val possibleFroms = cmsn<List<UserLocation>>()
	private val possibleTos = cmsn<List<BackupLocation>>()
	private val delegate = cmsn<AppComposition>()

	init {
		if(from != null) {
			check(from.name == backup.userLocationName)
			possibleFroms.value = listOf(from)
		}
		if(to != null) {
			check(to.name == backup.backupLocationName)
			possibleTos.value = listOf(to)
		}
	}


	private fun load(scope: CoroutineScope) {
		var fs = possibleFroms.value
		var ts = possibleTos.value
		if(fs != null && ts != null)
			return
		scope.launchIo {
			val locations = LocationRepo.getBreakdown()
			if(fs == null)
				fs = locations.user
					.filter { it.name == backup.userLocationName }
					.sortedWith(::compareUserLocations)
			if(ts == null)
				ts = locations.backup
					.filter { it.name == backup.backupLocationName }
					.sortedWith(::compareBackupLocations)
			withScope(scope) {
				possibleFroms.value = fs
				possibleTos.value = ts
			}
		}
	}

	@Composable override fun Compose(theme: AppTheme) {
		val d = delegate.value
		if(d != null)
			d.Compose(theme)
		else {
			CDialog("Update Backup", close) {
				val scope = rememberCoroutineScope()
				val fs = possibleFroms.value
				val ts = possibleTos.value
				if(fs == null || ts == null) {
					LaunchedEffect(null) { load(scope) }
					CSpinnerFull()
				} else {
					val fromIdx = rcms(if(fs.size == 1) 0 else -1)
					val toIdx = rcms(if(ts.size == 1) 0 else -1)
					CDialogContents(fs, fromIdx, ts, toIdx, theme)
				}
			}
		}
	}


	@Composable private fun CDialogContents(
		froms: List<UserLocation>,
		fromIdx: MutableState<Int>,
		tos: List<BackupLocation>,
		toIdx: MutableState<Int>,
		theme: AppTheme,
	) = CColMaxScrollV(horizontal = Alignment.Start) {
		val scope = rememberCoroutineScope()
		val fi = fromIdx.value
		val ti = toIdx.value

		theme.title.CW("Update Backup")
		theme.subtitle.CW(backup.name)
		CHC { theme.strong.C(backup.type.userFriendly) }

		theme.sectionTitle.C("Backup From")
		froms.forEachIndexed { i, location ->  theme.mono.CRadioWC(location.searchPath.toString(), i == fi) { fromIdx.value = i } }

		theme.sectionTitle.C("Backup To")
		tos.forEachIndexed { i, location ->  theme.mono.CRadioWC(location.searchPath.toString(), i == ti) { toIdx.value = i } }

		CRowWC {
			theme.link.C("Cancel", onClick = close)
			theme.btn.C("Update Backup", enabled = fi >= 0 && ti >= 0) { startBackup(froms[fi], tos[ti].backup(backup.type, backup.name), scope) }
		}
	}

	private fun startBackup(from: UserLocation, decl: BackupDecl, scope: CoroutineScope) {
		scope.launchDefault {
			val backup = decl.initExisting()
			val job = Sync1Job(from, backup)
			withScope(scope) {
				delegate.value = startJobWithDialog(job, close)
			}
		}
	}
}