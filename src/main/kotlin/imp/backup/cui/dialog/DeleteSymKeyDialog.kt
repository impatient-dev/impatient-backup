package imp.backup.cui.dialog

import androidx.compose.runtime.Composable
import imp.backup.crypto.SymKey
import imp.backup.crypto.SymKeyRepo
import imp.backup.cui.AppComposition
import imp.backup.cui.AppTheme
import imp.compose.CColMaxScrollV
import imp.compose.CRowWC
import imp.util.defaultScope
import imp.util.desktop.CDialog
import kotlinx.coroutines.launch

/**After user confirmation, deletes a symmetric key.*/
class DeleteSymKeyDialog(
	val key: SymKey,
	private val close: () -> Unit,
) : AppComposition {
	@Composable override fun Compose(theme: AppTheme) = CDialog("Delete Key", close) {
		CColMaxScrollV {
			theme.sectionTitle.C("Delete Encryption Key?")
			theme.strong.C(key.name!!)
			theme.bodyWarn.C("Any backups encrypted with this key will be unreadable.")

			CRowWC {
				theme.link.C("Cancel", onClick = close)
				theme.btnDanger.C("Delete") {
					defaultScope().launch {
						SymKeyRepo.delete(key)
					}
					close()
				}
			}
		}
	}
}