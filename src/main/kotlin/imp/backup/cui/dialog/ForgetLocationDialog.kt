package imp.backup.cui.dialog

import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.window.Dialog
import imp.backup.cui.AppComposition
import imp.backup.cui.AppTheme
import imp.backup.data.BackupLocation
import imp.backup.data.Location
import imp.backup.data.PatchLocation
import imp.backup.data.UserLocation
import imp.backup.repo.LocationPathsRepo
import imp.compose.CColMaxScrollV
import imp.compose.CRowWC
import imp.compose.CSelV
import imp.util.defaultScope
import kotlinx.coroutines.launch

/**After user confirmation, forgets a location.*/
class ForgetLocationDialog(
	val location: Location,
	private val close: () -> Unit,
) : AppComposition {
	@Composable override fun Compose(theme: AppTheme) = Dialog(onCloseRequest = close) {
		CColMaxScrollV {
			CSelV {
				theme.sectionTitle.C("Forget Location?")
				val name = remember { getName(location) }
				if(name != null)
					theme.strong.C(name)
				theme.mono.C(location.searchPath.toString())
				theme.body.C("No files will be deleted, but the location will be removed from all lists in this app.")
			}

			CRowWC {
				theme.link.C("Cancel", onClick = close)
				theme.btn.C("Forget") {
					defaultScope().launch {
						LocationPathsRepo.remove(location.searchPath)
					}
					close()
				}
			}
		}
	}
}


private fun getName(location: Location): String? = when(location) {
	is UserLocation -> location.name
	is BackupLocation -> location.name
	is PatchLocation -> location.name
	else -> null
}