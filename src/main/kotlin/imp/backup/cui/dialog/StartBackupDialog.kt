package imp.backup.cui.dialog

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.window.Dialog
import imp.backup.compareBackupLocations
import imp.backup.compareUserLocations
import imp.backup.crypto.SymKey
import imp.backup.cui.AppComposition
import imp.backup.cui.AppTheme
import imp.backup.cui.job.startJobWithDialog
import imp.backup.data.BackupLocation
import imp.backup.data.BackupType
import imp.backup.data.UserLocation
import imp.backup.job.Sync1Job
import imp.backup.repo.LocationRepo
import imp.compose.*
import imp.util.launchIo
import imp.util.withDefault
import imp.util.withScope
import kotlinx.coroutines.CoroutineScope


/**A dialog showing the user a list of user and backup locations and letting him choose a pair to start a backup between.*/
class StartBackupDialog (
	name: String = "",
	from: UserLocation? = null,
	to: BackupLocation? = null,
	type: BackupType = BackupType.RAW,
	private val close: () -> Unit,
) : AppComposition {
	private val delegate = cmsn<AppComposition>()
	private val froms = cmsn<List<UserLocation>>()
	private val tos = cmsn<List<BackupLocation>>()
	private val name = cms(name)
	private val from = cmsn(from)
	private val to = cmsn(to)
	private val type = cms(type)
	private val key = cmsn<SymKey>()

	private suspend fun load(scope: CoroutineScope) = withDefault {
		val breakdown = LocationRepo.getBreakdown()
		val user = breakdown.user.sortedWith(::compareUserLocations)
		val backup = breakdown.backup.sortedWith(::compareBackupLocations)
		withScope(scope) {
			froms.value = user
			tos.value = backup
		}
	}

	@Composable override fun Compose(theme: AppTheme) {
		val d = delegate.value
		if (d != null) {
			d.Compose(theme)
		} else {
			Dialog(title = "Start a Backup", onCloseRequest = close) {
				val scope = rememberCoroutineScope()
				val froms = froms.value
				val tos = tos.value
				if (froms == null || tos == null) {
					LaunchedEffect(null) { load(scope) }
					CSpinnerFull()
				} else {
					CDialogContent(froms, tos, scope, theme)
				}
			}
		}
	}

	@Composable private fun CDialogContent(
		froms: List<UserLocation>,
		tos: List<BackupLocation>,
		scope: CoroutineScope,
		theme: AppTheme,
	) = CColWC {
		val from = from.value
		val to = to.value
		val name = name.value
		val type = type.value
		val overwrite = remember { CLookupAsync<PlannedBackup, Boolean>(scope, initial = PlannedBackup(to, name, type)) { it.exists() } }

		TextField(name, isError = name.isEmpty(), onValueChange = {
			this@StartBackupDialog.name.value = it
			overwrite.submit(PlannedBackup(to, name, type))
		})

		Row {
			BackupType.values().forEach { t ->
				theme.strong.CRadio(t.userFriendly, t == type) {
					this@StartBackupDialog.type.value = t
					overwrite.submit(PlannedBackup(to, name, t))
				}
			}
		}

		val keyToDisplay = if(type == BackupType.RAW) null else key.value
		CKey(keyToDisplay, canEdit = type == BackupType.DB && overwrite.output == false, scope, theme)

		Row(Modifier.fillMaxWidth().weight(1f)) {
			Column(Modifier.weight(1f).verticalScroll(rememberScrollState())) {
				CUserLocations(from, froms, theme) { this@StartBackupDialog.from.value = it }
			}
			Column(Modifier.weight(1f).verticalScroll(rememberScrollState())) {
				CBackupLocations(to, tos, theme) {
					this@StartBackupDialog.to.value = it
					overwrite.submit(PlannedBackup(it, name, type))
				}
			}
		}

		CIsOverwrite(name, to, if (overwrite.isWorking) null else overwrite.output, theme)

		Row {
			theme.btn.C("Cancel", onClick = close)
			theme.btn.C("Start", enabled = from != null && to != null && !name.isEmpty() && !overwrite.isWorking) {
				startBackup(from!!, to!!, type, name, isNew = overwrite.output != true, key.value, scope)
			}
		}
	}

	private fun startBackup(
		from: UserLocation,
		to: BackupLocation,
		type: BackupType,
		name: String,
		isNew: Boolean,
		key: SymKey?,
		scope: CoroutineScope
	) {
		scope.launchIo {
			val decl = to.backup(type, name)
			val backup = if(isNew) decl.initNew(from.name, key) else decl.initExisting()
			val job = Sync1Job(from, backup)
			withScope(scope) {
				delegate.value = startJobWithDialog(job, close)
			}
		}
	}


	@Composable private fun CKey(key: SymKey?, canEdit: Boolean, scope: CoroutineScope, theme: AppTheme) {
		CRowC {
			if(key == null)
				theme.strongWarn.C("Not Encrypted")
			else if(key.key == null)
				theme.strongError.C("Encrypted with unknown key")
			else
				theme.strongGood.C("Encrypted with key: ${key.name!!}")
			if(canEdit) {
				val dialog = rcmsn<AppComposition>()
				dialog.value?.Compose(theme)
				theme.btn.C("...") { dialog.value = ChooseEncryptionKeyDialog(key, close = { dialog.value = null }, choose = { this@StartBackupDialog.key.value = it })  }
			}
		}
	}

	@Composable private fun CUserLocations(selected: UserLocation?, froms: List<UserLocation>, theme: AppTheme, select: (UserLocation) -> Unit) {
		for(location in froms) {
			CCheckWCol(selected == location, { select(location) }) {
				theme.titleItem.C(location.name)
				theme.body.C(location.metaDir!!.toString())
			}
		}
	}

	@Composable private fun CBackupLocations(selected: BackupLocation?, tos: List<BackupLocation>, theme: AppTheme, select: (BackupLocation) -> Unit) {
		for(location in tos) {
			CCheckWCol(
				checked = selected == location,
				setChecked = { select(location) }
			) {
				theme.titleItem.C(location.name)
				theme.body.C(location.path.toString())
			}
		}
	}

	@Composable private fun CIsOverwrite(
		name: String,
		to: BackupLocation?,
		/**Null if we're not sure yet.*/
		isOverwrite: Boolean?,
		theme: AppTheme,
	) {
		when {
			name.isEmpty() ->theme.strong.C("A name is required.")
			to == null -> theme.strong.C("A backup location is required.")
			isOverwrite == null -> CSpinner()
			isOverwrite -> theme.strong.C("Overwrite an existing backup.")
			else -> theme.strong.C("Create a new backup.")
		}
	}
}



private data class PlannedBackup (
	val location: BackupLocation?,
	val backupName: String,
	val type: BackupType,
) {
	suspend fun exists(): Boolean = location != null && location.backup(type, backupName).exists()
}