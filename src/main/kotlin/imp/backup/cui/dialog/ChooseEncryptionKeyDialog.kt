package imp.backup.cui.dialog

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.rememberCoroutineScope
import imp.backup.crypto.SymKey
import imp.backup.crypto.SymKeyRepo
import imp.backup.cui.AppComposition
import imp.backup.cui.AppTheme
import imp.compose.*
import imp.util.desktop.CDialog
import imp.util.launchDefault
import imp.util.withScope
import kotlinx.coroutines.CoroutineScope

class ChooseEncryptionKeyDialog (
	initial: SymKey?,
	private val close: () -> Unit,
	private val choose: (SymKey?) -> Unit,
) : AppComposition {
	private val keys = cmsn<List<SymKey>>()
	private val key = cmsn(initial)

	private fun load(scope: CoroutineScope) {
		scope.launchDefault {
			val ks = SymKeyRepo.getAll().sortedBy { it.name }
			withScope(scope) { keys.value = ks }
		}
	}

	@Composable override fun Compose(theme: AppTheme) = CDialog("Choose Encryption Key", close) {
		CColMaxScrollV {
			theme.title.CW("Choose Encryption Key")
			val scope = rememberCoroutineScope()
			val ks = keys.value
			if(ks == null) {
				LaunchedEffect(this@ChooseEncryptionKeyDialog) { load(scope) }
				CSpinnerFull()
			} else {
				CContent(ks, key.value, scope, theme)
			}
		}
	}

	@Composable private fun CContent(keys: List<SymKey>, key: SymKey?, scope: CoroutineScope, theme: AppTheme) {
		for(k in keys)
			theme.body.CRadioW(k.name!!, k == key) { this@ChooseEncryptionKeyDialog.key.value = k }
		CRowC {
			theme.link.C("Cancel", onClick = close)
			theme.btn.C("No Key") { choose(null); close() }
			theme.btn.C("Choose", enabled = key != null) { choose(key); close() }
		}
	}
}