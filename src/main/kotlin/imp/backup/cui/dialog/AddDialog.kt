package imp.backup.cui.dialog

import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.Immutable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.window.Dialog
import imp.backup.cui.AppComposition
import imp.backup.cui.AppTheme
import imp.backup.net.WS_PATH
import imp.backup.repo.LocationPathsRepo
import imp.compose.*
import imp.okhttp.impWsClientOk
import imp.okhttp.kill
import imp.okhttp.withAllTimeoutsS
import imp.util.CriticalLauncher
import imp.util.defaultScope
import imp.util.logger
import imp.web.ImpWs
import imp.web.ImpWsReceiver
import imp.web.httpWithDefaultPortAndPath
import imp.web.parseHttpUserUrl
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import okhttp3.OkHttpClient
import java.net.URL
import java.nio.ByteBuffer
import java.nio.file.Files
import java.nio.file.Path

private val log = AddDialog::class.logger

/**Lets the user add a location or outgoing connection.*/
class AddDialog (
	/**Whether we're adding a location instead of a connection.*/
	initiallyLocation: Boolean,
	private val close: () -> Unit,
) : AppComposition {
	private val showLocation = cms(initiallyLocation)
	private val locPanel = AddLocationPanel(close)
	private val connPanel = AddConnectionPanel(close)


	@Composable override fun Compose(theme: AppTheme) {
		val showingLoc = showLocation.value
		val scope = rememberCoroutineScope()
		Dialog(close, title = if(showingLoc) "Add Location" else "Add Connection") {
			CColC {
				Row {
					theme.strong.CRadio("Add Location", showingLoc) { showLocation.value = true}
					theme.strong.CRadio("Add Connection", !showingLoc) { showLocation.value = false}
				}
				Box(Modifier.fillMaxWidth().weight(1f)) {
					if(showingLoc) locPanel.Compose(scope, theme)
					else connPanel.Compose(scope, theme)
				}
			}
		}
	}
}


private class AddLocationPanel (private val close: () -> Unit) {
	private val path = cms("")

	@Composable fun Compose(scope: CoroutineScope, theme: AppTheme) = Column(Modifier.fillMaxSize()) {
		val validator = remember { CLookupAsync<String, ValidationResult>(scope, initial = path.value) { validate(it) } }
		val v = validator.output

		CTextField(path.value, "Path", "/home/me/Documents", isError = v != null && v.parsed == null) { path.value = it; validator.submit(it) }
		if(v?.duplicate == true)
			CHC { theme.strongError.C("Duplicate (already added).") }
		if(v?.parsed != null && !v.isAbsolute)
			CHC { theme.strongError.C("The path must be absolute, not relative.") }
		if(v?.parsed != null && !v.exists)
			CHC { theme.strongWarn.C("This path doesn't exist currently.") }
		CRowWC {
			theme.link.C("Cancel", onClick = close)
			theme.btn.C("Add", enabled = !validator.isWorking && v != null && v.okToAdd) { doAdd(validator.output!!.parsed!!) }
		}
	}

	private fun doAdd(path: Path) {
		defaultScope().launch { LocationPathsRepo.add(path) }
		close()
	}

	@Immutable private data class ValidationResult(
		/**Null if we weren't able to parse the path.*/
		val parsed: Path?,
		val exists: Boolean,
		val duplicate: Boolean,
	) {
		val isAbsolute = parsed?.isAbsolute ?: false
		val okToAdd get() = isAbsolute && !duplicate
	}

	private suspend fun validate(str: String): ValidationResult {
		val p: Path? = if(str.isBlank()) null else try { Path.of(str) } catch(e: Exception) { null }
		val exists = if(p == null || !p.isAbsolute) false else Files.exists(p)
		val duplicate = p != null && LocationPathsRepo.list().contains(p)
		return ValidationResult(p, exists, duplicate)
	}
}


private class AddConnectionPanel (private val close: () -> Unit) {
	private val addr = cms("")
	private val secure = cms(true)
	private val result = cmsn<ConnTestResult>()
	private val critical = CriticalLauncher()

	@Composable fun Compose(scope: CoroutineScope, theme: AppTheme) = Column(Modifier.fillMaxSize()) {
		val validator = remember { CLookupAsync<AddrInput, URL?>(scope) { parse(it) } }
		val connTest = remember { CLookupAsync<URL, ConnTestResult>(scope, backDispatcher = Dispatchers.IO, deduplicate = false) { testConn(it) } }
		val url = validator.output

		TextField(
			addr.value,
			onValueChange = { addr.value = it; validator.submit(AddrInput(it, secure.value)) },
			isError = url == null,
			placeholder = { Text("example.com:80/ws") })
		theme.body.CCheckW("Secure", secure.value) { secure.value = it; validator.submit(AddrInput(addr.value, it)) }

		CHC {
			if (connTest.isWorking)
				CSpinner()
			else {
				val result = connTest.output
				if (result != null && result.url == url) {
					if (result.error == null)
						theme.strongGood.C("This connection works")
					else
						theme.strongError.C(result.error)
				}
			}
		}

		CRowWC {
			theme.link.C("Cancel", onClick = close)
			theme.btn.C("Test Connection", enabled = !validator.isWorking && url != null) { connTest.submit(url!!) }
			theme.btn.C("Add", enabled = !validator.isWorking && url != null) { TODO(); close() }
		}
	}

	private fun parse(inp: AddrInput): URL? {
		try {
			return parseHttpUserUrl(inp.addr, inp.secure)
		} catch(e: Exception) {
			log.trace("Invalid URL: ${inp.addr}", e)
			return null
		}
	}

	private suspend fun testConn(url: URL): ConnTestResult {
		val errorCh = Channel<String?>(2)
		val client = OkHttpClient.Builder().withAllTimeoutsS(10).build()
		defaultScope().launch {
			delay(15_000)
			errorCh.trySend("Websocket connection timed out.")
			errorCh.close()
			client.kill()
		}
		try {
			impWsClientOk(url.httpWithDefaultPortAndPath(WS_PATH).toString(), MyWsReceiver(errorCh), client)
			val error = errorCh.receive()
			return ConnTestResult(url, error)
		} catch(e: Exception) {
			log.error("Failed to connect to $url", e)
			return ConnTestResult(url, e.message)
		} finally {
			errorCh.close()
		}
	}

	@Immutable private data class AddrInput (
		val addr: String,
		val secure: Boolean,
	)

	@Immutable private data class ConnTestResult(
		val url: URL,
		/**Null means success.*/
		val error: String?,
	)


	/**Sends an error message to the channel if an error happens; sends null if the connection succeeds.*/
	private class MyWsReceiver(private val errorChannel: Channel<String?>) : ImpWsReceiver {
		override fun onOpen(ws: ImpWs) {
			ws.close()
			runBlocking { errorChannel.send(null) }
		}
		override fun onError(e: Throwable, ws: ImpWs) {
			log.error("Websocket test connection failed for ${ws.remoteAddr.hostString}.", e)
			errorChannel.trySend(e.message)
			ws.close()
		}
		override fun onBinary(message: ByteBuffer, ws: ImpWs) {}
		override fun onText(message: String, ws: ImpWs) {}
		override fun onClose(ws: ImpWs) {}
	}
}