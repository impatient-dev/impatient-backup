package imp.backup.cui

import androidx.compose.runtime.*
import imp.backup.cui.job.startJobWithDialog
import imp.backup.data.ArbitraryPath
import imp.backup.data.BackupDecl
import imp.backup.data.BackupLocation
import imp.backup.data.UserLocation
import imp.backup.job.Sync1Job
import imp.backup.repo.BackupI
import imp.backup.repo.LocationRepo
import imp.compose.*
import imp.util.desktop.CDialog
import imp.util.exToNull
import imp.util.launchDefault
import imp.util.withScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.nio.file.Files
import java.nio.file.Path
import kotlin.io.path.Path

/**After user confirmation, restores a backup to some user-specified place (.*/
class RestoreBackupDialog (
	private val backupInfo: BackupI,
	/**Null if unknown.*/
	backupLocation: BackupLocation?,
	to: UserLocation?,
	private val close: () -> Unit,
) : AppComposition {
	private val delegate = cmsn<AppComposition>()
	private val loading = cms(true)
	private val backup = cmsn<BackupDecl>()
	private val backupLocation = cmsn(backupLocation)
	private val initialLocationTo = to

	@Composable override fun Compose(theme: AppTheme) {
		val d = delegate.value
		if(d != null)
			d.Compose(theme)
		else
			CDialog("Restore Backup", close) { CDialogContent(theme) }
	}


	@Composable private fun CDialogContent(theme: AppTheme) = CColMaxScrollV {
		val scope = rememberCoroutineScope()
		val to = remember { CLookupAsync(scope, initial = initialLocationTo?.searchPath?.toString()) { lookupPath(it) } }
		LaunchedEffect(null) { initialLoad(scope) }
		val b = backup.value
		val bLoc = backupLocation.value
		val dest = to.output

		theme.sectionTitle.C("Restore Backup?")
		CRowC {
			theme.strong.C(backupInfo.name)
			CHPad(txtPad)
			theme.C(backupInfo.type)
		}
		theme.labelBody.C("Location", backupInfo.backupLocationName)
		if(b != null)
			theme.mono.C(b.userPath)

		if (!loading.value && bLoc == null)
			theme.strongError.C("Location not found, or there are duplicates.")

		CTextField(to.input ?: "", "Restore To", "/home/me/Documents", isError = dest == null) { to.submit(it) }

		if(dest != null && dest.occupied)
			theme.strongWarn.C("There are already files here, which may be overwritten by the restore.")

		CRowC {
			theme.link.C("Cancel", onClick = close)
			(if(dest?.occupied == true) theme.btnWarn else theme.btn)
				.C("Restore", enabled = b != null && bLoc != null && dest != null && !to.isWorking) { restore(b!!, dest!!.parsed, scope) }
		}
	}

	private fun lookupPath(str: String) = exToNull<RestoreDestination> {
		val path = Path(str)
		check(!Files.isRegularFile(path))
		val occupied = if(Files.isDirectory(path)) Files.list(path).anyMatch { true } else false
		return RestoreDestination(path, occupied)
	}

	private fun initialLoad(scope: CoroutineScope) {
		var b = backup.value
		var bLoc = backupLocation.value
		scope.launch {
			if(bLoc == null)
				bLoc = LocationRepo.getBackupIf1(backupInfo.backupLocationName)
			if(b == null)
				b = bLoc?.backup(backupInfo.type, backupInfo.name)
			withScope(scope) {
				backup.value = b
				backupLocation.value = bLoc
				loading.value = false
			}
		}
	}

	private fun restore(decl: BackupDecl, to: Path, scope: CoroutineScope) {
		scope.launchDefault {
			val backup = decl.initExisting()
			val job = Sync1Job(backup, ArbitraryPath(to))
			withScope(scope) {
				delegate.value = startJobWithDialog(job, close)
			}
		}
	}
}



@Immutable private data class RestoreDestination(
	val parsed: Path,
	/**True if there are already things at this path (which would be overwritten by the restore).*/
	val occupied: Boolean,
)