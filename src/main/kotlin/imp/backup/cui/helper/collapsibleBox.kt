package imp.backup.cui.helper

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import imp.backup.cui.AppIcon
import imp.backup.cui.AppTheme
import imp.compose.CColWC
import imp.compose.CRowC
import imp.compose.CWeightBox
import imp.compose.rcms
import imp.util.desktop.CIcon

/**A box that has a header and some content, where clicking on the header collapses/expands the content.
 * The header and content will fill the available width.*/
@Composable fun CCollapsibleBox(
	theme: AppTheme,
	expanded: MutableState<Boolean> = rcms(true),
	header: @Composable () -> Unit,
	content: @Composable () -> Unit,
) = CColWC {
	val e = expanded.value
	CRowC (Modifier.fillMaxWidth().background(theme.boxTitle.back).clickable { expanded.value = !e }) {
		theme.boxTitle.fore.CIcon(if(e) AppIcon.down else AppIcon.right)
		CWeightBox { header() }
	}
	if(e) {
		content()
	}
}

