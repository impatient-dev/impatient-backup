package imp.backup.cui.helper

import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import imp.backup.data.Location
import imp.backup.repo.LocationRepo
import imp.compose.cRemember
import kotlinx.coroutines.CoroutineScope


/**Registers a listener during composition (and deregisters it when this function call leaves the composition)
 * that will invoke your code when the specified location is removed.*/
@Composable fun rememberOnRemoveLocation(
	location: Location,
	scope: CoroutineScope = rememberCoroutineScope(),
	onRemove: () -> Unit
) {
	LocationRepo.listeners.cRemember(scope) { MyLocationListener(location, onRemove) }
}


private class MyLocationListener (
	val location: Location,
	val onRemove: () -> Unit,
) : LocationRepo.Listener {
	override suspend fun postRemoveLocation(location: Location) {
		if(location == this.location)
			onRemove()
	}
}