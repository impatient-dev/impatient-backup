package imp.backup.cui.job

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.window.Dialog
import imp.backup.cui.AppComposition
import imp.backup.cui.AppTheme
import imp.backup.cui.hpad
import imp.backup.job.FailedPath
import imp.compose.CColMaxScrollV
import imp.compose.CHC
import imp.compose.CSelVWL

/**A dialog that shows errors that occurred during a job. Don't show this if there are no errors.*/
class JobErrorsDialog (
	val sample: List<FailedPath>,
	val totalErrors: Long,
	val close: () -> Unit,
) : AppComposition {
	@Composable override fun Compose(theme: AppTheme) = Dialog(close, title = "Errors") {
		CColMaxScrollV {
			theme.sectionTitleError.C("$totalErrors Errors")
			theme.note.C("If there are multiple errors listed for a path, the lower ones caused the higher ones.")
			CSelVWL {
				sample.forEachIndexed { i, err ->
					Row {
						theme.body.C("#${i + 1}", Modifier.padding(end = hpad))
						theme.body.C(err.path.toString())
					}
					CStacktrace(err.th, theme)
				}
				if (totalErrors > sample.size)
					CHC { theme.bodyError.C("${totalErrors - sample.size} more errors not shown") }
			}
		}
	}
}


@Composable private fun CStacktrace(t: Throwable, theme: AppTheme) {
	var th = t
	var depth = 1
	while(true) {
		theme.bodyError.C("${th.javaClass.name}: ${th.message}")
		if(th.cause == null || th.cause == th || ++depth > 10)
			break
		th = th.cause!!
	}
}