package imp.backup.cui.job

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.window.Dialog
import imp.backup.cui.AppComposition
import imp.backup.cui.AppTheme
import imp.backup.cui.adapter.JobAdapter
import imp.backup.cui.adapter.Sync1JobAdapter
import imp.backup.job.AppJob
import imp.backup.job.Sync1Job
import imp.compose.CColMaxScrollV
import imp.compose.CRowWC
import imp.compose.CWeight
import imp.compose.rcmsn
import imp.util.defaultScope
import imp.util.ioScope
import imp.util.logger
import imp.util.unexpected
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

private val log = JobDialog::class.logger
private val jobScope = ioScope()
private val adapterScope = defaultScope()

/**Starts running a job, and returns a dialog that shows the progress of the job.
 * You should display the dialog until close is called.*/
fun startJobWithDialog(job: AppJob, onClose: () -> Unit): JobDialog<*> {
	if(job is Sync1Job)
		return Sync1JobDialog(Sync1JobAdapter(job), onClose)
	return unexpected(job)
}


/**Returns the coroutine Job that is running our job; the coroutine job that is running the adapter is separate and cannot be cancelled.*/
private fun runJobAndAdapter(adapter: JobAdapter<*>): Job {
	adapterScope.launch {
		do {
			delay(1000)
			adapter.update()
		} while (!adapter.isDone)
		log.trace("Done with adapter {}.", adapter)
	}
	return jobScope.launch { adapter.job.run(this) }
}


/**Maintains a dialog that runs a job and shows its progress.*/
abstract class JobDialog <Adapter: JobAdapter<*>> (
	val adapter: Adapter,
	private val onClose: () -> Unit,
) : AppComposition {
	@Composable override fun Compose(theme: AppTheme) {
		val scope = rememberCoroutineScope()
		val job = rcmsn<Job>()
		LaunchedEffect(adapter) {
			job.value = runJobAndAdapter(adapter)
		}
		val j = job.value

		Dialog(title = "Backup", onCloseRequest = ::maybeClose) {
			CColMaxScrollV {
				CContent(theme)
				CWeight()
				CRowWC {
					val done = adapter.done.value
					theme.btn.C("Close", enabled = done, onClick = onClose)
					theme.btn.C("Stop", enabled = !done && j != null) { j?.cancel() }
				}
			}
		}
	}

	@Composable protected abstract fun CContent(theme: AppTheme)

	private fun maybeClose() {
		if(adapter.isDone)
			onClose()
	}
}