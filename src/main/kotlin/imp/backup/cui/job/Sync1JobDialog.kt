package imp.backup.cui.job

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import imp.backup.cui.AppComposition
import imp.backup.cui.AppIcon
import imp.backup.cui.AppTheme
import imp.backup.cui.adapter.Sync1JobAdapter
import imp.backup.cui.padBig
import imp.backup.data.BackupImpl
import imp.backup.job.Sync1Job
import imp.compose.*
import imp.util.fmt.TimeLetterFmt
import imp.util.fmtBytes

class Sync1JobDialog (
	adapter: Sync1JobAdapter,
	onClose: () -> Unit,
) : JobDialog<Sync1JobAdapter>(adapter, onClose) {
	@Composable override fun CContent(theme: AppTheme) = Column {
		val state = adapter.state.value
		val done = adapter.done.value
		val pathsCopied = adapter.pathsCopied.value
		val bytesCopied = adapter.bytesCopied.value
		val pathsDeleted = adapter.pathsDeleted.value
		val bytesDeleted = adapter.bytesDeleted.value
		val inspecting = adapter.inspecting.value
		val copying = adapter.copying.value
		val errorCount = adapter.errorCount.value
		val errors = adapter.errors.value
		val ms = adapter.ms.value
		val isBackup = adapter.job.dst is BackupImpl

		val dialog = rcmsn<AppComposition>()
		dialog.value?.Compose(theme)

		CTitle(state, if(isBackup) "Backup" else "Restore", theme)
		CFromTo(adapter.job, theme)

		theme.divider.CH()

		theme.body.C(TimeLetterFmt.durationToS(ms))
		theme.body.C("$pathsCopied copied - ${fmtBytes(bytesCopied)}")
		theme.body.C("$pathsDeleted deleted - ${fmtBytes(bytesDeleted)}")

		if(errorCount > 0) {
			val str = "$errorCount errors"
			if(errors == null)
				theme.bodyError.C(str)
			else
				theme.linkError.C(str) { dialog.value = JobErrorsDialog(errors, errorCount) { dialog.value = null } }
		}

		if(copying != null) {
			theme.divider.CH()
			theme.body.C(copying.path.toString())
			theme.body.C("${fmtBytes(copying.bytesCopied)} / ${fmtBytes(copying.sizeBytes)}")
			LinearProgressIndicator(copying.bytesCopied.toFloat() / copying.sizeBytes.toFloat())
		} else if(!done && inspecting != null) {
			theme.divider.CH()
			theme.body.C(inspecting.toString())
		}
	}


	@Composable private fun CTitle(state: Sync1Job.State, jobType: String, theme: AppTheme) = when(state) {
		Sync1Job.State.RUNNING -> theme.title.CW("$jobType in Progress")
		Sync1Job.State.SUCCESS -> theme.title.CW("$jobType Finished")
		Sync1Job.State.ABORTED -> theme.titleWarn.CW("$jobType Aborted")
		Sync1Job.State.FAILED -> theme.titleError.CW("$jobType Failed")
		Sync1Job.State.NOT_STARTED -> theme.title.CW("$jobType Preparing to Start")
	}


	@Composable private fun CFromTo(job: Sync1Job, theme: AppTheme) = CRowWC {
		CWeightBox {
			CSelVWL {
				theme.strong.C(job.src.locationName ?: "")
				theme.mono.C(job.src.userPath)
			}
		}
		Box(Modifier.padding(padBig)) {
			theme.icon.C(AppIcon.arrowRightBold)
		}
		CWeightBox {
			CSelVWR {
				theme.strong.C(job.dst.locationName ?: "")
				theme.mono.C(job.dst.userPath)
			}
		}
	}
}