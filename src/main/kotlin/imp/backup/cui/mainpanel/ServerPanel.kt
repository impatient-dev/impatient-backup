package imp.backup.cui.mainpanel

import androidx.compose.foundation.layout.Row
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.rememberCoroutineScope
import imp.backup.cui.AppComposition
import imp.backup.cui.AppTheme
import imp.backup.net.AppServer
import imp.backup.net.AppServerCfg
import imp.backup.net.WS_PORT_LOCAL
import imp.compose.*
import imp.util.*
import imp.web.X509CertificatePresentation
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch


private val log = ServerPanel::class.logger


/**Lets the user control the embedded websocket server.*/
object ServerPanel : AppComposition {
	private val loading = cms(true)
	private val running = cms(false)
	private val runningCert = cmsn<X509CertificatePresentation>()
	private val error = cmsn<String>()

	private val host = cms("localhost")
	private val sslKeystorePassword = cms("")

	@Composable override fun Compose(theme: AppTheme) {
		CColMaxScrollV {
			val scope = rememberCoroutineScope()
			LaunchedEffect(null) { initialLoad(scope) }
			val working = loading.value
			val running = running.value

			theme.title.CW("Server")
			CServerStatus(working, running, error.value, theme)
			CServerControls(scope, working, running, theme)
			CHL { theme.note.C("When the embedded server is running, other computers can connect to this one.") }
		}
	}


	@Composable private fun CServerStatus(
		loading: Boolean,
		running: Boolean,
		error: String?,
		theme: AppTheme,
	) {
		CHL {
			if(loading)
				CSpinner()
			else if(running)
				theme.sectionTitleActive.C("Running on port $WS_PORT_LOCAL")
			else
				theme.sectionTitleOff.C("Not Running")
		}
		if(error != null)
			CHL { theme.bodyError.C(error) }
	}

	@Composable private fun CServerControls(
		scope: CoroutineScope,
		working: Boolean,
		running: Boolean,
		theme: AppTheme,
	) = Row {
		theme.btn.C("Start", enabled = !working && !running) { startAsync(scope) }
		theme.btn.C("Stop", enabled = !working && running) { stopAsync(scope) }
	}


	private fun initialLoad(scope: CoroutineScope) = scope.launchDefault {
		val r = AppServer.isRunning()
		withScope(scope) {
			running.value = r
			loading.value = false
		}
	}

	private fun startAsync(scope: CoroutineScope) {
		val h = host.value
		val pwd = sslKeystorePassword.value.emptyToNull()
		loading.value = true
		scope.launchDefault {
			var err: String? = null
			try {
				val cfg = AppServerCfg(h)
				AppServer.startIfNotRunning(cfg)
			} catch(e: Exception) {
				log.error("Failed to start server.", e)
				err = stacktraceNoCode(e)
			}
			withScope(scope) {
				running.value = err == null
				error.value = err
				loading.value = false
			}
		}
	}

	private fun stopAsync(scope: CoroutineScope) = scope.launch {
		loading.value = true
		withDefault { AppServer.stopIfRunning() }
		running.value = false
		loading.value = false
	}
}