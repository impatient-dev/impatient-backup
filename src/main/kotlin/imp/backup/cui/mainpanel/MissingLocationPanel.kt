package imp.backup.cui.mainpanel

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import imp.backup.cui.AppComposition
import imp.backup.cui.AppTheme
import imp.backup.cui.MainWindow
import imp.backup.cui.dialog.ForgetLocationDialog
import imp.backup.cui.helper.rememberOnRemoveLocation
import imp.backup.data.MissingLocation
import imp.compose.*


private val typeDescription = "This location doesn't currently exist (e.g. USB drive isn't plugged in), or some other problem prevents us from working with it currently."

class MissingLocationPanel (
	val location: MissingLocation,
	private val parent: MainWindow,
) : AppComposition {
	private val dialog = cmsn<AppComposition>()

	@Composable override fun Compose(theme: AppTheme) = CColMaxScrollV {
		rememberOnRemoveLocation(location) { parent.closeDetailPane() }
		dialog.value?.Compose(theme)
		CSelV(horizontal = Alignment.Start) {
			theme.titleMonoWarn.CW(location.searchPath.toString(), horizontal = Arrangement.Start)
			theme.subtitleWarn.CW("Missing Location", horizontal = Arrangement.Start)
			theme.note.C(typeDescription)
			theme.strongError.C(location.reason)
		}
		CWeight()
		CRowWR {
			theme.btn.C("Remove") { dialog.value = ForgetLocationDialog(location) { dialog.value = null } }
		}
	}
}