package imp.backup.cui.mainpanel

import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import imp.backup.cui.AppComposition
import imp.backup.cui.AppIcon
import imp.backup.cui.AppTheme
import imp.backup.cui.MainWindow
import imp.backup.cui.dialog.ForgetLocationDialog
import imp.backup.cui.dialog.StartBackupDialog
import imp.backup.cui.helper.CCollapsibleBox
import imp.backup.cui.helper.rememberOnRemoveLocation
import imp.backup.data.BackupLocation
import imp.backup.repo.BackupI
import imp.backup.repo.BackupRepo
import imp.compose.*
import imp.util.CompareUtil
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers


private val typeDescription = "A place where backups are stored."


class BackupLocationPanel (
	val location: BackupLocation,
	private val parent: MainWindow,
) : AppComposition {
	private val dialog = cmsn<AppComposition>()

	private suspend fun loadBackups(scope: CoroutineScope): List<BackupI> {
		return BackupRepo.getAllTo(location)
			.sortedWith { a, b -> CompareUtil.nullsGreater(a.name, b.name) }
	}

	@Composable override fun Compose(theme: AppTheme) = CMainPanel(location.name, "Backup Location", typeDescription, theme) {
		val scope = rememberCoroutineScope()
		rememberOnRemoveLocation(location, scope) { parent.closeDetailPane() }
		dialog.value?.Compose(theme)
		CBackupList(scope, theme)
		CRowWR {
			theme.btn.C("New Backup...") { dialog.value = StartBackupDialog(to = location) { dialog.value = null } }
		}
		CWeight()
		CRowWR {
			theme.btn.C("Remove") { dialog.value = ForgetLocationDialog(location) { dialog.value = null } }
		}
	}


	@Composable private fun CBackupList(scope: CoroutineScope, theme: AppTheme) {
		val reload = remember(location) { CLookupAsync(scope, Dispatchers.IO, initial = scope, deduplicate = false) { loadBackups(it) } }
		val list = reload.output

		CCollapsibleBox(
			theme,
			header = {
				CRowWC {
					theme.boxTitle.CB("Backups (${list?.size ?: 0})")
					CWeight()
					if(reload.isWorking)
						CSpinner()
					theme.btn.CIcon(AppIcon.reload, enabled = !reload.isWorking) { reload.submit(scope) }
				}
			}, content = {
				CColWC(theme.boxBorder.modifier()) {
					list?.forEachIndexed { i, backup ->
						if(i > 0)
							theme.divider.CH()
						CBackup(backup, dialog, theme, toLocation = location)
					}
				}
			}
		)
	}
}