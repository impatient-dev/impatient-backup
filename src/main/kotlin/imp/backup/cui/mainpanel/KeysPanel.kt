package imp.backup.cui.mainpanel

import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.runtime.*
import imp.backup.crypto.SecretPaths
import imp.backup.crypto.SecretPathsRepo
import imp.backup.crypto.SymKey
import imp.backup.crypto.SymKeyRepo
import imp.backup.cui.AppComposition
import imp.backup.cui.AppIcon
import imp.backup.cui.AppTheme
import imp.backup.cui.dialog.DeleteSymKeyDialog
import imp.compose.*
import imp.util.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.lang.Integer.parseInt
import java.nio.file.Path
import java.security.Security
import kotlin.io.path.Path

private val log = KeysPanel::class.logger

/**Shows a list of encryption keys and lets the user generate new ones.*/
object KeysPanel : AppComposition {
	private val algorithms = Security.getAlgorithms("Cipher").sorted()


	@Composable override fun Compose(theme: AppTheme) = CColMaxScrollV {
		val scope = rememberCoroutineScope()
		CKeysDirSection(scope, theme)
		theme.divider.CH()
		CKeyGenSection(scope, theme)
		theme.divider.CH()
		CKeysList(scope, theme)
	}


	@Composable private fun CKeysDirSection(scope: CoroutineScope, theme: AppTheme) {
		val dirParse = remember { CLookupSync<String, Path> { Path(it) } }
		val hasChanged = rcms(false)
		LaunchedEffect(this) { loadKeysDir(scope, dirParse) }

		val d = dirParse.input
		if(d != null) {
			theme.sectionTitle.C("Encryption Keys Location")
			theme.body.C("The directory where this application stores encryption keys and other sensitive info. This is a directory you create, and it should probably be part of an encrypted volume.")
			CRowC {
				CTextField(d, "Directory", "", isError = dirParse.output == null) { dirParse.submit(it); hasChanged.value = true }
				theme.btn.C("Save", enabled = dirParse.output != null && hasChanged.value) {
					val path = dirParse.output!!
					ioScope().launch { SecretPathsRepo.set(SecretPaths(path)) }
					hasChanged.value = false
				}
			}
		}
	}


	@Composable private fun CKeyGenSection(scope: CoroutineScope, theme: AppTheme) {
		val names = rcmsn<Set<String>>()
		SymKeyRepo.listeners.cRemember(scope) { AllKeyNamesMaintainer(scope, names) }

		theme.sectionTitle.C("Generate New Encryption Key")

		val name = rcms("")
		CTextField(name.value, "Name", "", isError = name.value.isBlank()) { name.value = it }

		val alg = rcmsn<String>()
		CRowC {
			val open = rcms(false)
			if(alg.value == null)
				theme.strongWarn.C("No algorithm selected")
			else
				theme.strong.C(alg.value!!)
			theme.btn.C("...") { open.value = true}
			DropdownMenu(open.value, { open.value = false }) {
				for(a in algorithms) {
					DropdownMenuItem({ alg.value = a; open.value = false }) { theme.body.C(a) }
				}
			}
		}

		val bits = remember { CLookupSync("") { exToNull { parseInt(it) } } }
		val bitStr = bits.input ?: ""
		CTextField(bitStr, "Key Size", "256", isError = bitStr.isNotEmpty() && bits.output == null) { bits.submit(it) }

		val error = rcmsn<String>()
		error.value?.let { theme.bodyError.C(it) }

		if(names.value?.contains(name.value) == true)
			theme.strongWarn.C("Name already in use.")

		theme.btn.C("Generate", enabled = name.value.isNotBlank() && alg.value != null && (bitStr.isEmpty() || bits.output != null)) {
			val n = name.value
			val a = alg.value!!
			val b = bits.output
			ioScope().launch {
				val err = try {
					SymKeyRepo.generate(n, a, b)
					null
				} catch(e: Exception) {
					log.error("Failed to generate symmetric key.", e)
					e
				}
				withScope(scope) { error.value = err?.message }
			}
		}
	}


	@Composable private fun CKeysList(scope: CoroutineScope, theme: AppTheme) {
		val keys = rcmsn<List<SymKey>?>()
		val loading = rcms(false)
		val listener = remember { MyKeyListMaintainer(scope, keys, loading) }
		SymKeyRepo.listeners.cRemember(scope) { listener }

		val dialog = rcmsn<AppComposition>()
		dialog.value?.Compose(theme)

		val k = keys.value
		if(k != null) {
			CRowWC {
				theme.sectionTitle.C("Keys (${k.size})")
				theme.btn.CIcon(AppIcon.reload, enabled = !loading.value) { listener.forceRefreshFromUiThread() }
			}

			for(key in k) {
				CRowC {
					theme.strong.C(key.name!!)
					theme.body.C(" ${key.key!!.encoded.size * 8}-bit ${key.key.algorithm}")
					theme.btn.CIconPlain(AppIcon.delete) { dialog.value = DeleteSymKeyDialog(key) { dialog.value = null } }
				}
			}
		}
	}


	private suspend fun loadKeysDir(scope: CoroutineScope, target: CLookupSync<String, Path>) {
		val dir = withDefault { SecretPathsRepo.opt()?.root?.toString() ?: "" }
		withScope(scope) { target.submit(dir) }
	}


	/**Maintains a set of all key names. Loads the set initially, and changes it whenever names change.*/
	private class AllKeyNamesMaintainer (
		val scope: CoroutineScope,
		val names: MutableState<Set<String>?>,
	) : SymKeyRepo.Listener {
		init {
			scope.launchDefault { refresh() }
		}

		private suspend fun refresh() {
			val n = SymKeyRepo.getAll().map { it.name!! }.toSet()
			withScope(scope) { names.value = n }
		}

		override suspend fun onKeyUpsert(key: SymKey) = refresh()
		override suspend fun onKeyDelete(key: SymKey) = refresh()
	}


	/**Maintains a list of all keys. Loads the list initially, and changes it whenever keys change.*/
	private class MyKeyListMaintainer (
		val scope: CoroutineScope,
		val list: MutableState<List<SymKey>?>,
		val loading: MutableState<Boolean>,
	) : SymKeyRepo.Listener {
		fun forceRefreshFromUiThread() {
			scope.launchIo { refresh(true) }
		}

		init { forceRefreshFromUiThread() }

		suspend fun refresh(forceRefresh: Boolean = false) {
			withScope(scope) { loading.value = true }
			withDefault {
				val keys = SymKeyRepo.getAll(forceRefresh).sortedBy { it.name }
				withScope(scope) {
					list.value = keys
					loading.value = false
				}
			}
		}

		override suspend fun onKeyUpsert(key: SymKey) = refresh()
		override suspend fun onKeyDelete(key: SymKey) = refresh()
	}
}