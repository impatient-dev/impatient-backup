package imp.backup.cui.mainpanel

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import imp.backup.cui.AppComposition
import imp.backup.cui.AppTheme
import imp.backup.cui.DeleteBackupDialog
import imp.backup.cui.RestoreBackupDialog
import imp.backup.cui.dialog.UpdateBackupDialog
import imp.backup.data.BackupLocation
import imp.backup.data.UserLocation
import imp.backup.repo.BackupI
import imp.backup.todFmt
import imp.backup.util.orQuestionMarks
import imp.compose.*
import imp.util.fmtBytes
import imp.util.toLocal


@Composable fun CMainPanel(
	/**Something large and important, like the location name.*/
	title: String,
	/**The type of location/thing this panel is showing.*/
	typeName: String,
	/**A sentence or 2 explaining what the type name means.*/
	typeDescription: String,
	theme: AppTheme,
	content: @Composable ColumnScope.() -> Unit,
) = CColMaxScrollV {
	CSelV {
		theme.title.CW(title, horizontal = Arrangement.Start)
		theme.subtitle.CW(typeName, horizontal = Arrangement.Start)
		theme.note.C(typeDescription)
	}
	theme.divider.CH()
	content()
}


/**Renders information about a backup.*/
@Composable fun CBackup(
	backup: BackupI,
	dialog: MutableState<AppComposition?>,
	theme: AppTheme,
	fromLocation: UserLocation? = null,
	toLocation: BackupLocation? = null,
)  = CColL {
	theme.titleItem.C(backup.name)
	theme.C(backup.type)
	theme.labelBody.C("to", backup.backupLocationName)
	CRowC {
		if(backup.lastStart == null)
			theme.bodyError.C("No start time")
		else
			theme.body.C(todFmt.dateHm(backup.lastStart.toLocal()))
		theme.body.C(" — ")
		if(backup.lastEnd == null)
			theme.bodyWarn.C("Not Finished")
		else
			theme.body.C(todFmt.dateHm(backup.lastEnd.toLocal()))
	}
	theme.body.C("${backup.files.orQuestionMarks()} files, ${backup.bytes?.let(::fmtBytes) ?: "???"}")
	CRowWR {
		theme.btn.C("Delete") { dialog.value = DeleteBackupDialog(backup, toLocation) { dialog.value = null } }
		theme.btn.C("Restore") { dialog.value = RestoreBackupDialog(backup, toLocation, fromLocation) { dialog.value = null } }
		theme.btn.C("Update") { dialog.value = UpdateBackupDialog(backup, from = fromLocation, to = toLocation, close = { dialog.value = null }) }
	}
}