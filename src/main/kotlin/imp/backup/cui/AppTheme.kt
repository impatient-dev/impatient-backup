package imp.backup.cui

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.material.Colors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontFamily.Companion.Monospace
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import imp.backup.data.BackupType
import imp.compose.ColorPair
import imp.compose.csBorder
import imp.compose.maxContrast
import imp.compose.style.*
import imp.compose.under
import imp.util.desktop.CSIcon
import imp.util.desktop.csIcon

class AppTheme(
	val main: ColorPair,
	primary: Color,
	primaryStrong: Color,
	primaryWeak: Color,
	disabled: ColorPair,
	foreActive: Color,
	foreGood: Color,
	foreWeak: Color,
	foreWarn: Color,
	foreError: Color,
	val backWeak: Color,
	backWarn: Color,
	btnWarn: ColorPair,
	btnDanger: ColorPair,
	backupTypeRaw: Color,
	backupTypeDb: Color,
) {
	private val isLight = main.back.maxContrast() == Color.Black
	private inline val isDark get() = !isLight
	inline val fore: Color get() = main.fore
	inline val back: Color get() = main.back

	val body = appTypography.body1.csText.copy(color = fore)
	val bodyActive = body.copy(color = foreActive)
	val bodyWarn = body.copy(color = foreWarn)
	val bodyError = body.copy(color = foreError)

	val strong = body.copy(weight = FontWeight.W600)
	val strongGood = strong.copy(color = foreGood)
	val strongError = strong.copy(color = foreError)
	val strongWarn = strong.copy(color = foreWarn)

	val mono = body.copy(family = Monospace)

	val title = body.copy(background = backWeak, size = 24.sp, weight = FontWeight.Bold).csTextBar
	val titleActive = title.copy(fore = foreActive)
	val titleGood = title.copy(fore = foreGood)
	val titleWarn = title.copy(fore = foreWarn)
	val titleError = title.copy(fore = foreError)

	val titleMonoWarn = title.copy(family = Monospace, back = backWarn)

	val subtitle = title.copy(size = 16.sp, weight = FontWeight.W600)
	val subtitleWarn = subtitle.copy(back = backWarn)

	val sectionTitle = body.copy(size = 22.sp, weight = FontWeight.W600)
	val sectionTitleActive = sectionTitle.copy(color = foreActive)
	val sectionTitleError = sectionTitle.copy(color = foreError)
	val sectionTitleOff = sectionTitle.copy(color = foreWeak)

	/**A title for an item in a list.*/
	val titleItem = body.copy(size = 16.sp, weight = FontWeight.W600)

	/**A label indicating what the nearby body text means.*/
	val labelBody = body.csLabeled(body.copy(color = foreWeak, size = 12.sp), 8.dp)
	/**Small, gray text.*/
	val note = body.copy(color = foreWeak, size = 12.sp)

	val link = body.csLink(disabled.fore, hpad = 4.dp)
	val linkActive = link.copy(fore = foreActive)
	val linkError = link.copy(fore = foreError)

	val btn = csBtnText(body.family, 16.sp, FontWeight.W500, primary.under(), disabled).csIcon()
	val btnWarn = btn.copy(btnWarn)
	val btnDanger = btn.copy(btnDanger)
	val icon = CSIcon(fore)

	val toggle = CSToggle(
		primaryStrong.under().csText(body.family, 16.sp, FontWeight.W600),
		primaryWeak.under().csText(body.family, 16.sp, FontWeight.W600),
		10.dp,
	)

	val divider = CSDivider(fore)

	val boxTitle = csTextBar(body.family, ColorPair(primaryWeak, fore), 16.sp, FontWeight.W600)
	val boxBorder = csBorder(backWeak, 2.dp, 2.dp)

	val material = Colors(
		primary = primary,
		primaryVariant = primary,
		secondary = primary,
		secondaryVariant = primary,
		background = main.back,
		surface = main.back,
		error = foreError,
		onPrimary = primary.maxContrast(),
		onSecondary = primary.maxContrast(),
		onBackground = main.fore,
		onSurface = main.fore,
		onError = foreError.maxContrast(),
		isLight = main.fore.maxContrast() == Color.White,
	)


	val backupTypeRaw = strong.copy(color = backupTypeRaw)
	val backupTypeDb = strong.copy(color = backupTypeDb)
	@Composable fun C(type: BackupType) = (if(type == BackupType.RAW) backupTypeRaw else backupTypeDb).C(type.userFriendly)
}


/**Horizontal padding in major containers so text doesn't bump right against the left/right edge.*/
val hpad = 2.dp
val pad = PaddingValues(horizontal = hpad)
val padBig = 12.dp
/**Padding between 2 pieces of text that shouldn't touch.*/
val txtPad = 10.dp



@FunctionalInterface interface AppComposition {
	@Composable fun Compose(theme: AppTheme)
}