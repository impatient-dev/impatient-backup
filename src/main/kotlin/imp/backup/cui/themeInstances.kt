package imp.backup.cui

import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Shapes
import androidx.compose.material.Typography
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import imp.compose.ColorPair
import imp.compose.opaque
import imp.compose.opaquePair
import imp.compose.style.csText


val appTypography = Typography(body1 = csText(FontFamily.SansSerif, Color.Unspecified, 14.sp).style)

private val appShapes = Shapes(
	small = RoundedCornerShape(4.dp),
	medium = RoundedCornerShape(4.dp),
	large = RoundedCornerShape(0.dp)
)

private val lightTheme = AppTheme(
	main = ColorPair(Color.White, Color.Black),
	primary = opaque(0x0277bd), // light blue 800
	primaryStrong = opaque(0x004c8c),
	primaryWeak = opaque(0x58a5f0),
	disabled = opaquePair(0xaaaaaa, 0x666666),
	foreActive = opaque(0x006699),
	foreGood = opaque(0x009922),
	foreWeak = opaque(0x666666),
	foreWarn = opaque(0xffaa22),
	foreError = Color.Red,
	backWeak = opaque(0xcccccc),
	backWarn = opaque(0xffaaaa),
	btnWarn = opaquePair(0xE65100, 0xFFE0B2), // orange 900, orange 100
	btnDanger = opaquePair(0xB71C1C, 0xFFCDD2, ), // red 900, red 100
	backupTypeRaw = opaque(0x663300),
	backupTypeDb = opaque(0x330066),
)


@Composable fun CAppTheme(content: @Composable (AppTheme) -> Unit) {
	val theme = lightTheme
	MaterialTheme(
		colors = theme.material,
		typography = appTypography,
		shapes = appShapes,
	) {
		content(theme)
	}
}