package imp.backup.cui

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.window.Window
import imp.backup.cui.mainpanel.*
import imp.backup.cui.sidebar.CMainSidebar
import imp.backup.data.BackupLocation
import imp.backup.data.Location
import imp.backup.data.MissingLocation
import imp.backup.data.UserLocation
import imp.backup.net.AppServer
import imp.backup.net.PeerConnI
import imp.compose.CSurface
import imp.compose.cRemember
import imp.compose.cms
import imp.compose.cmsn
import imp.util.withScope
import kotlinx.coroutines.CoroutineScope
import kotlin.system.exitProcess

class MainWindow {
	private val dialog = cmsn<AppComposition>()
	private val detailPane = cmsn<AppComposition>()
	private val serverRunning = cms(false) // the server is never running when the GUI starts

	@Composable fun compose() = CAppTheme { theme ->
		val scope = rememberCoroutineScope()
		AppServer.listeners.cRemember(scope) { MyServerListener(scope) }
		dialog.value?.Compose(theme)

		Window(
			title = "impatient-backup",
			onCloseRequest = { exitProcess(0) },
		) {
			CSurface { renderWindowContents(theme) }
		}
	}

	@Composable private fun renderWindowContents(theme: AppTheme) {
		Column {
			Row(Modifier.fillMaxWidth().weight(1f)) {
				Box(Modifier.fillMaxHeight().weight(1f).padding(pad)) {
					CMainSidebar(
						onSelectLocation = { openLocationPane(it) },
						onSelectConnection = { openConnectionPane(it) },
						theme,
					)
				}
				theme.divider.CV()
				Box(Modifier.weight(1f).fillMaxHeight().padding(pad)) {
					detailPane.value?.Compose(theme)
				}
			}
			Row(Modifier.fillMaxWidth().background(theme.backWeak).padding(pad)) {
				theme.link.C("Keys") { detailPane.value = KeysPanel }
				(if(serverRunning.value) theme.linkActive else theme.link).C("Server") { detailPane.value = ServerPanel }
			}
		}
	}


	/**Close whatever detail panel is currently open, if any.*/
	fun closeDetailPane() {
		detailPane.value = null
	}

	private fun openLocationPane(location: Location?) {
		val pane = when(location) {
			is UserLocation -> UserLocationPanel(location, this)
			is BackupLocation -> BackupLocationPanel(location, this)
			is MissingLocation -> MissingLocationPanel(location, this)
			else -> null
		}
		detailPane.value = pane
	}

	private fun openConnectionPane(conn: PeerConnI?) {
		val pane = if(conn == null) null else null //TODO
		detailPane.value = pane
	}

	private inner class MyServerListener(private val scope: CoroutineScope): AppServer.Listener {
		override suspend fun onServerStart() {
			withScope(scope) { serverRunning.value = true }
		}
		override suspend fun onServerStop() {
			withScope(scope) { serverRunning.value = false }
		}
	}
}