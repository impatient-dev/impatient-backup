package imp.backup.cui.sidebar

import androidx.compose.foundation.clickable
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import imp.backup.cui.AppTheme
import imp.backup.data.Location
import imp.backup.net.PeerConnI
import imp.compose.CColMaxScrollV
import imp.compose.CColWL
import imp.compose.CWeight


@Composable fun <L: Location> CMainSidebarLocations(
	locations: List<L>,
	theme: AppTheme,
	onSelect: (L) -> Unit,
) = CColMaxScrollV {
	for(location in locations) {
		CColWL(Modifier.clickable { onSelect(location) }) {
			val name = location.name
			if(name != null)
				theme.titleItem.C(name)
			theme.body.C(location.searchPath.toString())
		}
	}
	CWeight()
}


@Composable fun CMainSidebarConnections(
	conns: List<PeerConnI>,
	theme: AppTheme,
	onSelect: (PeerConnI) -> Unit,
) = CColMaxScrollV {
	for(conn in conns) {
		CColWL(Modifier.clickable { onSelect(conn) }) {
			theme.titleItem.C(conn.channel.remoteAddr.hostString)
		}
	}
	CWeight()
}