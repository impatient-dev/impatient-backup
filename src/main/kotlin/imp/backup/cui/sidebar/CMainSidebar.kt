package imp.backup.cui.sidebar

import androidx.compose.foundation.layout.Row
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.snapshots.SnapshotStateList
import imp.backup.compareBackupLocations
import imp.backup.compareMissingLocations
import imp.backup.comparePatchLocations
import imp.backup.compareUserLocations
import imp.backup.cui.AppComposition
import imp.backup.cui.AppIcon
import imp.backup.cui.AppTheme
import imp.backup.cui.dialog.AddDialog
import imp.backup.data.*
import imp.backup.net.AppWebsockets
import imp.backup.net.PeerConnI
import imp.backup.repo.LocationRepo
import imp.compose.*
import imp.util.*
import imp.web.ImpWs
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

private val log = MainSidebarView::class.logger

enum class MainSidebarView {
	USER_LOCATIONS,
	BACKUP_LOCATIONS,
	PATCH_LOCATIONS,
	MISSING_LOCATIONS,
	CONNECTIONS,
}

/**Returns the matching argument.*/
private fun <T> MainSidebarView.choose(user: T, backup: T, patch: T, missing: T, connections: T): T = when(this) {
	MainSidebarView.USER_LOCATIONS -> user
	MainSidebarView.BACKUP_LOCATIONS -> backup
	MainSidebarView.PATCH_LOCATIONS -> patch
	MainSidebarView.MISSING_LOCATIONS -> missing
	MainSidebarView.CONNECTIONS -> connections
}


/**Shows all locations and connections, and lets the user select one.*/
@Composable fun CMainSidebar(
	onSelectLocation: (Location) -> Unit,
	onSelectConnection: (PeerConnI) -> Unit,
	theme: AppTheme,
) {
	val scope = rememberCoroutineScope()
	val dialog = rcmsn<AppComposition>()
	val loading = rcms(true)
	val user = rcmsl<UserLocation>()
	val backup = rcmsl<BackupLocation>()
	val patch = rcmsl<PatchLocation>()
	val missing = rcmsl<MissingLocation>()
	val conns = rcmsl<PeerConnI>()
	val view = rcms(MainSidebarView.USER_LOCATIONS)
	/**Item we're showing details on - either a location or a connection.*/
	val detail = rcms<Any?>(null)
	val currentList = view.value.choose(user, backup, patch, missing, conns)

	LocationRepo.listeners.cRemember(scope) { MyLocationListener(scope, user, backup, patch, missing, detail) }
	AppWebsockets.listeners.cRemember(scope) { MyWsListener(scope, conns) }

	LaunchedEffect(null) {
		withDefault {
			LocationRepo.refresh() // fires listener methods
			withScope(scope) { loading.value = false }
		}
	}

	dialog.value?.Compose(theme)

	CColC {
		Row {
			CView("U", user, view, MainSidebarView.USER_LOCATIONS, theme)
			CView("B", backup, view, MainSidebarView.BACKUP_LOCATIONS, theme)
			CView("P", patch, view, MainSidebarView.PATCH_LOCATIONS, theme)
			CView("?", missing, view, MainSidebarView.MISSING_LOCATIONS, theme)
			CView("C", conns, view, MainSidebarView.CONNECTIONS, theme)
		}
		CWeightBox {
			if(view.value == MainSidebarView.CONNECTIONS)
				CMainSidebarConnections(conns, theme, onSelectConnection)
			else
				CMainSidebarLocations(currentList as List<Location>, theme, onSelectLocation)
		}
		theme.note.C(description(view.value))
		CRowWR {
			if(loading.value)
				CSpinner()
			theme.btn.CIcon(AppIcon.plus) { dialog.value = AddDialog(view.value != MainSidebarView.CONNECTIONS) { dialog.value = null } }
			theme.btn.CIcon(AppIcon.reload, enabled = !loading.value) { refresh(scope, loading) }
		}
	}
}


@Composable private fun CView(
	name: String,
	list: List<*>,
	currentView: MutableState<MainSidebarView>,
	view: MainSidebarView,
	theme: AppTheme,
) {
	theme.toggle.COption("$name (${list.size})", currentView.value == view) { currentView.value = view }
}


private fun refresh(
	scope: CoroutineScope,
	loading: MutableState<Boolean>,
) {
	loading.value = true
	scope.launchDefault {
		LocationRepo.refresh()
		withScope(scope) { loading.value = false }
	}
}

private fun description(view: MainSidebarView): String = when(view) {
	MainSidebarView.USER_LOCATIONS -> "User locations: locations containing files you want to back up, such as My Documents."
	MainSidebarView.BACKUP_LOCATIONS -> "Backup locations: locations where backups are stored, such as an external drive."
	MainSidebarView.PATCH_LOCATIONS -> "Patch locations: locations where you apply patches (e.g. video game mods)."
	MainSidebarView.MISSING_LOCATIONS -> "Missing locations: locations that are invalid or don't currently exist (e.g. the backup drive isn't plugged in)."
	MainSidebarView.CONNECTIONS -> "Connections to other computers."
}


/**Listens for location changes, and updates the provided variables when changes happen.
 * Everything happens on the dispatcher of the provided coroutine scope (should be a main dispatcher).*/
private class MyLocationListener (
	val scope: CoroutineScope,
	val user: MutableList<UserLocation>,
	val backup: MutableList<BackupLocation>,
	val patch: MutableList<PatchLocation>,
	val missing: MutableList<MissingLocation>,
	val selected: MutableState<Any?>,
) : LocationRepo.Listener {
	private val log = MyLocationListener::class.logger

	override suspend fun postAddLocation(location: Location) = launchUnit(scope) {
		log.trace("Adding location: {}.", location)
		when (location) {
			is UserLocation -> user.addSortedWith(location, allowReplace = true, compare = ::compareUserLocations)
			is BackupLocation -> backup.addSortedWith(location, allowReplace = true, compare = ::compareBackupLocations)
			is PatchLocation -> patch.addSortedWith(location, allowReplace = true, compare = ::comparePatchLocations)
			is MissingLocation -> missing.addSortedWith(location, allowReplace = true, compare = ::compareMissingLocations)
		}
	}

	override suspend fun postRemoveLocation(location: Location) = launchUnit(scope) {
		log.trace("Removing location: {}.", location)
		when (location) {
			is UserLocation -> user.removeSortedWith(location, ::compareUserLocations)
			is BackupLocation -> backup.removeSortedWith(location, ::compareBackupLocations)
			is PatchLocation -> patch.removeSortedWith(location, ::comparePatchLocations)
			is MissingLocation -> missing.removeSortedWith(location, ::compareMissingLocations)
		}
		if(selected.value == location)
			selected.value = null
	}

	override suspend fun postChangeLocation(old: Location, new: Location) = launchUnit(scope) {
		postRemoveLocation(old)
		postAddLocation(new)
		if(selected.value == old)
			selected.value = new
	}
}


/**Maintains an up-to-date list of current connections, sorted by host string.*/
private class MyWsListener (
	private val scope: CoroutineScope,
	private val conns: SnapshotStateList<PeerConnI>,
) : AppWebsockets.Listener {
	private inline fun update(index: Int, block: (PeerConnI) -> PeerConnI) {
		conns.set(index, block(conns[index]))
	}

	override fun onWsOpen(ch: ImpWs) {
		val entry = PeerConnI(ch)
		scope.launch {
			val i = conns.indexOfFirst { !it.open && it.hostStr == entry.hostStr }
			if(i == -1)
				conns.addSortedBy(entry) { it.hostStr }
			else
				conns[i] = entry
		}
	}

	override fun onWsClose(ch: ImpWs) = launchUnit(scope) {
		val i = conns.indexOfFirst { it.channel === ch }
		if(i != -1)
			update(i) { it.copy(open = false) }
	}

	override fun onWsFatal(ch: ImpWs, e: Throwable) {
		log.error("Fatal error with websocket ${ch.remoteAddr.hostString}.", e)
	}
}