package imp.backup.cui

object AppIcon {
	val arrowRightBold = "icons/arrow-right-bold.svg"
	val delete = "icons/alpha-x.svg"
	val plus = "icons/plus.svg"
	val reload = "icons/reload.svg"
	val down = "icons/menu-down.svg"
	val right = "icons/menu-right.svg"
}