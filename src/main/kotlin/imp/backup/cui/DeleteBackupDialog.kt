package imp.backup.cui

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.rememberCoroutineScope
import imp.backup.data.BackupDecl
import imp.backup.data.BackupLocation
import imp.backup.repo.BackupI
import imp.backup.repo.LocationRepo
import imp.compose.*
import imp.util.desktop.CDialog
import imp.util.ioScope
import imp.util.withScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

private val deleteScope = ioScope()

/**After user confirmation, deletes a backup.*/
class DeleteBackupDialog (
	val info: BackupI,
	location: BackupLocation?,
	private val close: () -> Unit,
) : AppComposition {
	private val state = cms(State.LOADING)
	private val backup = cmsn<BackupDecl>()
	private val location = cmsn(location)

	@Composable override fun Compose(theme: AppTheme) = CDialog("Delete Backup", ::tryClose) {
		CColMaxScrollV {
			val s = state.value
			val loc = location.value
			val b = backup.value
			val scope = rememberCoroutineScope()
			LaunchedEffect(null) { initialLoad(scope) }

			theme.sectionTitle.C("Delete Backup?")
			CRowC {
				theme.strong.C(info.name)
				CHPad(txtPad)
				theme.C(info.type)
			}
			theme.labelBody.C("Location", info.backupLocationName)
			if(s == State.DELETING || s == State.LOADING)
				CSpinner()
			else if(loc == null)
				theme.strongError.C("Location not found, or there are duplicates.")
			CRowC {
				theme.link.C("Cancel", enabled = s != State.DELETING, onClick = ::tryClose)
				theme.btnWarn.C("Delete", enabled = s == State.READY && loc != null && b != null) { delete(b!!, scope) }
			}
		}
	}

	private fun initialLoad(scope: CoroutineScope) {
		var loc = location.value
		scope.launch {
			if(loc == null)
				loc = LocationRepo.getBackupIf1(info.backupLocationName)
			val b = loc?.backup(info.type, info.name)
			withScope(scope) {
				location.value = loc
				backup.value = b
				state.value = State.READY
			}
		}
	}

	private fun delete(backup: BackupDecl, scope: CoroutineScope) {
		state.value = State.DELETING
		deleteScope.launch {
			backup.delete()
			withScope(scope) {
				close()
			}
		}
	}


	private fun tryClose() {
		if(state.value != State.DELETING)
			close()
	}


	private enum class State {
		LOADING,
		READY,
		DELETING,
	}
}