package imp.backup.crypto

import imp.backup.files.FullLocFileOutputStream
import imp.backup.files.LocFileInputStream
import imp.backup.files.RawLocFileOutputStream
import kotlinx.coroutines.CoroutineScope
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import kotlin.math.max

/**Wraps another stream and encrypts the input.
 * This object will create the cipher and allow it to choose an initialization vector;
 * you need to save that initialization vector in order to be able to decrypt this later.*/
class CryptoWrapLocFileOutputStream (
	private val wrap: RawLocFileOutputStream,
	key: SymKey,
) : FullLocFileOutputStream {
	private val cipher = Cipher.getInstance(key.key!!.algorithm)
	private var ciphertextBuffer: ByteArray? = null

	init {
		cipher.init(Cipher.ENCRYPT_MODE, key.key)
	}

	override suspend fun write(buffer: ByteArray, scope: CoroutineScope, count: Int) {
		var cb = ciphertextBuffer
		val minSize = cipher.getOutputSize(count)
		if(cb == null || cb.size < minSize) {
			cb = ByteArray(max(minSize, buffer.size))
			ciphertextBuffer = cb
		}
		val cc = cipher.update(buffer, 0, count, cb)
		if(cc > 0)
			wrap.write(cb, scope, cc)
	}

	override suspend fun finish(scope: CoroutineScope) {
		val result = cipher.doFinal()
		if(result!= null && result.isNotEmpty())
			wrap.write(result, scope)
		wrap.finish(cipher.iv, scope)
	}

	override fun close() = wrap.close()
}



/**Wraps another stream and decrypts the output.*/
class CryptoWrapLocFileInputStream (
	private val wrap: LocFileInputStream,
	key: SymKey,
	initializationVector: ByteArray?,
) : LocFileInputStream {
	private val cipher = Cipher.getInstance(key.key!!.algorithm)
	private var ciphertextBuffer: ByteArray? = null

	init {
		val iv = initializationVector?.let { IvParameterSpec(it) }
		cipher.init(Cipher.DECRYPT_MODE, key.key, iv)
	}

	override suspend fun read(buffer: ByteArray, scope: CoroutineScope): Int {
		var cb = ciphertextBuffer
		val minSize = cipher.getOutputSize(buffer.size)
		if(cb == null || cb.size < minSize) {
			cb = ByteArray(max(minSize, buffer.size))
			ciphertextBuffer = cb
		}
		val count = wrap.read(cb, scope)
		if(count != -1)
			return cipher.update(cb, 0, count, buffer)
		val out = cipher.doFinal(buffer, 0)
		return if(out == 0) -1 else out
	}

	override fun close() = wrap.close()
}