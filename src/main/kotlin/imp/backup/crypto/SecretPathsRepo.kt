package imp.backup.crypto

import imp.backup.AppPaths
import imp.util.BasicCoListeners
import imp.util.SusRepo1
import imp.util.impWrite
import imp.util.logger
import java.nio.file.Path
import kotlin.io.path.deleteIfExists
import kotlin.io.path.exists
import kotlin.io.path.readText

private val log = SecretPathsRepo::class.logger

/**Paths to sensitive files. These files are not meant to be stored with the rest of the app config, but instead in an encrypted location.*/
class SecretPaths(
	val root: Path,
) {
	val symKeysDir = root.resolve("sym-keys")
	val serverKeystorePwd = root.resolve("server-keystore-password.txt")
}


/**Loads the location where sensitive files are stored, separate from the rest of the app config.
 * Null if the user hasn't chosen a directory.*/
object SecretPathsRepo {
	private val file = AppPaths.secretLink

	suspend fun opt(): SecretPaths? = loader.get()
	suspend fun req(): SecretPaths = opt() ?: throw Exception("Secrets directory location is not configured.")
	suspend fun set(path: SecretPaths?) = loader.set(path)


	private val loader = object : SusRepo1<SecretPaths?>() {
		override suspend fun load(): SecretPaths? {
			if(!file.exists())
				return null
			try {
				val str = file.readText()
				return SecretPaths(Path.of(str))
			} catch(e: Exception) {
				log.error("Failed to read secret directory path.", e)
				return null
			}
		}

		override suspend fun save(item: SecretPaths?) {
			log.debug("Saving secrets dir {}.", item)
			if(item == null)
				file.deleteIfExists()
			else
				file.impWrite(item.toString())
			_listeners.fireAsync { it.onSecretPathsChange(item) }
		}
	}


	private val _listeners = BasicCoListeners<Listener>()
	val listeners get() = _listeners.toInterface
	interface Listener {
		suspend fun onSecretPathsChange(path: SecretPaths?)
	}
}