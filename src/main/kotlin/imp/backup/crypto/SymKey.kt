package imp.backup.crypto

import imp.backup.jsonMapper
import imp.json.impRead
import imp.json.impWrite
import imp.util.*
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import java.util.*
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec
import kotlin.io.path.deleteIfExists

/**A key for symmetric encryption.
 * The name and key are normally present; these are only null when we're trying to represent that something was encrypted with a key we don't recognize.*/
data class SymKey (
	val uuid: UUID,
	val name: String?,
	val key: SecretKey?,
)
private fun SymKey.toJson(): SymKeyJ {
	check(name != null && key != null)
	val base64 = Base64.getEncoder().encodeToString(key.encoded)
	return SymKeyJ(uuid, name, key.algorithm, base64)
}

private data class SymKeyJ(
	val uuid: UUID,
	val name: String,
	val algorithm: String,
	val keyBase64: String,
) {
	fun parsed(): SymKey {
		val bytes = Base64.getDecoder().decode(keyBase64)
		val key = SecretKeySpec(bytes, algorithm)
		return SymKey(uuid, name, key)
	}
}



class CiphertextPair(val ciphertext: ByteArray, val iv: ByteArray?)

fun encrypt(key: SymKey, str: String): CiphertextPair {
	check(key.key != null) { "Missing key - unable to encrypt." }
	val cipher = Cipher.getInstance(key.key.algorithm)
	cipher.init(Cipher.ENCRYPT_MODE, key.key)
	val iv = cipher.iv
	return CiphertextPair(cipher.doFinal(str.toByteArray()), iv)
}

/**Parses the bytes as a UTF-8 string after decrypting the bytes.*/
fun decrypt(key: SymKey, bytes: ByteArray, iv: ByteArray?): String {
	check(key.key != null) { "Missing key - unable to decrypt." }
	val ivp = iv?.let(::IvParameterSpec)
	val cipher = Cipher.getInstance(key.key.algorithm)
	cipher.init(Cipher.DECRYPT_MODE, key.key, ivp)
	return String(cipher.doFinal(bytes))
}



/**Keeps track of all known symmetric keys that can be used to encrypt/decrypt backups.
 * Unknown/unrecognized keys are never provided to or returned by this class.*/
object SymKeyRepo {
	private val log = SymKeyRepo::class.logger
	private val mutex = Mutex()
	/**Null until loaded the first time.*/
	private var byId: Map<UUID, SymKey>? = null
	private var initialized = false

	private suspend fun file(key: SymKey) = SecretPathsRepo.req().symKeysDir.resolve(key.uuid.toString())

	private suspend fun loadIfNecessary(forceReload: Boolean = false) {
		if(!forceReload && byId != null)
			return
		mutex.withLock { lockedLoadIfNecessary(forceReload) }
	}

	private suspend fun lockedLoadIfNecessary(forceReload: Boolean = false) {
		if(!forceReload && byId != null)
			return
		if(!initialized) {
			SecretPathsRepo.listeners.add(MyDirListener())
			initialized = true
		}
		val dirs = SecretPathsRepo.opt()
		if(dirs == null) {
			byId = emptyMap()
		} else {
			val map = HashMap<UUID, SymKey>()
			dirs.symKeysDir.impDirList { file ->
				try {
					val key = jsonMapper.impRead<SymKeyJ>(file).parsed()
					map.putNew(key.uuid, key)
				} catch(e: Exception) {
					log.error("Error parsing symmetric key in $file.", e)
				}
			}
			byId = map
		}
	}

	suspend fun getAll(forceReload: Boolean = false): Collection<SymKey> {
		loadIfNecessary(forceReload)
		return byId!!.values
	}

	/**Returns null if the key with this UUID can't be found.*/
	suspend fun opt(uuid: UUID): SymKey? {
		loadIfNecessary()
		return byId!![uuid]
	}

	/**If the key with this UUID can't be found, returns a key with a null name and SecretKey.*/
	suspend fun orMissing(uuid: UUID): SymKey = opt(uuid) ?: SymKey(uuid, null, null)

	suspend fun req(uuid: UUID): SymKey = opt(uuid) ?: throw Exception("Encryption key not found: $uuid")

	/**Creates a new key with the given parameters.*/
	suspend fun generate(name: String, algorithm: String, keySize: Int?) = mutex.withLock {
		lockedLoadIfNecessary()
		log.info("Creating {}-bit key with algorithm {}.", keySize, algorithm)
		val uuid = UUID.randomUUID()
		val gen = KeyGenerator.getInstance(algorithm)
		if(keySize != null)
			gen.init(keySize)
		val key = SymKey(uuid, name, gen.generateKey())
		jsonMapper.impWrite(file(key), key.toJson())
		byId = byId!!.plus(Pair(uuid, key))
		_listeners.fireAsync { it.onKeyUpsert(key) }
	}

	suspend fun changeName(key: SymKey, name: String) = mutex.withLock {
		lockedLoadIfNecessary()
		val changed = key.copy(name = name)
		jsonMapper.impWrite(file(changed), changed.toJson())
		byId = byId!!.plus(Pair(key.uuid, changed))
		_listeners.fireAsync { it.onKeyUpsert(changed) }
	}

	suspend fun delete(key: SymKey) = mutex.withLock {
		lockedLoadIfNecessary()
		file(key).deleteIfExists()
		byId = byId!!.minus(key.uuid)
		_listeners.fireAsync { it.onKeyDelete(key) }
	}


	private class MyDirListener : SecretPathsRepo.Listener {
		override suspend fun onSecretPathsChange(path: SecretPaths?) {
			getAll(true)
		}
	}


	private val _listeners = BasicCoListeners<Listener>()
	val listeners: CoListeners<Listener> = _listeners
	interface Listener {
		suspend fun onKeyUpsert(key: SymKey)
		suspend fun onKeyDelete(key: SymKey)
	}
}