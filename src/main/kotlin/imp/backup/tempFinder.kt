package imp.backup

import imp.util.forEachIn
import org.apache.commons.io.FileUtils
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

private val maxFileBytes = 10_000_000L

fun main() {
	val dir = "/foo/bar"
	val search = "something"

	val results = ArrayList<Path>()
	findRecursively(Paths.get(dir), search, results)

	println("\n\n\nFound ${results.size} results:")
	for(result in results)
		println(result)
}


private fun findRecursively(dir: Path, search: String, results: MutableList<Path>) {
	dir.forEachIn { path ->
		if(Files.isDirectory(path))
			findRecursively(path, search, results)
		else {
			val size = Files.size(path)
			if(size <= maxFileBytes && couldBePlainTextFile(path.fileName.toString())) {
				println("Searching $path")
				val content = FileUtils.readFileToString(path.toFile(), Charsets.UTF_8)
				if(content.contains(search)) {
					println("\t\tFound!")
					results.add(path)
				}
			}
		}
	}
}


private fun couldBePlainTextFile(filename: String): Boolean {
	val dotIdx = filename.lastIndexOf('.')
	if(dotIdx == -1)
		return true
	val extension = filename.substring(dotIdx + 1, filename.length)

	return when(extension.lowercase()) {
		"bmp", "dds", "gif", "heic", "jpg", "jpeg", "png", "webp" -> false
		"avi", "flac", "mp3", "mp4" -> false
		"docx", "pdf" -> false
		"7z", "zip" -> false
		"jar", "class" -> false
		"txt", "md" -> true
		else -> true
	}
}