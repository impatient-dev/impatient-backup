package imp.backup.net

import imp.util.BasicCoListeners
import imp.util.CoListeners
import imp.web.ImpWebserver
import imp.web.ImpWebserverCfg
import imp.web.ImpWsServerCfg
import imp.web.undertow.startUndertow
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock


class AppServerCfg (
	val host: String,
) {
	fun toWeb() = ImpWebserverCfg(host, WS_PORT_LOCAL, null, ImpWsServerCfg(WS_PATH) { AppWebsockets })
}

/**Starts and stops the embedded websocket server.*/
object AppServer {
	private val mutex = Mutex()
	private var server: ImpWebserver? = null
	private val _listeners = BasicCoListeners<Listener>()
	val listeners: CoListeners<Listener> get() = _listeners

	suspend fun startIfNotRunning(config: AppServerCfg) = mutex.withLock {
		if(server != null)
			return
		server = config.toWeb().startUndertow()
		_listeners.fireAsync { it.onServerStart() }
	}

	suspend fun stopIfRunning() = mutex.withLock {
		val s = server ?: return
		s.stop()
		server = null
		_listeners.fireAsync { it.onServerStop() }
	}

	suspend fun isRunning(): Boolean = mutex.withLock { server != null }


	interface Listener {
		suspend fun onServerStart()
		suspend fun onServerStop()
	}
}