package imp.backup.net

import imp.util.defaultScope
import imp.util.logger
import imp.web.ImpWs
import imp.web.remoteHost
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import java.security.cert.X509Certificate

private val log = WsAuthRepo::class.logger

/**It's not acceptable for auth to take this long on an incoming connection.
 * (Outgoing connections can take as long as they want - the user may need to approve,
 * and can always cancel if nothing is happening).*/
private const val AUTH_TIMEOUT_MS = 30_000L


data class OpenWs(
	val ws: ImpWs,
	val remoteCert: X509Certificate,
	/**Information about whether we would recognize the other side as an admin, if authentication was finished.*/
	val admin: SavedAdmin,
	/**The next message we are expecting to receive related to us authenticating the other side,
	 * or null if they passed our challenge and no further auth is needed.*/
	val nextMsgToAuthThem: AuthStage?,
	/**The next message we are expecting to receive related to them authenticating us,
	 * or null if they have said we passed the challenge and are fully authenticated.*/
	val nextMsgToAuthUs: AuthStage?,
	val trustThem: TrustLevel,
	val trustUs: TrustLevel,
) {
	val isOutgoing = ws.isClient
	inline val isIncoming get() = !isOutgoing
	inline val isOpen get() = ws.isOpen
	inline val isClosed get() = !isOpen

	enum class AuthStage {
		DEMAND_INFO,
		PROVIDE_INFO,
		CHALLENGE,
		SIGNATURE,
		SUCCESS,
	}
}



/**Keeps track of open websockets and whether they have authenticated yet.
 * This class handles the details of authentication - you just need to check the trust level on the objects this class returns.*/
object WsAuthRepo {
	private val mutex = Mutex()
	private val map = HashMap<ImpWs, OpenWs>()
	private val scope = defaultScope()

	suspend fun opt(ws: ImpWs): OpenWs? = map[ws]


	/**Checks whether we consider the other side of this websocket an admin.
	 * If we do, runs the block. If not, the websocket is closed with an error.
	 * The action descries what restricted action we're trying to do, e.g. "add entry".*/
	suspend fun admin(ws: ImpWs, action: String, block: suspend () -> Unit) {
		mutex.withLock {
			val record = map[ws]
			if(record == null || record.trustThem < TrustLevel.ADMIN) {
				wsReportNaughty(ws, "Non-admin cannot $action")
				map.remove(ws)
				return
			}
		}
		block()
	}


	/**Adds a new websocket and starts the process of authenticating.*/
	suspend fun onOpen(ws: ImpWs) {
		val cert = ws.remoteCert
		if(cert == null) {
			log.warn("No certificate on ws {} - closing.", ws.remoteHost)
			ws.close()
			return
		}
		mutex.withLock {

		}
		TODO()
	}

	suspend fun onClose(ws: ImpWs) = mutex.withLock { map.remove(ws) }

	suspend fun handleDemand(ws: ImpWs) {
		TODO()
	}

	suspend fun handleResponse(auth: AuthJ, ws: ImpWs) {
		TODO()
	}

	suspend fun handleChallenge(bytes: ByteArray, ws: ImpWs) {
		TODO()
	}

	suspend fun handleSignature(bytes: ByteArray, ws: ImpWs) {
		TODO()
	}

	suspend fun handleSuccess(trust: TrustLevel, ws: ImpWs) {

	}
}