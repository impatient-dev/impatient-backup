package imp.backup.net

import imp.backup.backScope
import imp.util.fmt.Millis
import imp.util.logger
import imp.util.withIo
import imp.web.ImpWs
import imp.web.idmf.IdmfSender
import imp.web.isClosed
import imp.web.remoteHost
import kotlinx.coroutines.*
import java.lang.System.currentTimeMillis

private val log = RemoteLocationsRepo::class.logger
/**Old responses are subject to more checks by the cleanup coroutine.*/
private const val OLD_RESPONSE_MS = 2 * Millis.MINUTE
private const val AlWAYS_DELETE_MS = 30 * Millis.MINUTE
private val sender = IdmfSender(epGetLocations)


/**Keeps track of what locations and backups exist on remote machines.*/
object RemoteLocationsRepo {
	private val sync = Object()
	private val responses = HashMap<ImpWs, MyEntry>()
	private var expireJob: Job? = null


	/**Returns a cached response, or sends a request for a new one if necessary.
	 * This call should be wrapped in a timeout.*/
	suspend fun getAll(ws: ImpWs, scope: CoroutineScope, forceRefresh: Boolean = false): AllLocationsJ = withIo {
		val minMs = if(forceRefresh) currentTimeMillis() else Long.MIN_VALUE
		return@withIo synchronized(sync) {
			getAllWithLock(minMs, ws, scope)
		}
	}

	private fun getAllWithLock(minMs: Long, ws: ImpWs, scope: CoroutineScope): AllLocationsJ {
		var first = true
		while(true) {
			val out = responses[ws]
			if(out != null && out.ms >= minMs)
				return out.msg
			scope.ensureActive()
			if(first) {
				log.debug("Requesting locations from {}.", ws.remoteHost)
				sender.send(Unit, ws)
				first = false
			}
			sync.wait(1_000)
		}
	}

	suspend fun handle(msg: AllLocationsJ, ws: ImpWs) = withIo {
		synchronized(sync) {
			responses[ws] = MyEntry(msg, ws)
			if(expireJob == null)
				expireJob = backScope.launch { periodicCleanup() }
			sync.notifyAll()
		}
	}


	/**Periodically removes entries for closed websockets from the map.*/
	private suspend fun periodicCleanup() {
		val toCheck = ArrayList<MyEntry>()
		while(true) {
			delay(1 * Millis.MINUTE)

			synchronized(sync) {
				val it = responses.iterator()
				val now = currentTimeMillis()
				val oldCutoff = now - OLD_RESPONSE_MS
				val alwaysDeleteCutoff = now - AlWAYS_DELETE_MS
				while(it.hasNext()) {
					val entry = it.next().value
					if(entry.ws.isClosed || entry.ms < alwaysDeleteCutoff)
						it.remove()
					else if(entry.ms < oldCutoff)
						toCheck.add(entry)
				}
			}

			// remove anything the auth repo doesn't know about (check is only performed on old responses)
			if(toCheck.isEmpty())
				continue
			val chkIt = toCheck.iterator()
			while(chkIt.hasNext()) {
				val c = chkIt.next()
				if(WsAuthRepo.opt(c.ws) != null)
					chkIt.remove()
			}
			if(toCheck.isEmpty())
				continue
			synchronized(sync) {
				for(rm in toCheck)
					responses.remove(rm.ws)
			}
			toCheck.clear()
		}
	}


	private class MyEntry (
		val msg: AllLocationsJ,
		val ws: ImpWs,
		val ms: Long = currentTimeMillis(),
	)
}