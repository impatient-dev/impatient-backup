package imp.backup.net

import imp.backup.backScope
import imp.util.*
import imp.web.ImpWs
import imp.web.ImpWsReceiver
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import java.nio.ByteBuffer

private val log = AppWebsockets::class.logger


/**Handles websocket events (open/close/message), and keeps track of all open websockets.*/
object AppWebsockets : ImpWsReceiver {
	private val mutex = Mutex()
	private val _listeners = BasicCoListeners<Listener>()
	val listeners: CoListeners<Listener> get() = _listeners
	private val open = ArrayList<PeerConnI>()

	/**Returns "client" or "server".*/
	private fun cs(channel: ImpWs): String = if(channel.isClient) "client" else "server"

	override fun onOpen(ws: ImpWs) {
		log.debug("{} websocket opened to {}.", cs(ws), ws.remoteAddr.hostString)
		backScope.launchMutex(mutex) {
			open.add(PeerConnI(ws))
			_listeners.fireAsync { it.onWsOpen(ws) }
		}
	}

	override fun onBinary(message: ByteBuffer, ws: ImpWs) {
		(if(ws.isClient) wsClientHandler else wsServerHandler).handleBinary(message, ws)
	}

	override fun onText(message: String, ws: ImpWs) {
		log.error("Received text ws msg from {} {}: {}.", cs(ws), ws.remoteAddr.hostString, message.abbreviateEllipsis(100))
		ws.close()
	}

	override fun onError(e: Throwable, ws: ImpWs) {
		log.error("Fatal error with ${cs(ws)} websocket to ${ws.remoteAddr.hostString}.", e)
		ws.close()
		_listeners.fireAsync { it.onWsFatal(ws, e) }
	}

	override fun onClose(ws: ImpWs) {
		log.info("{} websocket closed from {}.", cs(ws), ws.remoteAddr.hostString)
		backScope.launch {
			mutex.withLock { open.removeIf { it.channel === ws } }
			_listeners.fireAsync { it.onWsClose(ws) }
		}
	}


	interface Listener {
		fun onWsOpen(ch: ImpWs)
		fun onWsClose(ch: ImpWs)
		/**Called when a fatal error causes a websocket connection to close, or to fail to open in the first place.
		 * If the websocket is open, onWsClose() will also be called, either before or after.*/
		fun onWsFatal(ch: ImpWs, e: Throwable)
	}
}