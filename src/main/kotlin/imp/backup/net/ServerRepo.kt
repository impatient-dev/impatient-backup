package imp.backup.net

import androidx.compose.runtime.Immutable
import com.fasterxml.jackson.module.kotlin.readValue
import imp.backup.AppPaths
import imp.backup.jsonMapper
import imp.json.impWriteOrDelete
import imp.util.BasicCoListeners
import imp.util.CoListeners
import imp.util.logger
import imp.web.base64ToX509Certificate
import imp.web.encodedBase64Str
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import java.security.cert.X509Certificate
import java.util.*
import kotlin.io.path.exists

private val log = ServerRepo::class.logger

@Immutable data class SavedServer (
	val uuid: UUID,
	val host: String,
	/**Null if we want to connect to the default port.*/
	val port: Int?,
	/**Stuff to add after the host, either null or starting with a slash.*/
	val path: String?,
	val name: String,
	/**The certificate we expect the server to use.*/
	val cert: X509Certificate?,
) {
	fun toJson() = ServerJ(uuid = uuid, host = host, port = port, path = path, name = name, cert?.encodedBase64Str())
}

/**A representation of a saved server, suitable for storing in JSON.*/
data class ServerJ (
	val uuid: UUID,
	val host: String,
	val port: Int?,
	val path: String?,
	val name: String,
	val certBase64: String?,
) {
	fun parsed() = SavedServer(uuid = uuid, host = host, port = port, path = path, name = name, certBase64?.base64ToX509Certificate())
}


object ServerRepo {
	private val mutex = Mutex()
	private val file = AppPaths.savedServers
	private var list: List<SavedServer>? = null

	private fun lockedList(forceRefresh: Boolean = false): List<SavedServer> {
		if(forceRefresh || list == null) {
			if(file.exists())
				list = jsonMapper.readValue<Set<ServerJ>>(file.toFile()).map { it.parsed() }
			else
				list = emptyList()
		}
		return list!!
	}

	private fun lockedSet(new: List<SavedServer>) {
		log.debug("Saving {} servers.", new.size)
		jsonMapper.impWriteOrDelete(file, new.map { it.toJson()} )
		list = new
	}

	suspend fun list(forceRefresh: Boolean = false): List<SavedServer> = mutex.withLock { lockedList(forceRefresh) }

	suspend fun add(server: SavedServer) = mutex.withLock {
		lockedSet(lockedList().plus(server))
		_listeners.fireAsync { it.onAdd(server) }
	}

	suspend fun remove(server: SavedServer) = mutex.withLock {
		lockedSet(lockedList().minus(server))
		_listeners.fireAsync { it.onRemove(server) }
	}


	private val _listeners = BasicCoListeners<Listener>()
	val listeners: CoListeners<Listener> get() = _listeners
	interface Listener {
		suspend fun onAdd(server: SavedServer)
		suspend fun onRemove(server: SavedServer)
	}
}