package imp.backup.net

import imp.backup.backScope
import imp.util.impRmFirst
import imp.util.launchDefault
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import java.lang.System.currentTimeMillis

/**A basic exchange, where incoming messages can meet up with the method calls that are waiting for them.
 * This is implemented by putting all messages into a list and waking up the waiting coroutines, who will iterate through the list to find what they're looking for.
 * The performance may be poor if large numbers of incoming messages are received.*/
class BlockingListExchange <T> (
	/**Received messages are dropped after this long.*/
	private val cleanupMs: Long,
) {
	private val sync = Object()
	private val entries = ArrayList<MyEntry<T>>()
	private var cleanup: Job? = null

	/**Waits until we receive a message matching the predicate, then returns that message.
	 * This method blocks instead of suspending, because I don't have a good coroutine-based equivalent of wait+notify.
	 * So it should only be called on the IO dispatcher, not the default one.
	 * This method can be called before or after the expected message is received,
	 * as long as it's not so late that the received message has been discarded by the cleanup coroutine.*/
	fun waitFor(timeoutMs: Long, predicate: (T) -> Boolean): T {
		val cutoff = currentTimeMillis() + timeoutMs
		synchronized(sync) {
			while(true) {
				val out = entries.impRmFirst { predicate(it.msg) }
				if(out != null)
					return out.msg
				if(currentTimeMillis() > cutoff)
					throw Exception("Timed out waiting for message.")
				sync.wait(timeoutMs)
			}
		}
	}

	/**Adds a received message, and wakes up any coroutines that might be waiting for it.*/
	fun put(msg: T) = synchronized(sync) {
		entries.add(MyEntry(msg, currentTimeMillis()))
		if(cleanup == null)
			cleanup = backScope.launchDefault { periodicCleanup() }
		sync.notifyAll()
	}

	private suspend fun periodicCleanup() {
		try {
			while(true) {
				delay(cleanupMs)
				synchronized(sync) {
					val cutoff = currentTimeMillis() - cleanupMs
					entries.removeIf { it.ms < cutoff }
					if(entries.isEmpty())
						return
				}
			}
		} finally {
			cleanup = null
		}
	}



	private class MyEntry<T> (
		val msg: T,
		val ms: Long,
	)
}