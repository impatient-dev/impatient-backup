package imp.backup.net

import androidx.compose.runtime.Immutable
import com.fasterxml.jackson.module.kotlin.readValue
import imp.backup.AppPaths
import imp.backup.jsonMapper
import imp.json.impWriteOrDelete
import imp.util.SusRepo1
import imp.util.logger
import imp.web.base64ToX509Certificate
import imp.web.encodedBase64Str
import java.security.cert.X509Certificate
import java.util.*
import kotlin.io.path.exists

private val log = AdminRepo::class.logger

/**A certificate that is allowed to do anything it wants to.*/
@Immutable data class SavedAdmin(
	val uuid: UUID,
	val name: String,
	val cert: X509Certificate,
)

private fun SavedAdmin.toJson() = AdminJ(uuid, name, cert.encodedBase64Str())

@Immutable private class AdminJ (
	val uuid: UUID,
	val name: String,
	val certBase64: String,
) {
	fun parsed() = SavedAdmin(uuid, name, certBase64.base64ToX509Certificate())
}


/**Keeps track of which certificates are admins.*/
object AdminRepo {
	private val file = AppPaths.savedAdmins

	suspend fun list(): List<SavedAdmin> = loader.get()
	suspend fun isAdmin(cert: X509Certificate): Boolean = loader.get().any { it.cert == cert }
	suspend fun add(cert: X509Certificate, name: String) = loader.update { prev ->
		if(prev.any { it.cert == cert })
			prev
		else
			prev.plus(SavedAdmin(UUID.randomUUID(), name, cert))
	}


	private val loader = object : SusRepo1<List<SavedAdmin>>() {
		override suspend fun load(): List<SavedAdmin> {
			if(file.exists()) {
				try {
					return jsonMapper.readValue<List<AdminJ>>(file.toFile()).map { it.parsed() }
				} catch(e: Exception) {
					log.error("Failed to load admins.", e)
				}
			}
			return emptyList()
		}

		override suspend fun save(item: List<SavedAdmin>) {
			log.debug("Saving {} admins.", item.size)
			jsonMapper.impWriteOrDelete(file, item.map { it.toJson() })
		}
	}
}