package imp.backup.net

import androidx.compose.runtime.Immutable
import imp.web.ImpWs
import java.net.InetAddress


/**A connection to another computer running this app.*/
@Immutable data class PeerConnI (
	val channel: ImpWs,
	val hostStr: String = channel.remoteAddr.hostString,
	val open: Boolean = channel.isOpen,
)

@Immutable data class RunningServerInfo (
	val port: Int,
	/**Addresses of this computer, displayed to the user so he knows what to type into another computer.*/
	val addrs: List<InetAddress>,
)