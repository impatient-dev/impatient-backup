package imp.backup.net

import imp.util.fmt.Millis
import imp.util.logger
import imp.util.withIo
import imp.web.ImpWs
import imp.web.idmf.IdmfSender
import imp.web.remoteHost
import java.util.*

private val log = RemoteRootDirExchange::class.logger
private val sender = IdmfSender(epGetRootDir)

object RemoteRootDirExchange {
	private val exchange = BlockingListExchange<RootDirResponseJ>(2 * Millis.MINUTE)

	/**Sends the request to get or create the root directory, and waits for the response.
	 * This is blocking and does not require a timeout.*/
	suspend fun request(tgt: SyncTgtJ, ws: ImpWs, createIfMissing: Boolean = false): RootDirResponseJ = withIo {
		val msg = RootDirRequestJ(UUID.randomUUID(), tgt)
		log.trace("Request to {} to {} root dir {}.", ws.remoteHost, if(createIfMissing) "create" else "get", msg.requestId)
		sender.send(msg, ws)
		exchange.waitFor(30 * Millis.SECOND) { it.forRequestId == msg.requestId }
	}

	fun handle(msg: RootDirResponseJ, ws: ImpWs) {
		log.trace("Received root dir response {} from {}.", msg.forRequestId, ws.remoteHost)
		exchange.put(msg)
	}
}