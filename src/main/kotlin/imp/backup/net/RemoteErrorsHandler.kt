package imp.backup.net

import imp.util.logger
import imp.web.ImpWs
import imp.web.remoteHost

private val log = RemoteErrorsHandler::class.logger

/**Notified when websockets notify us about errors.
 * Just logs for now, but this class may eventually notify the UI about errors.*/
object RemoteErrorsHandler {
	suspend fun handle(err: StackJ, ws: ImpWs) {
		log.error("Ws {} notified us about an error:\n{}.", ws.remoteHost, err.stacktrace)
	}
}