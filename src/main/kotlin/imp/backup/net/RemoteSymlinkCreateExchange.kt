package imp.backup.net

import imp.util.fmt.Millis
import imp.util.logger
import imp.util.withIo
import imp.web.ImpWs
import imp.web.idmf.IdmfSender
import imp.web.remoteHost
import java.util.*

private val log = RemoteSymlinkCreateExchange::class.logger
private val sender = IdmfSender(epCreateSymlink)

object RemoteSymlinkCreateExchange {
	private val exchange = BlockingListExchange<CreateSymlinkResponseJ>(2 * Millis.MINUTE)

	/**Sends the request to create a symlink, and waits for the response.
	 * This is blocking and does not require a timeout.*/
	suspend fun request(
		tgt: SyncTgtJ,
		parent: LocEntryIdJ,
		name: ByteArray,
		nameIv: ByteArray?,
		target: ByteArray,
		targetIv: ByteArray?,
		ws: ImpWs,
	): LocationEntryJ = withIo {
		val msg = CreateSymlinkRequestJ(UUID.randomUUID(), tgt, parent, name, nameIv, target, targetIv)
		log.trace("Request to {} to create symlink {}.", ws.remoteHost, msg.requestId)
		sender.send(msg, ws)
		exchange.waitFor(30 * Millis.SECOND) { it.forRequestId == msg.requestId }.entry
	}

	fun handle(msg: CreateSymlinkResponseJ, ws: ImpWs) {
		log.trace("Received create symlink response {} from {}.", msg.forRequestId, ws.remoteHost)
		exchange.put(msg)
	}
}