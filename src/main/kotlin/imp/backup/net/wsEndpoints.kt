package imp.backup.net

import imp.backup.data.BackupType
import imp.backup.data.LocationType
import imp.backup.jsonMapper
import imp.web.idmf.ClientAndOrServer.Companion.BOTH
import imp.web.idmf.IdmfByteArrayMapper
import imp.web.idmf.IdmfEmptyMapper
import imp.web.idmf.IdmfEndpointsBuilder
import imp.web.idmf.IdmfJsonMapper
import java.nio.file.Path
import java.time.Instant
import java.util.*

/**Path where the server listens for websocket connections.*/
const val WS_PATH = "/ws"
/**Port where a server running on a personal computer listens for websocket connections.*/
const val WS_PORT_LOCAL = 8242


private val builder = IdmfEndpointsBuilder()

/**Send when someone receives a message that is invalid in some way.
 * Usually whoever sent this closes the websocket afterward.*/
val epInvalid = builder.binary(BOTH, "Invalid", IdmfJsonMapper(jsonMapper, InvalidJ::class))
/**Sent when something goes wrong on the sender side, when the sender trusts the receiver enough to tell him about it.
 * The connection is closed afterward.*/
val epStacktrace = builder.binary(BOTH, "Stacktrace", IdmfJsonMapper(jsonMapper, StackJ::class))

/**The sender demands the other side send authentication information (X.509 certificate).*/
val epAuthDemand = builder.binary(BOTH, "Auth-Demand", IdmfEmptyMapper)
/**The sender provides authentication information that was previously requested.*/
val epAuthResponse = builder.binary(BOTH, "Auth-Response", IdmfJsonMapper(jsonMapper, AuthJ::class))
/**The sender demands the receiver sign some data with the private key the receiver previously said they have (via an X.509 certificate).*/
val epAuthChallenge = builder.binary(BOTH, "Auth-Challenge", IdmfByteArrayMapper)
/**The sender provides a previously-requested signature in response to an auth challenge.*/
val epAuthSignature = builder.binary(BOTH, "Auth-Signature", IdmfByteArrayMapper)
/**The sender indicates that the receiver has been successfully authenticated and tells it how much is trusted.
 * The receiver may now start trying to do things over this connection.*/
val epAuthSuccess = builder.binary(BOTH, "Auth-Success", IdmfJsonMapper(jsonMapper, TrustLevel::class))

val epGetLocations = builder.binary(BOTH, "Locations-Get", IdmfEmptyMapper)
val epLocations = builder.binary(BOTH, "Locations-Response", IdmfJsonMapper(jsonMapper, AllLocationsJ::class))

/**A request to get information about the root directory in the open location or backup.*/
val epGetRootDir = builder.binary(BOTH, "Root-Dir-Request", IdmfJsonMapper(jsonMapper, RootDirRequestJ::class))
/**A response with information about the root directory in the open location or backup.*/
val epRootDir = builder.binary(BOTH, "Root-Dir-Response", IdmfJsonMapper(jsonMapper, RootDirResponseJ::class))
/**A request to list entries in a directory.*/
val epListDir = builder.binary(BOTH, "Dir-List-Request", IdmfJsonMapper(jsonMapper, ListDirRequestJ::class))
/**A response listing entries in a directory.*/
val epDirList = builder.binary(BOTH, "Dir-List-Response", IdmfJsonMapper(jsonMapper, DirListJ::class))
/**A request to create a directory.*/
val epCreateDir = builder.binary(BOTH, "Dir-Create", IdmfJsonMapper(jsonMapper, CreateDirRequestJ::class))
/**A response telling that a directory was created.*/
val epDirCreated = builder.binary(BOTH, "Dir-Created", IdmfJsonMapper(jsonMapper, CreateDirResponseJ::class))
/**A request to create a symlink.*/
val epCreateSymlink = builder.binary(BOTH, "Symlink-Create", IdmfJsonMapper(jsonMapper, CreateSymlinkRequestJ::class))
/**A response telling that a symlink was created.*/
val epSymlinkCreated = builder.binary(BOTH, "Symlink-Created", IdmfJsonMapper(jsonMapper, CreateSymlinkResponseJ::class))
/**A request to delete a file or directory. If this succeeds, no response message is sent.*/
val epDeleteEntry = builder.binary(BOTH, "Entry-Delete", IdmfJsonMapper(jsonMapper, DeleteEntryRequestJ::class))

val epCreateBackup = builder.binary(BOTH, "Backup-Create", IdmfJsonMapper(jsonMapper, CreateBackupRequestJ::class))
val epBackupCreated = builder.binary(BOTH, "Backup-Created", IdmfJsonMapper(jsonMapper, RequestIdJ::class))
val epDeleteBackup = builder.binary(BOTH, "Backup-Delete", IdmfJsonMapper(jsonMapper, BackupIdJ::class))
val epBackupDeleted = builder.binary(BOTH, "Backup-Deleted", IdmfJsonMapper(jsonMapper, RequestIdJ::class))

/**A request for the receiver to send all bytes of a file, in chunks.
 * Only one read can happen at a time (but one write can happen simultaneously, sending bytes in the other direction).*/
val epFileRead = builder.binary(BOTH, "File-Read", IdmfJsonMapper(jsonMapper, ReadFileRequestJ::class))
/**A request to create a new file, which will be followed by chunks of bytes making up the file content.
 *  * Only one write can happen at a time (but one read can happen simultaneously, sending bytes in the other direction).*/
val epFileCreate = builder.binary(BOTH, "File-Create", IdmfJsonMapper(jsonMapper, CreateFileRequestJ::class))
/**A chunk of bytes being read from or written to a file.*/
val epFileChunk = builder.binary(BOTH, "File-Chunk", FileChunkBody.Mapper)
/**An acknowledgement of how many chunks have been received for the current file. The other side should avoid sending us too many un-acknowledged chunks.*/
val epFileChunkAck = builder.binary(BOTH, "File-Chunk-Ack", IdmfJsonMapper(jsonMapper, ChunkAckJ::class))
/**Indicates that the last chunk has been sent for the file currently being written/read.*/
val epFileDone = builder.binary(BOTH, "File-Chunks-Done", IdmfEmptyMapper)


/**A declaration of all messages that can be sent between clients and servers.*/
val endpoints = builder.build()




/**A request with a unique ID.*/
interface UniqueRequestJ {
	val requestId: UUID
}
/**A response to a request that had a unique ID.*/
interface UniqueResponseJ {
	val forRequestId: UUID
}

/**Information about something we did that the other side of a connection considered invalid.*/
class InvalidJ(
	val msg: String,
	val requestId: UUID?,
)
/**A stacktrace describing some error that the sender encountered.*/
class StackJ (
	val stacktrace: String,
)

class AuthJ(
	/**An X.509 certificate, base-64 encoded.*/
	val certBase64: String,
)

/**How trustworthy some side of a websocket connection is. You can use greater/less-than comparisons with this enum.*/
enum class TrustLevel {
	/**This entity is not confirmed to be trustworthy.*/
	NONE,
	/**This entity is trustworthy enough to receive files.*/
	PASSIVE,
	/**This entity is trustworthy enough to be allowed to do anything it wants to.*/
	ADMIN,
}

class AllLocationsJ (
	val locations: List<LocationJ>,
)

class LocationJ (
	val name: String,
	val type: LocationType,
	val backups: List<BackupJ>,
)

class BackupJ (
	val name: String,
	val type: BackupType,
	val lastStart: Instant?,
	val lastEnd: Instant?,
	val files: Long?,
	val bytes: Long?,
)

/**Uniquely identifies either a user location, or a backup in a backup location.*/
class SyncTgtJ (
	val locationName: String,
	val backupType: BackupType?,
	val backupName: String?,
)

/**Uniquely identifies a file or directory in a sync target (location or backup).
 * The specific file/dir is identified by either the path (for raw backups and user locations) or ID (for database-based backups).*/
data class LocEntryIdJ (
	val path: Path?,
	val id: Long?,
)

data class CreateBackupRequestJ (
	override val requestId: UUID,
	/**Name of the backup location.*/
	val locationName: String,
	val type: BackupType,
	val name: String,
	val fromLocationName: String,
	/**The encryption key ID, if this is an encrypted backup.*/
	val keyId: UUID?,
) : UniqueRequestJ

data class BackupIdJ (
	/**Name of the backup location.*/
	val locationName: String,
	val type: BackupType,
	val name: String,
	val requestId: UUID,
)

data class RequestIdJ (
	val requestId: UUID,
)

class LocationEntryJ (
	val id: Long?,
	val name: ByteArray,
	val nameIv: ByteArray?,
	val contentIv: ByteArray?,
	val path: Path?,
	val bytes: Long?,
	val symlinkTo: ByteArray?,
	val lastModified: Instant?,
)




class RootDirRequestJ (
	override val requestId: UUID,
	val tgt: SyncTgtJ,
) : UniqueRequestJ
class RootDirResponseJ (
	override val forRequestId: UUID,
	val exists: Boolean,
	val id: Long?,
) : UniqueResponseJ

class ListDirRequestJ (
	override val requestId: UUID,
	val tgt: SyncTgtJ,
	val dir: LocEntryIdJ,
) : UniqueRequestJ
class DirListJ (
	override val forRequestId: UUID,
	val entries: List<LocationEntryJ>,
) : UniqueResponseJ

class CreateDirRequestJ (
	override val requestId: UUID,
	val tgt: SyncTgtJ,
	val parent: LocEntryIdJ,
	/**Possibly encrypted.*/
	val name: ByteArray,
	val nameIv: ByteArray?,
) : UniqueRequestJ
class CreateDirResponseJ (
	override val forRequestId: UUID,
	val entry: LocationEntryJ,
) : UniqueResponseJ

class CreateSymlinkRequestJ (
	override val requestId: UUID,
	val tgt: SyncTgtJ,
	val parent: LocEntryIdJ,
	/**Possibly encrypted.*/
	val name: ByteArray,
	val nameIv: ByteArray?,
	/**Possibly encrypted.*/
	val target: ByteArray,
	val targetIv: ByteArray?,
) : UniqueRequestJ
class CreateSymlinkResponseJ (
	override val forRequestId: UUID,
	val entry: LocationEntryJ,
) : UniqueResponseJ

class DeleteEntryRequestJ (
	override val requestId: UUID,
	val tgt: SyncTgtJ,
	val entry: LocEntryIdJ,
) : UniqueRequestJ
class DeleteEntryResponseJ (
	override val forRequestId: UUID,
) : UniqueResponseJ

class ReadFileRequestJ (
	override val requestId: UUID,
	val tgt: SyncTgtJ,
	val file: LocEntryIdJ,
	val chunkSize: Int,
	/**How many chunks should be sent beyond the last one that was acknowledged.*/
	val simultaneousChunks: Int,
) : UniqueRequestJ

class CreateFileRequestJ (
	override val requestId: UUID,
	val parent: LocEntryIdJ,
	val name: ByteArray,
	val nameIV: ByteArray?,
	val contentIV: ByteArray?,
) : UniqueRequestJ

class ChunkAckJ (
	/**The last index that was received and processed.*/
	val idx: Int,
)