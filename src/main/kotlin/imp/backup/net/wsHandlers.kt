package imp.backup.net

import imp.backup.backScope
import imp.util.abbreviateEllipsis
import imp.util.logger
import imp.util.stacktraceNoCode
import imp.web.ImpWs
import imp.web.idmf.IdmfBinEndpoint
import imp.web.idmf.IdmfBinMsg
import imp.web.idmf.IdmfRegistryBuilder
import imp.web.idmf.IdmfSender
import imp.web.remoteHost
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.util.*

private val log = logger("imp.backup.network.handlers")

/**Handles IDMF messages received on client-side websockets.*/
val wsClientHandler = endpoints.buildClientHandlers()
	.also(::addCommonHandlers)
	.build(::handleUnexpectedMessageTitle)

/**Handles IDMF messages received on server-side websockets.*/
val wsServerHandler = endpoints.buildClientHandlers()
	.also(::addCommonHandlers)
	.build(::handleUnexpectedMessageTitle)

/**Adds handlers that are common to both client and server websockets.
 * Permission checking is handled here.
 * Implementation note: we skip auth checks for messages that are supposed to be responses, because if they send us an unwanted response,
 * the relevant handler should notice this and close the connection.*/
private fun addCommonHandlers(builder: IdmfRegistryBuilder) {
	builder
		.handle(epInvalid, ::handleInvalid)
		.handleBack(epStacktrace) { req, ws -> RemoteErrorsHandler.handle(req, ws) }

		.handleBack(epAuthDemand) { _, ws -> backScope.launch { WsAuthRepo.handleDemand(ws) } }
		.handleBack(epAuthResponse) { msg, ws -> backScope.launch { WsAuthRepo.handleResponse(msg, ws) } }
		.handleBack(epAuthChallenge, WsAuthRepo::handleChallenge)
		.handleBack(epAuthSignature, WsAuthRepo::handleSignature)
		.handleBack(epAuthSuccess, WsAuthRepo::handleSuccess)

		.handleAdmin(epGetLocations, "get locations") { _, ws -> BasicHandlers.handleGetLocations(ws) }
		.handleBack(epLocations, RemoteLocationsRepo::handle)

		.handleAdmin(epGetRootDir, "get root dir") { _, _ -> TODO() }
		.handleBack(epRootDir) { msg, ws -> TODO() }
		.handleAdmin(epListDir, "list dir", BasicHandlers::handleListDir)
		.handleBack(epDirList) { msg, ws -> RemoteDirListExchange.handle(msg, ws) }
		.handleAdmin(epCreateDir, "create dir", BasicHandlers::handleCreateDir)
		.handleBack(epDirCreated, RemoteDirCreateExchange::handle)
		.handleAdmin(epDeleteEntry, "delete entry", BasicHandlers::handleDeleteEntry)

		.handleAdmin(epCreateBackup, "create backup", BasicHandlers::handleCreateBackup)
		.handleBack(epBackupCreated) { _, _ -> TODO() }
		.handleAdmin(epDeleteBackup, "delete backup", BasicHandlers::handleDeleteBackup)
		.handleBack(epBackupDeleted) { _, _ -> TODO() }

		.handleAdmin(epFileRead, "read file", RemoteFileTransferExchange::handleReadFile)
		.handleAdmin(epFileCreate, "create file", RemoteFileTransferExchange::handleCreateFile)
		.handleBack(epFileChunk, RemoteFileTransferExchange::handleChunk)
		.handleBack(epFileChunkAck, RemoteFileTransferExchange::handleAck)
		.handleBack(epFileDone, RemoteFileTransferExchange::handleDone)
}


/**Handler that launches a background coroutine per request. This function does no authentication, so stacktraces are not reported to the websocket.*/
private fun <T> IdmfRegistryBuilder.handleBack (
	endpoint: IdmfBinEndpoint<T>,
	block: suspend (T, ImpWs) -> Unit,
): IdmfRegistryBuilder = handle(endpoint) { msg, ws ->
	backScope.launch {
		block(msg, ws)
	}
}

/**Ensures this websocket is an admin before running the block. Because we then trust this websocket, any thrown error will be reported to it.*/
private fun <T> IdmfRegistryBuilder.handleAdmin (
	endpoint: IdmfBinEndpoint<T>,
	action: String,
	block: suspend (T, ImpWs) -> Unit,
): IdmfRegistryBuilder = handle(endpoint) { msg, ws ->
	backScope.launch {
		WsAuthRepo.admin(ws, action) {
			try {
				block(msg, ws)
			} catch(e: Exception) {
				log.error("Error with $action for admin ${ws.remoteHost}.", e)
				sendStack.send(StackJ(stacktraceNoCode(e)), ws)
			}
		}
	}
}


val sendInvalid = IdmfSender(epInvalid)
val sendStack = IdmfSender(epStacktrace)

/**Tells the remote machine it did something unacceptable (and logs this), then closes the websocket.*/
fun wsReportNaughty(ws: ImpWs, msg: String, requestId: UUID? = null) {
	log.error("Ws {} misbehaving: {}", ws.remoteHost, msg)
	sendInvalid.send(InvalidJ(msg, requestId), ws)
	ws.close()
}

private fun handleUnexpectedMessageTitle(msg: IdmfBinMsg, channel: ImpWs) {
	log.error("Received msg from {} with unexpected title: {}.", channel.remoteHost, msg.title.abbreviateEllipsis(200))
	sendInvalid.send(InvalidJ("Invalid message title: ${msg.title}", null), channel)
	channel.close()
}

private fun handleInvalid(msg: InvalidJ, channel: ImpWs) {
	log.error("We did something invalid to {} in request {}: {}.", channel.remoteHost, msg.requestId, msg.msg)
	channel.close()
}


/**Runs the block in a coroutine. If the block throws, this function will send a message over the websocket mentioning the error.
 * This function will not close the websocket on failure.*/
fun launchReportingTo(ws: ImpWs, requestId: UUID? = null, scope: CoroutineScope = backScope, block: suspend CoroutineScope.() -> Unit) {
	scope.launch {
		try {
			block(this)
		} catch(e: Exception) {
			log.error("Error doing work ($requestId) for ${ws.remoteHost}.", e)
			sendStack.send(StackJ(stacktraceNoCode(e)), ws)
		}
	}
}
