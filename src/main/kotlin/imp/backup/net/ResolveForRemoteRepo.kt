package imp.backup.net

import imp.backup.backScope
import imp.backup.data.BackupImpl
import imp.backup.data.UserLocation
import imp.backup.files.SyncTarget
import imp.backup.repo.LocationRepo
import imp.util.fmt.Millis
import imp.util.launchDefault
import imp.util.logger
import imp.util.unexpected
import imp.web.ImpWs
import imp.web.remoteHost
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import java.lang.System.currentTimeMillis
import java.util.*

private val log = ResolveForRemoteRepo::class.logger
private const val TIMEOUT_MS = 1 * Millis.MINUTE


/**An object that finds sync targets (locations / backups) for remote websockets. Not responsible for auth.*/
object ResolveForRemoteRepo {
	private val mutex = Mutex()
	private val cache = HashMap<ImpWs, CacheEntry>()
	private var cleaner: Job? = null


	private fun putInCacheWithLock(tgt: SyncTarget, ws: ImpWs) {
		cache[ws] = CacheEntry(tgt)
		if(cleaner == null)
			cleaner = backScope.launchDefault { cleanupPeriodically() }
	}

	/**Finds the location or backup. If it doesn't exist, closes the websocket with the "naughty" report function and returns null.*/
	suspend fun orClose(tgt: SyncTgtJ, ws: ImpWs, forRequestId: UUID?): SyncTarget? = mutex.withLock {
		check((tgt.backupName == null) == (tgt.backupType == null))
		val cached = cache[ws]
		if(cached != null && cached.matches(tgt)) {
			cached.lastAccessMs = currentTimeMillis()
			return cached.tgt
		}

		log.debug("Opening for {}: {}.", ws.remoteHost, tgt)
		val new: SyncTarget? = if(tgt.backupName == null) {
			LocationRepo.getUserIf1(tgt.locationName)
		} else {
			val location = LocationRepo.getBackupIf1(tgt.backupName)
			val backup = location?.backup(tgt.backupType!!, tgt.backupName!!)
			if(backup?.exists() == false) {
				wsReportNaughty(ws, "Backup doesn't exist: $tgt", forRequestId)
				return null
			}
			backup?.initExisting()
		}
		if(new == null)
			wsReportNaughty(ws, "Sync target not found (or not unique): $tgt", forRequestId)
		else
			putInCacheWithLock(new, ws)
		return new
	}


	private suspend fun cleanupPeriodically() {
		try {
			while(true) {
				delay(TIMEOUT_MS)
				mutex.withLock {
					val cutoff = currentTimeMillis() - TIMEOUT_MS
					val it = cache.iterator()
					while(it.hasNext()) {
						val entry = it.next().value
						if(entry.lastAccessMs < cutoff)
							it.remove()
					}
					if(cache.isEmpty()) {
						cleaner = null
						return
					}
				}
			}
		} finally {
			cleaner = null
		}
	}
}




private class CacheEntry (
	val tgt: SyncTarget,
	var lastAccessMs: Long = currentTimeMillis(),
) {
	fun matches(json: SyncTgtJ): Boolean {
		if(tgt is UserLocation)
			return tgt.name == json.locationName && json.backupType == null && json.backupName == null
		if(tgt is BackupImpl)
			return tgt.locationName == json.locationName && tgt.decl.type == json.backupType && tgt.decl.name == json.backupName
		unexpected(tgt)
	}
}