package imp.backup.net

import imp.util.fmt.Millis
import imp.util.logger
import imp.util.withIo
import imp.web.ImpWs
import imp.web.idmf.IdmfSender
import imp.web.remoteHost
import java.util.*

private val log = RemoteDirCreateExchange::class.logger
private val sender = IdmfSender(epCreateDir)

object RemoteDirCreateExchange {
	private val exchange = BlockingListExchange<CreateDirResponseJ>(2 * Millis.MINUTE)

	/**Sends the request to create a directory, and waits for the response.
	 * This is blocking and does not require a timeout.*/
	suspend fun request(tgt: SyncTgtJ, parent: LocEntryIdJ, name: ByteArray, nameIv: ByteArray?, ws: ImpWs): LocationEntryJ = withIo {
		val msg = CreateDirRequestJ(UUID.randomUUID(), tgt, parent, name, nameIv)
		log.trace("Request to {} to create dir {}.", ws.remoteHost, msg.requestId)
		sender.send(msg, ws)
		exchange.waitFor(30 * Millis.SECOND) { it.forRequestId == msg.requestId }.entry
	}

	fun handle(msg: CreateDirResponseJ, ws: ImpWs) {
		log.trace("Received create dir response {} from {}.", msg.forRequestId, ws.remoteHost)
		exchange.put(msg)
	}
}