package imp.backup.net

import imp.backup.crypto.SymKey
import imp.backup.data.*
import imp.backup.files.RawLocEntry
import imp.backup.repo.BackupI
import imp.backup.repo.BackupRepo
import imp.backup.repo.LocationRepo
import imp.util.EMPTY_BYTE_ARRAY
import imp.util.logger
import imp.web.ImpWs
import imp.web.idmf.IdmfSender
import imp.web.remoteHost
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import java.util.*
import kotlin.io.path.name

private val log = BasicHandlers::class.logger

/**Functions to handle remote requests. These functions are not responsible for auth and assume it has already happened.
 * All of these functions may close the websocket with the "naughty" report function if the request is bad.
 * This object is currently single-threaded, because processing multiple incoming requests for the same location may not be safe.*/
object BasicHandlers {
	private val mutex = Mutex()

	suspend fun handleGetLocations(ws: ImpWs) = mutex.withLock  {
		log.debug("Getting locations for {}.", ws.remoteHost)
		val locations = LocationRepo.refresh()
		val out = ArrayList<LocationJ>(locations.size)
		for(loc in locations)
			loc.toJson()?.let(out::add)
		log.trace("Sending {} locations to {}.", out.size, ws.remoteHost)
		sendLocations.send(AllLocationsJ(out), ws)
	}

	suspend fun handleCreateBackup(msg: CreateBackupRequestJ, ws: ImpWs) = mutex.withLock {
		log.debug("Creating backup for {}: {}.", ws.remoteHost, msg)
		val location = backupLocationOrClose(msg.locationName, msg.requestId, ws) ?: return
		val key = msg.keyId?.let { SymKey(it, null, null) }
		location.backup(msg.type, msg.name).initNew(msg.fromLocationName, key)
		sendBackupCreated.send(RequestIdJ(msg.requestId), ws)
	}

	suspend fun handleDeleteBackup(msg: BackupIdJ, ws: ImpWs) = mutex.withLock {
		log.debug("Deleting backup for {}: {}.", ws.remoteHost, msg)
		val location = backupLocationOrClose(msg.locationName, msg.requestId, ws) ?: return
		location.backup(msg.type, msg.name).delete()
		sendBackupDeleted.send(RequestIdJ(msg.requestId), ws)
	}

	suspend fun handleListDir(msg: ListDirRequestJ, ws: ImpWs) = mutex.withLock {
		log.debug("Listing dir {} for {}: {}.", msg.dir.id ?: msg.dir.path!!, ws.remoteHost, msg.requestId)
		val tgt = ResolveForRemoteRepo.orClose(msg.tgt, ws, msg.requestId) ?: return
		if(tgt is DbBackupImpl)
			check(msg.dir.id != null)
		else
			check(msg.dir.path != null)
		// hopefully they don't need any of the properties we aren't setting
		val dir = RawLocEntry(msg.dir.path?.name?.toByteArray() ?: EMPTY_BYTE_ARRAY, null, null, msg.dir.path, msg.dir.id, null, null, null, null)
		val entries = tgt.raw.listDir(dir)
		log.trace("Found {} entries for {} {}.", entries.size, ws.remoteHost, msg.requestId)
		sendDirList.send(DirListJ(msg.requestId, entries.map { it.toJson() }), ws)
	}

	suspend fun handleCreateDir(msg: CreateDirRequestJ, ws: ImpWs) = mutex.withLock {
		log.debug("Creating dir for {}: {}.", ws.remoteHost, msg)
		val tgt = ResolveForRemoteRepo.orClose(msg.tgt, ws, msg.requestId) ?: return
		val parent = RawLocEntry(msg.parent.path?.name?.toByteArray() ?: EMPTY_BYTE_ARRAY, null, null, msg.parent.path, msg.parent.id, null, null, null, null)
		val created = tgt.raw.createDir(parent, msg.name, msg.nameIv, null)
		val response = CreateDirResponseJ(msg.requestId, created.toJson())
		sendDirCreated.send(response, ws)
	}

	suspend fun handleDeleteEntry(msg: DeleteEntryRequestJ, ws: ImpWs) = mutex.withLock {
		log.debug("Deleting entry for {}: {}.", ws.remoteHost, msg)
		val tgt = ResolveForRemoteRepo.orClose(msg.tgt, ws, msg.requestId) ?: return
		tgt.raw.delete(msg.entry.path, msg.entry.id)
	}
}

private val sendLocations = IdmfSender(epLocations)
private val sendDirList = IdmfSender(epDirList)
private val sendDirCreated = IdmfSender(epDirCreated)
private val sendBackupCreated = IdmfSender(epBackupCreated)
private val sendBackupDeleted = IdmfSender(epBackupDeleted)


/**If the backup location doesn't exist or isn't unique, closes the websocket with the "naughty" report function.*/
private suspend fun backupLocationOrClose(name: String, requestId: UUID, ws: ImpWs): BackupLocation? {
	val out = LocationRepo.getBackupIf1(name)
	if(out == null)
		sendInvalid.send(InvalidJ("Backup location not found (or not unique): $name", requestId), ws)
	return out
}


private fun RawLocEntry.toJson() = LocationEntryJ(
	id = id,
	name = name,
	nameIv = nameIv,
	contentIv = contentIv,
	path = path,
	bytes = bytes,
	symlinkTo = null,
	lastModified = lastModified,
)

/**Returns null for a location type that isn't to be shared.*/
private suspend fun Location.toJson(): LocationJ? {
	if(this is UserLocation)
		return LocationJ(name, LocationType.USER, emptyList())
	if(this is BackupLocation) {
		val backups = BackupRepo.getAllTo(this).map { it.toJson() }
		return LocationJ(name, LocationType.BACKUP, backups)
	}
	return null
}

private fun BackupI.toJson() = BackupJ(name, type, lastStart, lastEnd, files, bytes)