package imp.backup.net

import imp.util.fmt.Millis
import imp.util.logger
import imp.util.withIo
import imp.web.ImpWs
import imp.web.idmf.IdmfSender
import imp.web.remoteHost
import java.util.*

private val log = RemoteDirListExchange::class.logger
private val sender = IdmfSender(epListDir)

/**Handles requests to list entries in remote directories.*/
object RemoteDirListExchange {
	private val exchange = BlockingListExchange<DirListJ>(Millis.MINUTE)

	/**Sends the request to list files in the provided directory, and waits for the response.
	 * This is blocking and does not require a timeout.
	 * You need to ensure you've opened a remote location or backup before calling this.*/
	suspend fun request(tgt: SyncTgtJ, dir: LocEntryIdJ, ws: ImpWs): List<LocationEntryJ> = withIo {
		val msg = ListDirRequestJ(UUID.randomUUID(), tgt, dir)
		log.trace("Requesting dir {} from {}.", msg, ws.remoteHost)
		sender.send(msg, ws)
		exchange.waitFor(20 * Millis.SECOND) { it.forRequestId == msg.requestId }.entries
	}

	fun handle(msg: DirListJ, ws: ImpWs) {
		log.trace("Received list dir response {} from {}.", msg.forRequestId, ws.remoteHost)
		exchange.put(msg)
	}
}