package imp.backup.net

import imp.web.idmf.IdmfBinMapper
import imp.web.idmf.IdmfBinMsg
import java.nio.ByteBuffer
import java.util.*


/**A chunk of bytes from a file, plus a UUID saying what request this is for.*/
class FileChunkBody (
	/**The original request to read or write a file.*/
	val forRequestId: UUID,
	/**A message index, which must increase by 1 for each chunk sent for this specific request ID.*/
	val msgIdx: Int,
	val bytes: ByteBuffer,
) {


	object Mapper : IdmfBinMapper<FileChunkBody> {
		override fun read(message: IdmfBinMsg): FileChunkBody {
			val bytes = message.body
			val uuid = UUID(bytes.long, bytes.long)
			val idx = bytes.int
			return FileChunkBody(uuid, idx, bytes)
		}

		override fun write(title: String, body: FileChunkBody): IdmfBinMsg {
			val bytes = ByteBuffer.allocate(16 + 1 + body.bytes.remaining())
			bytes.putLong(body.forRequestId.mostSignificantBits)
			bytes.putLong(body.forRequestId.leastSignificantBits)
			bytes.putInt(body.msgIdx)
			bytes.put(body.bytes)
			return IdmfBinMsg(title, bytes)
		}
	}
}