package imp.backup.net

import imp.util.fmt.Millis
import imp.util.logger
import imp.util.withIo
import imp.web.ImpWs
import imp.web.idmf.IdmfSender
import imp.web.remoteHost
import java.util.*

private val log = RemoteDeleteExchange::class.logger
private val sender = IdmfSender(epDeleteEntry)

object RemoteDeleteExchange {
	private val exchange = BlockingListExchange<DeleteEntryResponseJ>(2 * Millis.MINUTE)

	/**Sends the request to delete an entry, and waits for the response.
	 * This is blocking and does not require a timeout.*/
	suspend fun request(tgt: SyncTgtJ, id: LocEntryIdJ, ws: ImpWs): DeleteEntryResponseJ = withIo {
		val msg = DeleteEntryRequestJ(UUID.randomUUID(), tgt, id)
		log.trace("Request to {} to delete entry {}.", ws.remoteHost, msg.requestId)
		sender.send(msg, ws)
		exchange.waitFor(30 * Millis.SECOND) { it.forRequestId == msg.requestId }
	}

	fun handle(msg: DeleteEntryResponseJ, ws: ImpWs) {
		log.trace("Received create dir response {} from {}.", msg.forRequestId, ws.remoteHost)
		exchange.put(msg)
	}
}