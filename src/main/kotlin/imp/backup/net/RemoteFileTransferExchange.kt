package imp.backup.net

import imp.web.ImpWs

object RemoteFileTransferExchange {
	suspend fun handleReadFile(msg: ReadFileRequestJ, ws: ImpWs) {
		TODO()
	}

	suspend fun handleCreateFile(msg: CreateFileRequestJ, ws: ImpWs) {
		TODO()
	}

	suspend fun handleChunk(msg: FileChunkBody, ws: ImpWs) {
		TODO()
	}

	suspend fun handleAck(msg: ChunkAckJ, ws: ImpWs) {
		TODO()
	}

	suspend fun handleDone(msg: Unit, ws: ImpWs) {
		TODO()
	}
}