package imp.backup.util

import java.io.OutputStream

/**Calls an abstract listener function every time bytes are written.*/
abstract class AbstractCountingOutputStream (
	private val wrapped: OutputStream,
) : OutputStream() {

	override fun write(b: Int) {
		wrapped.write(b)
		onWriteCount(1)
	}

	override fun write(b: ByteArray, off: Int, len: Int) {
		wrapped.write(b, off, len)
		onWriteCount(len)
	}

	/**Called with the number of bytes involved in the write that just happened.*/
	protected abstract fun onWriteCount(count: Int)
}


/**Calls a callback every time bytes are written.*/
class CallbackCountingOutputStream(
	val callback: (bytes: Int) -> Unit,
	wrapped: OutputStream,
) : AbstractCountingOutputStream(wrapped) {
	override fun onWriteCount(count: Int) {
		callback(count)
	}
}