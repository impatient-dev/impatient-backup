package imp.backup.util


inline fun Any?.orQuestionMarks(): String = this?.toString() ?: "???"