package imp.backup.util

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.io.path.isSymbolicLink

/* Note on symbolic links: Path.isDirectory / Path.isRegularFile / etc. can still return true for symbolic links,
so you need to do a separate check for each entry.*/


fun Path.encodeSymlink(): ByteArray = this.toString().encodeToByteArray()
fun ByteArray.decodeSymlink(): Path = Paths.get(this.decodeToString())

fun Path.symlinkTgt(): Path? = if(this.isSymbolicLink()) Files.readSymbolicLink(this) else null