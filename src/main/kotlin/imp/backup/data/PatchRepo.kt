package imp.backup.data

import imp.sqlite.Database
import imp.sqlite.withTransaction
import imp.util.forEachIn
import imp.util.logger
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths


private val log = PatchRepo::class.logger


private fun isAcceptablePatchName(name: String) = !name.contains('.')



/**Loads information about patches, and makes changes.*/
object PatchRepo {
	fun loadBasic(location: PatchLocation): ArrayList<BasicPatchInfo> {
		log.debug("Loading basic information about {}.", location.name)
		location.db().use {db ->
			val out = ArrayList<BasicPatchInfo>()
			val appliedNames = HashSet<String>()
			patchOrm.query(Patch::class).selectEach(db) {patch ->
				out.add(BasicPatchInfo(patch.name, patch.applied))
				appliedNames.add(patch.name)
			}

			Files.list(location.stagingDir).use {stream ->
				stream.forEach {path ->
					val name = path.fileName.toString()
					if(isAcceptablePatchName(name) && !appliedNames.contains(name) && Files.isDirectory(path))
						out.add(BasicPatchInfo(name, false))
				}
			}

			return out
		}
	}


	/**Selects the patch record with this name, or else inserts a new one that is not yet applied.*/
	fun getOrCreatePatch(name: String, db: Database): Patch {
		var out = patchOrm.query(patchTable).andWhere("name = ?").param(name).selectOnly(db)
		if(out != null)
			return out
		out = Patch(-1, name, false)
		patchTable.insert(out, db)
		return out
	}

	/**Selects the file record with this path and patch, or else inserts a new one with the specified value of applied.*/
	fun getOrCreateFile(path: Path, patchId: Long?, applied: Boolean, db: Database): PatchFile {
		var out = patchOrm.query(patchFileTable).andWhere("path = ? AND patchId = ?").param(path.toString()).param(patchId).selectOnly(db)
		if(out != null)
			return out
		out = PatchFile(-1, path.toString(), patchId, applied)
		patchFileTable.insert(out, db)
		return out
	}


	/**Finds files to apply, and conflicts that get in the way of applying files.
	 * Designed to allow code sharing between planning code and code that actually changes files.
	 * This function does not make any database changes or alter any files - that's your job.
	 * @param db the database containing patches and files in this patch location
	 * @param doApply applies a file (the path is relative to the location).
	 * This will not be called if there is a conflict - the destination file is guaranteed not to exist.
	 * @param tryResolveConflict called if another patch has an applied file with the same path as one in the patch we're applying.
	 * This function tries to move or delete the conflicting file (and update the database).
	 * If it returns false, the file was left alone, so we'll skip doApply for this file.
	 * If the patch is null, this is a preexisting file from before any patches were applied -
	 * it still needs to be dealt with (and false should not be returned in this case).*/
	fun forEachToApply(patch: Patch, location: PatchLocation, db: Database, doApply: (Path) -> Unit, tryResolveConflict: (Path, Patch?) -> Boolean) {
		applyHelper(Paths.get(""), location.path, location.stagingDir.resolve(patch.name), patch, db, doApply, tryResolveConflict)
	}

	/**@param patchDir the directory where files in this patch come from*/
	private fun applyHelper(relative: Path, locationDir: Path, patchDir: Path, patch: Patch, db: Database, doApply: (Path) -> Unit, resolveConflict: (Path, Patch?) -> Boolean) {
		val from = patchDir.resolve(relative)
		if(Files.isDirectory(from)) {
			from.forEachIn { path ->
				applyHelper(patchDir.relativize(path), locationDir, patchDir, patch, db, doApply, resolveConflict)
			}
			return
		}

		val patchesById = patchOrm.query(patchTable).selectAll(db).associateBy {it.id}
		val conflict = patchOrm.query(patchFileTable).andWhere("path = ? AND applied = 1 AND patchId != ?").param(relative).param(patch.id).selectOnly(db)
		if(conflict != null) {
			val otherPatch = if(conflict.patchId == null) null else patchesById[conflict.patchId] ?: throw Exception("Unknown patch ID: ${conflict.patchId}")
			if(!resolveConflict(relative, otherPatch))
				return
		}

		if(conflict == null && Files.exists(locationDir.resolve(relative)))
			if(!resolveConflict(relative, null))
				return

		doApply(relative)
	}


	/**Finds files to unapply, and applies replacements when applicable.
	 * Designed to allow code sharing between planning code and code that actually changes files.
	 * This function does not make any database changes or alter any files - that's your job.
	 * @param db the database containing patches and files in this patch location
	 * @param replacementPriorities maps patch IDs to a priority - the highest-priority patch will be preferred when replacing removed files.
	 * Null is not a valid patch ID here. Patches not in this list have a replacement priority of zero.
	 * @param doRemove deletes or moves the applied file so it can be overwritten by a replacement.
	 * Returns true if the file was removed - if false, we won't try to replace the file.*/
	fun forEachToRemove(patch: Patch, location: PatchLocation, replacementPriorities: Map<Long, Int>, db: Database, doRemove: (Path, PatchFile) -> Boolean) {
		var lastParent: Path = Paths.get("")
		val tryDeleteDir = {dir: Path ->
			val toDelete = location.path.resolve(lastParent)
			log.debug("Deleting if empty: {}.", toDelete)
			toDelete.toFile().delete()
		}
		val patchesById: Map<Long, Patch> = patchOrm.query(patchTable).selectAll(db).associateBy { it.id }

		patchOrm.query(patchFileTable).andWhere("patchId = ?").param(patch.id).orderBy("path").selectEach(db) {file ->
			val path = Paths.get(file.path)
			if(doRemove(path, file)) {
				val replacements = patchOrm.query(patchFileTable).andWhere("path = ? AND applied = 0").param(path.toString()).selectAll(db)
				if(replacements.size > 0)
					tryReplace(path, replacements, replacementPriorities, location, patchesById, db)

				val parent = path.parent
				if(parent != null && parent.nameCount > 0) {
					if (parent != lastParent)
						tryDeleteDir(lastParent)
					lastParent = parent
				}
			}
		}

		tryDeleteDir(lastParent)
	}


	private fun tryReplace(relative: Path, replacements: List<PatchFile>, replacementPriorities: Map<Long, Int>, location: PatchLocation, patchesById: Map<Long, Patch>, db: Database) {
		val best = replacements.maxByOrNull {if(it.patchId == null) Integer.MIN_VALUE else replacementPriorities[it.patchId] ?: 0}
		if(best != null) { //TODO make sure we create records even when we don't apply a file, so this will work properly
			db.withTransaction {
				val replacmentPatch = patchesById[best.patchId]
				if(replacmentPatch == null && best.patchId != null)
					throw RuntimeException("Failed to find patch for $best.")

				best.applied = true
				if(best.patchId == null)
					patchFileTable.delete(best, db)
				else
					patchFileTable.update(best, db)

				val fromDir = if(replacmentPatch == null) location.baseFiles else location.path.resolve(replacmentPatch.name)
				val fromFile = fromDir.resolve(relative)
				val toFile = location.path.resolve(relative)
				log.debug("Replacing {} --> {}.", fromFile, toFile)
				Files.move(fromFile, toFile)
			}
		}
	}
}