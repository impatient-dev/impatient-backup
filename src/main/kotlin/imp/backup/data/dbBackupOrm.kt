package imp.backup.data

import imp.backup.files.EntryType
import imp.orm.kt.*
import java.util.*

val dbBackupOrm = Orm(listOf(
	DbBackupMeta::class,
	DbBackupEntry::class,
))

val dbTableMeta = dbBackupOrm.table(DbBackupMeta::class)
val dbTableEntry = dbBackupOrm.table(DbBackupEntry::class)


/**Overall statistics about this backup - there should be 1 record in this table.*/
@OrmTable("Meta")
data class DbBackupMeta (
	@PrimaryKey var id: Long,
	var fromLocationName: String,
	var lastStartMs: Long?,
	var lastEndMs: Long?,
	var files: Long,
	var bytes: Long,
	/**The ID of the key that was used to encrypt all file contents, if this is an encrypted backup.
	 * File and folder names are also encrypted and then base-64 encoded*/
	var encryptionKey: UUID?,
)


/**A file or directory.
 * Equals and hashCode() may work poorly due to the byte array members.*/
@OrmTable("Entry")
@OrmIndex("Entry_parent", "parentId")
data class DbBackupEntry(
	@PrimaryKey var id: Long,
	/**File or directory name, encrypted with the same key as the content (if any) but a different initialization vector.*/
	var name: ByteArray,
	/**The directory that contains this entry. This is null for the top-level directory, and should be non-null for everything else.*/
	@References("Entry") var parentId: Long?,
	/**File size, null for a directory or symlink, 0 or more otherwise.*/
	var bytes: Long?,
	/**The target this symlink points to, if it's a symlink. Possibly encrypted.*/
	var symlinkTo: ByteArray?,
	/**For files; null for a directory or symlink.*/
	var lastModifiedMs: Long?,
	/**The initialization vector used to encrypt the name, if it was encrypted with one.*/
	var nameIv: ByteArray?,
	/**The initialization vector used to encrypt the file contents or symlink target, if it was encrypted with one.*/
	var contentIv: ByteArray?,
) {
	val type: EntryType
	inline fun isDir() = type == EntryType.DIR
	inline fun isFile() = type == EntryType.FILE
	inline fun isSymlink() = type == EntryType.SYMLINK

	init {
		type = if(symlinkTo != null) EntryType.SYMLINK
			else if(bytes == null) EntryType.DIR else EntryType.FILE
	}
}