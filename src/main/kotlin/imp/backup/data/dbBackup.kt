package imp.backup.data

import imp.backup.crypto.SymKey
import imp.backup.crypto.SymKeyRepo
import imp.backup.files.CryptoFullEntryHandler
import imp.backup.files.DbBackupRawEntryHandler
import imp.backup.files.PlaintextFullEntryHandler
import imp.backup.logic.DbBackupEntries
import imp.orm.kt.impl.SchemaHandler
import imp.sqlite.Database
import imp.sqlite.cache.DbCache1Only
import imp.sqlite.main.Sqlite
import imp.sqlite.withTransaction
import imp.util.logger
import kotlinx.coroutines.withTimeout
import org.apache.commons.io.FileUtils
import java.nio.file.Files
import java.nio.file.Path
import java.time.Instant
import kotlin.io.path.exists
import kotlin.io.path.name

private val log = DbBackupImpl::class.logger


class DbBackupDecl (
	val dir: Path,
	override val locationName: String,
) : BackupDecl {
	override val name = dir.name
	override val type = BackupType.DB
	override val userPath get() = dir.toString()
	override suspend fun exists(): Boolean = dir.exists()

	/**Where the backed up files are stored.*/
	val filesDir: Path = dir.resolve("files")
	fun file(f: DbBackupEntry): Path {
		check(f.isFile())
		assert(f.id != -1L)
		return filesDir.resolve(f.id.toString())
	}

	private val dbFile = dir.resolve("db.sqlite")
	private val dbCache = DbCache1Only({ Sqlite.file(dbFile) }, 10_000L)

	override suspend fun delete() {
		log.info("Deleting backup {}.", dir)
		FileUtils.deleteDirectory(dir.toFile())
	}

	/**Opens a connection to the database, creating it if necessary.
	 * This function only allows 1 connection to exist at a time, to avoid errors relating to locked tables.
	 * This function expects you to not hold onto connections for very long, and will throw if it times out.*/
	suspend fun db(): Database {
		return withTimeout(15_000) { dbCache.borrow() }
	}

	suspend inline fun <T> db(block: suspend (Database) -> T): T {
		db().use { db ->
			return block(db)
		}
	}

	override suspend fun info(): BackupInfo? {
		if(!dbFile.exists())
			return null
		try {
			return db { db ->
				dbTableMeta.query().selectOnly(db)?.toInfo(name, locationName) ?: throw Exception("No meta record.")
			}
		} catch(e: Exception) {
			log.error("Failed to load meta from database $dbFile.", e)
			return null
		}
	}

	override suspend fun initExisting(): DbBackupImpl {
		Files.createDirectories(filesDir)
		var meta: DbBackupMeta? = null
		db { db ->
			SchemaHandler.apply(dbBackupOrm, db)
			meta = dbTableMeta.query().selectOnly(db)
			if(meta == null) {
				log.warn("Inserting missing meta for backup in {}.", dir)
				meta = DbBackupMeta(-1, "???", 0, null, 0, 0, null)
				dbTableMeta.insert(meta!!, db)
			}
		}
		val key = meta!!.encryptionKey?.let { SymKeyRepo.opt(it) ?: SymKey(it, null, null) }
		return DbBackupImpl(this, meta!!, key)
	}

	override suspend fun initNew(fromLocationName: String, key: SymKey?): DbBackupImpl {
		Files.createDirectories(filesDir)
		val meta = DbBackupMeta(-1, fromLocationName, null, null, 0, 0, key?.uuid)
		db { db ->
			SchemaHandler.apply(dbBackupOrm, db)
			db.withTransaction {
				dbTableMeta.query().delete(db)
				dbTableMeta.insert(meta, db)
			}
		}
		return DbBackupImpl(this, meta, key)
	}
}



class DbBackupImpl (
	override val decl: DbBackupDecl,
	private var meta: DbBackupMeta,
	val key: SymKey?,
) : BackupImpl {
	val entries = DbBackupEntries(this)

	init {
		check(key == null || key.key != null)
	}

	suspend inline fun <T> db(block: (Database) -> T): T = decl.db(block)

	override val raw = DbBackupRawEntryHandler(this)
	override val full = if(key == null) PlaintextFullEntryHandler(raw)
		else if(key.key == null) null
		else CryptoFullEntryHandler(key, raw)

	override fun reqFull() = full ?: throw Exception("Unknown key: ${key!!.uuid}")

	override suspend fun startBackup(start: Instant, fromLocationName: String) {
		setMeta(getMeta().copy(fromLocationName = fromLocationName, lastStartMs = start.toEpochMilli()))
	}

	override suspend fun endBackup(end: Instant, files: Long, bytes: Long) {
		setMeta(getMeta().copy(lastEndMs = end.toEpochMilli(), files = files, bytes = bytes))
	}

	suspend fun getMeta() = meta
	suspend fun setMeta(m: DbBackupMeta) {
		log.debug("Updating meta for backup {}.", decl.dir)
		check(m.id != -1L)
		db { db -> dbTableMeta.update(m, db) }
		meta = m
	}
}




private suspend fun DbBackupMeta.toInfo(
	name: String,
	locationName: String,
) = BackupInfo(
	name = name,
	type = BackupType.DB,
	fromLocationName = fromLocationName,
	toLocationName = locationName,
	lastStart = lastStartMs?.let(Instant::ofEpochMilli),
	lastEnd = lastEndMs?.let(Instant::ofEpochMilli),
	files = files,
	bytes = bytes,
	key = encryptionKey?.let { SymKeyRepo.orMissing(it) },
)