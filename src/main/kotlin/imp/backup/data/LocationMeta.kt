package imp.backup.data

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.module.kotlin.readValue
import imp.backup.UserException
import imp.backup.jsonMapper
import imp.util.checkedResolve
import imp.util.logger
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Path
import kotlin.io.path.Path

private val log = LocationMeta::class.logger

const val LOCATION_META_FILENAME = "impatient-backup.json"
/**Default staging dir for patch locations.*/
const val DEFAULT_STAGING_DIR = ".staging"

/**The contents of the JSON file defining the location.*/
class LocationMeta (
	val name: String,
	/**Defaults to USER.*/
	val type: LocationType?,
	/**The path where files are. Only valid for a patch location.*/
	val path: String? = null,
	/**Paths that are part of this location. If this is not specified, defaults to the directory containing the meta file. Only valid for a user location.*/
	val paths: List<String>? = null,
	/**Relative paths to skip. Only valid for a user location.*/
	val exclude: List<String>? = null,
	/**Required for PATCH locations, meaningless for other types.*/
	val stagingDirectory: String? = null,
)

enum class LocationType {
	USER,
	BACKUP,
	PATCH,
}

inline fun <T> LocationType?.choose(user: T, backup: T, patch: T, missing: T): T = when(this) {
	LocationType.USER -> user
	LocationType.BACKUP -> backup
	LocationType.PATCH -> patch
	null -> missing
}



/**The path can e a meta file, or the directory containing one.*/
fun loadLocation(path: Path): Location {
	log.debug("Trying to load location {}.", path)
	try {
		val metaPath = if (Files.isDirectory(path)) path.resolve(LOCATION_META_FILENAME) else path
		if (!Files.isRegularFile(metaPath)) {
			log.debug("Missing - nonexistent meta file: {}.", metaPath)
			return MissingLocation(path, "File is missing or unreadable: $metaPath")
		}

		val parsed: LocationMeta
		try {
			parsed = jsonMapper.readValue<LocationMeta>(metaPath.toFile())
		} catch (e: JsonProcessingException) {
			log.error("Failed to parse location meta file $metaPath", e)
			return MissingLocation(path, "Failed to parse $metaPath: ${e.message}")
		}

		return parseMeta(parsed, path, metaPath)
	} catch(e: Exception) {
		log.error("Failed to load location $path.", e)
		return MissingLocation(path, "Unexpected error (see log for details): $e")
	}
}


private fun parseMeta(meta: LocationMeta, origPath: Path, metaPath: Path): Location {
	try {
		// validation
		val type = meta.type ?: LocationType.USER
		if(meta.path != null && type != LocationType.PATCH)
			return MissingLocation(origPath, "Invalid meta file: can only specify path for a STAGING location.")
		if (meta.paths != null && type != LocationType.USER)
			return MissingLocation(origPath, "Invalid meta file: can only specify paths for a USER location.")
		if (meta.paths != null && meta.paths.isEmpty())
			return MissingLocation(origPath, "Invalid meta file: location has no paths - [].")
		if (meta.exclude != null && type != LocationType.USER && type != LocationType.PATCH)
			return MissingLocation(origPath, "Invalid meta file: cannot specify exclude list for a $type location.")
		if (meta.stagingDirectory != null && type != LocationType.PATCH)
			return MissingLocation(origPath, "Invalid meta file: can only specify staging directory for a PATCH location.")

		when(type) {
			LocationType.USER -> {
				val relPaths = if (meta.paths == null) listOf(Path.of("")) else meta.paths.map { mapUserRelative(it) }
				val exclude = if(meta.exclude == null) emptyList<Path>() else meta.exclude.map { mapUserRelative(it) }
				log.debug("Found user location {} at {}.", meta.name, origPath)
				return UserLocation(meta.name, metaPath, origPath, relPaths, exclude.toSet())
			}
			LocationType.BACKUP -> {
				log.debug("Found backup location {} at {}.", meta.name, origPath)
				return BackupLocation(meta.name, metaPath.parent, metaPath, origPath)
			}
			LocationType.PATCH -> {
				val path = if(meta.path == null) metaPath.parent.toAbsolutePath() else mapUserToAbsolute(meta.path, metaPath.parent)
				val stagingDir = if(meta.stagingDirectory == null) metaPath.parent.resolve(DEFAULT_STAGING_DIR).toAbsolutePath()
					else mapUserToAbsolute(meta.stagingDirectory, metaPath.parent)
				log.debug("Found patch location {} at {}.", meta.name, origPath)
				return PatchLocation(meta.name, path, metaPath, origPath, stagingDir)
			}
		}

	} catch(e: UserException) {
		log.error("Invalid meta file $metaPath.", e)
		return MissingLocation(origPath, "Invalid meta file: ${e.message}")
	}
}


@Throws(UserException::class) private fun mapUserPath(str: String): Path {
	try {
		return Path.of(str)
	} catch(e: IOException) {
		throw UserException("Invalid path: $str")
	}
}

@Throws(UserException::class) private fun mapUserRelative(str: String): Path {
	val out = mapUserPath(str)
	if(out.isAbsolute)
		throw UserException("Path must be relative (not absolute): $str")
	val normal = out.normalize()
	if(out != normal)
		throw UserException("Unacceptable path $str - must use $normal")
	try {
		Path("").checkedResolve(out)
	} catch(e: Exception) {
		throw UserException(e.message!!, e)
	}
	return out
}

@Throws(UserException::class) private fun mapUserToAbsolute(str: String, resolver: Path): Path {
	var out = mapUserPath(str)
	if(!out.isAbsolute)
		out = resolver.resolve(out).toAbsolutePath()
	return out
}