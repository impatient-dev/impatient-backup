package imp.backup.data

import imp.orm.kt.Orm
import imp.orm.kt.OrmIndex
import imp.orm.kt.PrimaryKey
import imp.orm.kt.References


/**Orm representing patches and files.
 * Note: we try to leave things out of the database unless they really need to be tracked.
 * Each table has a description of under what conditions records will exist.*/
val patchOrm = Orm(listOf(
	Patch::class,
	PatchFile::class,
))

val patchTable = patchOrm.table(Patch::class)
val patchFileTable = patchOrm.table(PatchFile::class)


/**A set of files that are applied together in a patch location. Only applied patches are stored in this table.*/
class Patch (
	@PrimaryKey var id: Long,
	var name: String,
	var applied: Boolean,
)

/**An individual file that is part of a patch, or possibly part of the base content before any patches were applied (if patchId == null).
 * 2 types of patch files are stored: files for applied patches, and un-applied files for no patch (replaced by a file in some applied patch).*/
@OrmIndex("PatchFile_path", "path")
class PatchFile (
	@PrimaryKey var id: Long,
	/**Relative to the patch location.*/
	var path: String,
	/**The parent patch id. If there is no patch ID, this represents a file that is not part of any patch (e.g. part of the base game).*/
	@References("Patch") var patchId: Long?,
	var applied: Boolean,
)
