package imp.backup.data

import imp.util.FirstList
import java.nio.file.Path

data class BasicPatchInfo (
	val name: String,
	val applied: Boolean,
)

/**Another already-applied patch that contains some of the same paths as the patch you're trying to apply/remove.*/
data class PatchConflict (
	val otherPatchName: String,
	var files: Int,
	val someFiles: FirstList<Path>,
)

/**Represents the user's decision for whether to replace the conflicting patch.*/
data class PatchConflictChoice (
	val patchName: String,
	val files: Int,
	val someFiles: FirstList<Path>,
	var replace: Boolean,
) {
	constructor(conflict: PatchConflict, replace: Boolean) : this(conflict.otherPatchName, conflict.files, conflict.someFiles, replace)
}

data class PatchToggleResult (
	val location: PatchLocation,
	val patchName: String,
	/**True if the patch was toggled; false if the user cancelled.*/
	val changed: Boolean,
	/**Whether the patch is applied now.*/
	val isApplied: Boolean,
)