package imp.backup.data

import com.fasterxml.jackson.module.kotlin.readValue
import imp.backup.crypto.SymKey
import imp.backup.files.DirRawEntryHandler
import imp.backup.files.PlaintextFullEntryHandler
import imp.backup.jsonMapper
import imp.json.impWrite
import imp.util.logger
import org.apache.commons.io.FileUtils
import java.nio.file.Path
import java.time.Instant
import kotlin.io.path.exists
import kotlin.io.path.name

private val log = RawBackupImpl::class.logger


class RawBackupDecl (
	val dir: Path,
	override val locationName: String,
) : BackupDecl {
	override val name = dir.name
	override val type = BackupType.RAW
	override val userPath get() = dir.toString()

	val metaFile = dir.resolve("backup.json")
	val filesDir = dir.resolve("files")

	fun writeMeta(meta: RawBackupMeta) {
		log.debug("Writing meta to {}", metaFile)
		jsonMapper.impWrite(metaFile, meta)
	}

	/**Returns null if the read fails.*/
	private fun tryReadMeta(): RawBackupMeta? {
		if(!metaFile.exists())
			return null
		try {
			return jsonMapper.readValue<RawBackupMeta>(metaFile.toFile())
		} catch(e: Exception) {
			log.error("Failed to read backup meta $metaFile.", e)
			return null
		}
	}

	override suspend fun info() = tryReadMeta()?.toInfo(name, locationName)

	override suspend fun exists() = filesDir.exists()

	override suspend fun delete() {
		log.info("Deleting backup at {}.", dir)
		FileUtils.deleteDirectory(dir.toFile())
	}

	override suspend fun initExisting(): RawBackupImpl {
		var meta = tryReadMeta()
		if(meta == null) {
			meta = RawBackupMeta(null, null, null, 0, 0)
			writeMeta(meta)
		}
		return RawBackupImpl(this, meta)
	}

	override suspend fun initNew(fromLocationName: String, key: SymKey?): RawBackupImpl {
		check(key == null) { "Encryption is not supported for raw backups." }
		val meta = RawBackupMeta(fromLocationName, null, null, 0, 0)
		writeMeta(meta)
		return RawBackupImpl(this, meta)
	}
}




class RawBackupImpl (
	override val decl: RawBackupDecl,
	private var meta: RawBackupMeta,
): BackupImpl {
	inline val dir get() = decl.dir
	inline val filesDir get() = decl.filesDir

	override fun toString() = "RawBackup($dir)"

	override val raw = DirRawEntryHandler(filesDir)
	override val full = PlaintextFullEntryHandler(raw)
	override fun reqFull() = full

	override suspend fun startBackup(start: Instant, fromLocationName: String) {
		setMeta(getMeta().copy(fromLocationName = fromLocationName, lastStart = start, lastEnd = null))
	}

	override suspend fun endBackup(end: Instant, files: Long, bytes: Long) {
		setMeta(getMeta().copy(lastEnd = end, files = files, bytes = bytes))
	}

	fun getMeta() = meta
	fun setMeta(m: RawBackupMeta) {
		decl.writeMeta(m)
		meta = m
	}
}


data class RawBackupMeta (
	/**Name of the user location that was backed up.*/
	val fromLocationName: String?,
	/**When the last full/incremental backup here started.*/
	var lastStart: Instant?,
	/**When the last full/incremental backup here finished, or null if the backup is still in progress.*/
	var lastEnd: Instant?,
	var files: Long,
	var bytes: Long,
) {
	fun toInfo(
		name: String,
		locationName: String,
	) = BackupInfo(
		name = name,
		type = BackupType.RAW,
		fromLocationName = fromLocationName,
		lastStart = lastStart,
		lastEnd = lastEnd,
		toLocationName = locationName,
		files = files,
		bytes = bytes,
		key = null,
	)
}


