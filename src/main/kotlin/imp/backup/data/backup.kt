package imp.backup.data

import androidx.compose.runtime.Immutable
import imp.backup.crypto.SymKey
import imp.backup.files.DirRawEntryHandler
import imp.backup.files.PlaintextFullEntryHandler
import imp.backup.files.SyncTarget
import java.nio.file.Path
import java.time.Instant

/**A declaration of a backup, which may or may not exist. Instances should ideally be stateless.*/
interface BackupDecl {
	val name: String
	/**The location this backup is a part of (not the location that was backed up into here).*/
	val locationName: String
	val type: BackupType
	/**Some kind of user-readable path telling where this backup is.*/
	val userPath: String
	/**Returns null if this backup doesn't exist yet.*/
	suspend fun info(): BackupInfo?
	suspend fun exists(): Boolean
	suspend fun delete()
	/**Sets this up as an existing backup. This function loads metadata to determine whether and how the backup is encrypted.
	 * If that metadata is missing, this function should try to fill it in, but may throw instead.*/
	suspend fun initExisting(): BackupImpl
	/**Sets this up as a new backup with the provided information.
	 * This should not be called if the backup already exists.*/
	suspend fun initNew(fromLocationName: String, key: SymKey?): BackupImpl
}

interface BackupImpl : SyncTarget {
	val decl: BackupDecl
	override val userPath get() = decl.userPath
	override val locationName get() = decl.locationName
	suspend fun startBackup(start: Instant, fromLocationName: String)
	/**Records in the metadata that the backup has finished.
	 * The implementation may use or ignore the files and bytes provided.
	 * Those numbers are the totals that should be in this backup, not the number that was copied/changed.*/
	suspend fun endBackup(end: Instant, files: Long, bytes: Long)
}

enum class BackupType (val userFriendly: String) {
	RAW("Raw"),
	DB("Database"),
}


@Immutable data class BackupInfo(
	val name: String,
	val type: BackupType,
	val fromLocationName: String?,
	val toLocationName: String,
	val lastStart: Instant?,
	val lastEnd: Instant?,
	val files: Long?,
	val bytes: Long?,
	val key: SymKey?,
)


/**An arbitrary path, which is a valid sync target (for restoring a backup).*/
class ArbitraryPath(val path: Path) : SyncTarget {
	override val locationName: String? = null
	override val userPath = path.toString()
	override val raw = DirRawEntryHandler(path)
	override val full = PlaintextFullEntryHandler(raw)
	override fun reqFull() = full
}