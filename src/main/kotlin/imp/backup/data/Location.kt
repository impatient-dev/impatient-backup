package imp.backup.data

import imp.backup.files.DirRawEntryHandler
import imp.backup.files.PlaintextFullEntryHandler
import imp.backup.files.SyncTarget
import imp.sqlite.Database
import imp.sqlite.main.Sqlite
import imp.util.logger
import java.nio.file.Files
import java.nio.file.Path
import kotlin.io.path.Path

private val log = Location::class.logger


sealed interface Location {
	/**Null if not known (MissingLocation).*/
	val name: String?
	/**The path we looked at to find this location (either the path to the meta file, or to a directory containing it).*/
	val searchPath: Path
	/**The meta file that defines this location.*/
	val metaFile: Path?
	/**The directory containing the meta file.*/
	val metaDir: Path? get() = metaFile?.parent
}

/**A directory where the user stores files that should be backed up.*/
data class UserLocation (
	override val name: String,
	override val metaFile: Path,
	override val searchPath: Path,
	/**Files/directories that are part of this location. These are canonical and relative to the directory that contains the location meta file.*/
	val paths: List<Path>,
	/**A set of canonical relative paths that should not be backed up or synchronized.*/
	val exclude: Set<Path>
) : Location, SyncTarget {
	init {
		check(paths.size == 1 && paths[0] == Path("")) { "Multiple/limited paths are not supported (yet): $paths" }
	}

	override fun toString() = "UserLocation($name $metaFile)"
	override val locationName = name
	override val userPath = searchPath.toString()
	override val raw = DirRawEntryHandler(metaDir!!)
	override val full = PlaintextFullEntryHandler(raw)
	override fun reqFull() = full
}

/**A directory where we store backups.*/
data class BackupLocation (
	override val name: String,
	val path: Path,
	override val metaFile: Path,
	override val searchPath: Path,
): Location {
	val rawBackupDir: Path = path.resolve("raw")
	val dbBackupDir: Path = path.resolve("db")
	override fun toString() = "BackupLocation($name $path)"

	private fun rawBackup(name: String) = RawBackupDecl(rawBackupDir.resolve(name), name)
	private fun dbBackup(name: String) = DbBackupDecl(dbBackupDir.resolve(name), this.name)
	fun backup(type: BackupType, name: String): BackupDecl = if(type == BackupType.RAW) rawBackup(name) else dbBackup(name)
}


/**Database file telling what files belong to which patches.*/
val PATCH_DB_FILENAME = ".patch.sqlite"
/**Folder in the staging directory, containing base files that were replaced by patch files.*/
val REPLACED_BASE_DIR = ".base"

/**A directory where we apply patches (e.g. for applying mods to a game).
 * Not-currently-applied files are stored in a staging directory (which can be located anywhere, but is not necessarily its own location).*/
data class PatchLocation (
	override val name: String,
	val path: Path,
	override val metaFile: Path,
	override val searchPath: Path,
	val stagingDir: Path,
) : Location {
	val dbFile: Path = this.stagingDir.resolve(PATCH_DB_FILENAME)
	/**The place where unapplied files not belonging to any patch are stored. (Files replaced by patch files.)*/
	val baseFiles: Path = this.stagingDir.resolve(REPLACED_BASE_DIR)
	/**Finds a patch directory by patch name.*/
	fun patchFiles(name: String): Path = this.stagingDir.resolve(name)

	@Volatile private var dbInitialized = false

	/**Connects to the database, creating and initializing it if necessary.*/
	fun db(): Database {
		val file = dbFile
		log.debug("Connecting to {}.", file)
		val create = !Files.exists(file)
		if(create)
			Files.createDirectories(file.parent)
		val db = Sqlite.file(file.toString())
		try {
			if(create || !dbInitialized) {
				patchOrm.applySchema(db)
				dbInitialized = true
			}
			return db
		} catch(e: Throwable) {
			db.close()
			throw e
		}
	}
}


/**A location that could not be loaded, probably because the file or directory is missing.*/
data class MissingLocation (
	override val searchPath: Path,
	val reason: String
) : Location {
	override val metaFile = null
	override val name get() = null
	override fun toString() = "MissingLocation($searchPath)"
}