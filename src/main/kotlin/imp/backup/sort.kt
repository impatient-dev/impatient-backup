package imp.backup

import imp.backup.data.BackupLocation
import imp.backup.data.MissingLocation
import imp.backup.data.PatchLocation
import imp.backup.data.UserLocation

fun compareUserLocations(a: UserLocation, b: UserLocation): Int {
	val out = a.name.compareTo(b.name)
	if(out != 0) return out
	return a.searchPath.compareTo(b.searchPath)
}

fun compareBackupLocations(a: BackupLocation, b: BackupLocation): Int {
	val out = a.name.compareTo(b.name)
	if(out != 0) return out
	return a.searchPath.compareTo(b.searchPath)
}

fun comparePatchLocations(a: PatchLocation, b: PatchLocation): Int {
	val out = a.name.compareTo(b.name)
	if(out != 0) return out
	return a.searchPath.compareTo(b.searchPath)
}

fun compareMissingLocations(a: MissingLocation, b: MissingLocation): Int = a.searchPath.compareTo(b.searchPath)