package imp.backup

import kotlin.io.path.Path

/**Paths for config files and other files that belong to this application.
 * The paths are relative to the working directory, which is the directory containing the jar (startup verifies this).*/
object AppPaths {
	val savedAdmins = Path("data", "admins.json")
	val savedServers = Path("data", "servers.json")
	val settings = Path("data", "settings.json")
	val secretLink = Path("cfg", "secret")
}