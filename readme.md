# impatient-backup

Backs up and restores files.

This program supports 2 types of backups:
* **Raw** - the files are copied to a backup location, with the names and folder structure left untouched. This type cannot be encrypted.
* **Database** - the files are copied to a backup location, but their names and folder structure change. A database keeps track of information about the files, including any encryption keys used.